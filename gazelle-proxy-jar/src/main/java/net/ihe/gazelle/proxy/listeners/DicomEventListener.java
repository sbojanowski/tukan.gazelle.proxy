package net.ihe.gazelle.proxy.listeners;

import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessage;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.model.message.DicomMessage;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;

import javax.persistence.EntityManager;
import java.nio.charset.Charset;
import java.sql.Timestamp;

public class DicomEventListener extends SameEventListener<DimseMessage> {

    protected static final Charset UTF_8 = Charset.forName("UTF-8");

    public DicomEventListener() {
        super();
    }

    protected void saveMessage(final Timestamp timeStamp, final DimseMessage dicom, final String requesterIp,
            final int requesterPort, final int proxyPort, final String responderIp, final int responderPort,
            final int requestChannelId, final int responseChannelId, final ProxySide side) {
        // Messages are coming from different threads!
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {

                        DicomMessage messageToStore = new DicomMessage(timeStamp, requesterIp, requesterPort, proxyPort,
                                responderIp, responderPort, side);
                        if (dicom.getDataSet() != null) {
                            String storageDicom = ApplicationConfiguration
                                    .getValueOfVariable("storage_dicom", entityManager);
                            String pathToStore = dicom.getDataSet().getAbsolutePath();
                            pathToStore = pathToStore.replace(storageDicom, "");
                            messageToStore.setMessageReceived(pathToStore.getBytes(UTF_8));
                        }

                        if (dicom.getCommandSet() != null && dicom.getCommandSet().length > 0) {
                            messageToStore.setFileCommandSet(dicom.getCommandSet());
                        }
                        messageToStore.setFileTransfertSyntax(dicom.getTransferSyntax());
                        processMessage(entityManager, messageToStore, requestChannelId, responseChannelId);

                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }
        }
    }
}
