/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.model.message;

import com.pixelmed.dicom.*;
import com.pixelmed.network.MessageServiceElementCommand;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.gui.MessageBean;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.io.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

@Entity
@Name("dicomMessage")
public class DicomMessage extends AbstractMessage implements java.io.Serializable {

    private static final DicomDictionary DICOM_DICTIONARY = new DicomDictionary();

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 54153409123922L;

    @Column(name = "dicom_filecommandset")
    @Basic(fetch = FetchType.LAZY)
    private byte[] fileCommandSet;

    @Column(name = "dicom_filetransfertsyntax")
    @Lob
    @Type(type = "text")
    private String fileTransfertSyntax;

    @Column(name = "dicom_affectedsopclassuid")
    private String infoAffectedSOPClassUID;

    @Column(name = "dicom_commandfield")
    private String infoCommandField;

    @Column(name = "dicom_requestedsopclassuid")
    private String infoRequestedSOPClassUID;

    // Constructors

    public DicomMessage() {
        super();
    }

    public DicomMessage(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
                        Integer remotePort, ProxySide proxySide) {
        super(timestamp, fromIP, localPort, proxyPort, toIP, remotePort, proxySide);
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) {
            return bytes + " B";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public String getInfoAffectedSOPClassUID() {
        return infoAffectedSOPClassUID;
    }

    public void setInfoAffectedSOPClassUID(String infoAffectedSOPClassUID) {
        this.infoAffectedSOPClassUID = infoAffectedSOPClassUID;
    }

    public String getInfoCommandField() {
        return infoCommandField;
    }

    public String getInfoRequestedSOPClassUID() {
        return infoRequestedSOPClassUID;
    }

    public ChannelType getChannelType() {
        return ChannelType.DICOM;
    }

    public byte[] getFileCommandSet() {
        return fileCommandSet.clone();
    }

    public void setFileCommandSet(byte[] fileCommandSet) {
        this.fileCommandSet = fileCommandSet.clone();
        dumpFileCommandSet();
    }

    public String getFileTransfertSyntax() {
        return fileTransfertSyntax;
    }

    public void setFileTransfertSyntax(String fileTransfertSyntax) {
        this.fileTransfertSyntax = fileTransfertSyntax;
    }

    public String getAttributesForCommandSetString() {
        return dumpFileCommandSet(false);
    }

    private String dumpFileCommandSet(boolean compact) {
        if (getFileCommandSet() != null) {
            AttributeList attributeList = new AttributeList();

            try {
                attributeList.read(new DicomInputStream(new ByteArrayInputStream(getFileCommandSet())));
                Set<AttributeTag> keys = attributeList.keySet();
                Iterator<AttributeTag> iterator = keys.iterator();

                StringWriter sw = new StringWriter();
                BufferedWriter bw = new BufferedWriter(sw);

                while (iterator.hasNext()) {
                    AttributeTag tag = iterator.next();
                    Attribute attributeValue = attributeList.get(tag);

                    if (!compact) {
                        bw.append("<p><span style=\"font-family: monospace;\">");
                        bw.append(StringEscapeUtils.escapeHtml(attributeValue.toString(DICOM_DICTIONARY)));
                        bw.append("</span></p>");
                        bw.newLine();
                    }

                    // affected sop class
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("AffectedSOPClassUID"))) {
                        try {
                            String fieldValue = attributeValue.getDelimitedStringValuesOrEmptyString();
                            String affectedSopClassUID = SOPClassDescriptions.getDescriptionFromUID(fieldValue);
                            if (affectedSopClassUID == null) {
                                affectedSopClassUID = fieldValue;
                            }
                            if (!compact) {
                                bw.append("<p>Affected SOP class : <span style=\"font-family: monospace;\">");
                                bw.append(StringEscapeUtils.escapeHtml(affectedSopClassUID));
                                bw.append("</span></p>");
                                bw.newLine();
                            } else {
                                bw.append("Aff. <span style=\"font-family: monospace;\">");
                                bw.append(StringEscapeUtils.escapeHtml(affectedSopClassUID));
                                bw.append("</span><br />");
                            }
                        } catch (Exception e) {
                            log.error("" + e);
                        }
                    }

                    // Command field
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("CommandField"))) {
                        try {
                            if (!compact) {
                                bw.append("<p>Command fields : <span style=\"font-family: monospace;\">");
                            } else {
                                bw.append("Cmd. <span style=\"font-family: monospace;\">");
                            }
                            int[] integerValues = attributeValue.getIntegerValues();
                            for (Integer fieldValue : integerValues) {
                                String commandFieldTmp = MessageServiceElementCommand.toString(fieldValue);
                                bw.append(StringEscapeUtils.escapeHtml(commandFieldTmp));
                                bw.append(" ");
                            }
                            if (!compact) {
                                bw.append("</span></p>");
                                bw.newLine();
                            } else {
                                bw.append("</span><br />");
                            }
                        } catch (Exception e) {
                            log.error("" + e);
                        }
                    }

                    // requested sop class
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("RequestedSOPClassUID"))) {
                        try {
                            String fieldValue = attributeValue.getDelimitedStringValuesOrEmptyString();
                            String requestedSopClassUID = SOPClassDescriptions.getDescriptionFromUID(fieldValue);
                            if (requestedSopClassUID == null) {
                                requestedSopClassUID = fieldValue;
                            }
                            if (!compact) {
                                bw.append("<p>Requested SOP class : <span style=\"font-family: monospace;\">");
                                bw.append(StringEscapeUtils.escapeHtml(requestedSopClassUID));
                                bw.append("</span></p>");
                                bw.newLine();
                            } else {
                                bw.append("Req. <span style=\"font-family: monospace;\">");
                                bw.append(StringEscapeUtils.escapeHtml(requestedSopClassUID));
                                bw.append("</span><br />");
                            }
                        } catch (Exception e) {
                            log.error("" + e);
                        }
                    }
                    if (!compact) {
                        if (iterator.hasNext()) {
                            bw.append("<hr />");
                        }
                    }
                }
                bw.close();
                return sw.toString();

            } catch (IOException e) {
                log.error("" + e);
            } catch (DicomException e) {
                log.error("" + e);
            }
        }
        return "";
    }

    public void dumpFileCommandSet() {
        if (getFileCommandSet() != null) {
            AttributeList attributeList = new AttributeList();

            try {
                attributeList.read(new DicomInputStream(new ByteArrayInputStream(getFileCommandSet())));
                Set<AttributeTag> keys = attributeList.keySet();
                Iterator<AttributeTag> iterator = keys.iterator();

                while (iterator.hasNext()) {
                    AttributeTag tag = iterator.next();
                    Attribute attributeValue = attributeList.get(tag);

                    // affected sop class
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("AffectedSOPClassUID"))) {
                        try {
                            String fieldValue = attributeValue.getDelimitedStringValuesOrEmptyString();
                            // See GazelleSOPClassDescriptions in
                            // DicomSCPScreener for more descriptions
                            String affectedSopClassUID = null;
                            if (affectedSopClassUID == null) {
                                affectedSopClassUID = fieldValue;
                            }
                            infoAffectedSOPClassUID = affectedSopClassUID;

                        } catch (Exception e) {
                            log.error("" + e);
                        }
                    }
                    // Command field
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("CommandField"))) {
                        try {
                            int[] integerValues = attributeValue.getIntegerValues();
                            for (Integer fieldValue : integerValues) {
                                String commandFieldTmp = MessageServiceElementCommand.toString(fieldValue);
                                infoCommandField = commandFieldTmp;
                            }
                        } catch (Exception e) {
                            log.error("" + e);
                        }
                    }
                    // requested sop class
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("RequestedSOPClassUID"))) {
                        try {
                            String fieldValue = attributeValue.getDelimitedStringValuesOrEmptyString();
                            // See GazelleSOPClassDescriptions in
                            // DicomSCPScreener for more descriptions
                            String requestedSopClassUID = null;
                            if (requestedSopClassUID == null) {
                                requestedSopClassUID = fieldValue;
                            }
                            infoRequestedSOPClassUID = requestedSopClassUID;
                        } catch (Exception e) {
                            log.error("" + e);
                        }
                    }
                }
            } catch (IOException e) {
                log.error("" + e);
            } catch (DicomException e) {
                log.error("" + e);
            }
        }
    }

    @Override
    public String getInfoGUI() {
        return dumpFileCommandSet(true);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Arrays.hashCode(fileCommandSet);
        result = prime * result + ((fileTransfertSyntax == null) ? 0 : fileTransfertSyntax.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DicomMessage other = (DicomMessage) obj;
        if (!Arrays.equals(fileCommandSet, other.fileCommandSet)) {
            return false;
        }
        if (fileTransfertSyntax == null) {
            if (other.fileTransfertSyntax != null) {
                return false;
            }
        } else if (!fileTransfertSyntax.equals(other.fileTransfertSyntax)) {
            return false;
        }
        return true;
    }

    public File getFile() {
        File storageFolder = new File(ApplicationConfigurationManager.instance().getDicomStorage());
        return new File(storageFolder, getMessageReceivedAsString());
    }

    @Override
    public int getMessageLength() {
        try {
            return (int) getFile().length();
        } catch (Throwable e) {
            return 0;
        }
    }

    @Override
    public InputStream getMessageReceivedStream() {
        try {
            return new FileInputStream(getFile());
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    @Override
    public PartSource getPartSource(String messageType) {
        try {
            return new FilePartSource(messageType, getFile());
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    @Override
    public PartSource getPartSource() {
        return getPartSource(getType());
    }

    public String getDicomMessageReceivedAsString() {
        MessageBean mb = new MessageBean();
        byte[] result;
        try {
            result = mb.dicom2txt(this);
            if (result != null) {
                return new String(result, "UTF-8");
            }
        } catch (IOException e) {
            log.error("Failed to dump file : " + e);
        }
        return null;
    }
}
