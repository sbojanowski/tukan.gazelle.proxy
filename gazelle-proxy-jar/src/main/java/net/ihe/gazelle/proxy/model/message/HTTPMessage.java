package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.apache.commons.lang.StringEscapeUtils;
import org.jboss.seam.annotations.Name;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.sql.Timestamp;

@Entity
@Name("httpMessage")
public class HTTPMessage extends AbstractMessage implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -422326643466081766L;

    @Column(name = "http_headers")
    @Basic(fetch = FetchType.LAZY)
    protected byte[] headers;

    @Column(name = "http_messageType")
    protected String messageType;

    public HTTPMessage(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
            Integer inRemotePort, ProxySide proxySide) {
        super(timestamp, fromIP, localPort, proxyPort, toIP, inRemotePort, proxySide);
    }

    public HTTPMessage() {
        super();
    }

    public byte[] getHeaders() {
        return headers.clone();
    }

    public void setHeaders(byte[] headers) {
        this.headers = headers.clone();
    }

    public String getHeadersAsString() {
        return new String(headers, UTF_8);
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @Override
    public String getInfoGUI() {
        return StringEscapeUtils.escapeHtml(messageType);
    }

    public ChannelType getChannelType() {
        return ChannelType.HTTP;
    }

}
