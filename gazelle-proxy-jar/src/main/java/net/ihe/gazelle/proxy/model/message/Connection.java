package net.ihe.gazelle.proxy.model.message;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "pxy_connection")
public class Connection implements java.io.Serializable {

    private static final long serialVersionUID = 6402109080942012360L;

    @Id
    @GeneratedValue
    private Integer id;

    @OneToMany(mappedBy = "connection")
    @OrderBy("dateReceived")
    private Set<AbstractMessage> messages;

    private String uuid;

    private transient List<AbstractMessage> sortedMessages = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Set<AbstractMessage> getMessages() {
        return messages;
    }

    public void setMessages(Set<AbstractMessage> messages) {
        this.messages = messages;
    }

    public List<AbstractMessage> getSortedMessages() {
        if (sortedMessages == null) {
            sortedMessages = new ArrayList<>(getMessages());
            Collections.sort(sortedMessages);
        }
        return sortedMessages;
    }

    @Override
    public String toString() {
        return "Connection [id=" + id + ", uuid=" + uuid + "]";
    }

}
