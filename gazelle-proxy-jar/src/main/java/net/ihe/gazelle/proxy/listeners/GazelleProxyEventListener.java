package net.ihe.gazelle.proxy.listeners;

import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.jms.ProxyMessageSender;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.HL7Message;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import org.jboss.seam.Component;
import org.jboss.seam.contexts.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public abstract class GazelleProxyEventListener<REQU, RESP> implements ProxyEventListener<REQU, RESP> {

    private static Logger log = LoggerFactory.getLogger(GazelleProxyEventListener.class);
    private Boolean jmsCommunicationEnabled = null;

    public GazelleProxyEventListener() {
        super();
    }

    public void processMessage(EntityManager entityManager, AbstractMessage abstractMessage, Integer requestChannelId,
                               Integer responseChannelId) {
        if (abstractMessage.getConnection() == null) {
            Connection connection = ConnectionManager
                    .retrieveConnection(entityManager, requestChannelId, responseChannelId);
            abstractMessage.setConnection(connection);
        }
        if (abstractMessage.getId() == null) {
            entityManager.persist(abstractMessage);
            // Without this flush, the response message would not exist in the database when TEE queries for it.
            entityManager.flush();
        } else {
            entityManager.merge(abstractMessage);
        }

        // A preference is define to enable jms communication with tm : jms_communication_is_enabled
        if (isJmsCommunicationEnabled()) {
//            since this request is occurring outside of the context of the servlet request,
//            need to wrap the call to the seam component with the Lifecycle.begin and Lifecycle.end
            Lifecycle.beginCall();
            if (abstractMessage instanceof HL7Message) {
                try {
                    ProxyMessageSender proxyMessageSender = (ProxyMessageSender) Component
                            .getInstance("proxyMessageSender");
                    proxyMessageSender.sendMessage((HL7Message) abstractMessage);
                } catch (Exception e) {
                    log.error("processMessage() : " + e.getCause());
                }
            }
            Lifecycle.endCall();
        }

    }

    private boolean isJmsCommunicationEnabled() {
        if (jmsCommunicationEnabled == null) {
            Lifecycle.beginCall();
            jmsCommunicationEnabled = ApplicationConfigurationManager.instance().isJmsCommunicationEnabled();
            Lifecycle.endCall();
        }
        return jmsCommunicationEnabled;
    }
}
