package net.ihe.gazelle.proxy.listeners;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.model.message.RawMessage;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class RawEventListener extends SameEventListener<byte[]> {

    public RawEventListener() {
        super();
    }

    protected void saveMessage(final Timestamp timeStamp, final byte[] message, final String requesterIp,
            final int requesterPort, final int proxyPort, final String responderIp, final int responderPort,
            final int requestChannelId, final int responseChannelId, final ProxySide side) {
        // Messages are coming from different threads!
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                        RawMessage rawmessage = new RawMessage(timeStamp, requesterIp, requesterPort, proxyPort,
                                responderIp, responderPort, side);
                        rawmessage.setMessageReceived(message);
                        processMessage(entityManager, rawmessage, requestChannelId, responseChannelId);
                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }
        }
    }
}
