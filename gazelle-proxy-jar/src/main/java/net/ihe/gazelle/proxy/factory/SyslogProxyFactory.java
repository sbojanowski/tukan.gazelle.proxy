package net.ihe.gazelle.proxy.factory;

import net.ihe.gazelle.proxy.listeners.SyslogEventListener;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyFactory;
import net.ihe.gazelle.proxy.netty.protocols.syslog.SyslogProxy;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.proxy.netty.ProxyFactory.class)
public class SyslogProxyFactory implements ProxyFactory {

    @Override
    public Proxy<?, ?> newProxy(ConnectionConfig connectionConfig) {
        SyslogEventListener proxyEventListener = new SyslogEventListener();
        return new SyslogProxy(proxyEventListener, connectionConfig);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.SYSLOG;
    }

}
