package net.ihe.gazelle.proxy.gui;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.evsclient.connector.api.EVSClientServletConnector;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.proxy.dao.ProxyDAO;
import net.ihe.gazelle.proxy.model.message.*;
import net.ihe.gazelle.proxy.netty.ChannelType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Name("messageBean")
@Scope(ScopeType.PAGE)
// @MeasureCalls
public class MessageBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2873106646480997932L;

    private static final Logger LOG = Logger.getLogger(MessageBean.class);
    @In(value = "#{facesContext}")
    FacesContext facesContext;
    @In
    private EntityManager entityManager;
    @In(value = "#{facesContext.externalContext}")
    private ExternalContext extCtx;
    private Integer messageIndex;

    private Integer messageIndexMax;

    private AbstractMessage message;

    private String validatorUrl;

    private boolean displayInText;

    public boolean isDisplayInText() {
        return displayInText;
    }

    public void setDisplayInText(boolean displayInText) {
        this.displayInText = displayInText;
    }

    public AbstractMessage getMessage() {
        return message;
    }

    public void setMessage(AbstractMessage message) {
        this.message = message;
    }

    public String displayURLOfPage() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();

        StringBuilder url = new StringBuilder();
        String requestUrl = req.getRequestURL().toString();
        String appUrl = ApplicationConfigurationManager.instance().getApplicationUrl();
        String reqUri = req.getRequestURI();

        if (!requestUrl.contains(appUrl)) {
            appUrl = appUrl.substring(0, appUrl.lastIndexOf("/"));
            url.append(appUrl).append(reqUri);
            requestUrl = url.toString();
        }
        return requestUrl;
    }

    public void retrieveMessage(String channelType) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String messageId = params.get("id");
        if (messageId == null || messageId.trim().length() < 1) {
            return;
        }
        try {
            int i = Integer.valueOf(messageId);
            message = ProxyDAO.getMessageByID(i);
        } catch (Exception e) {
            message = null;
            LOG.error("Failed to convert string to int : " + e.getCause());
            return;
        }

        ChannelType type = ChannelType.valueOf(channelType);
        if (message.getChannelType() != null && message.getChannelType() != type) {
            Redirect redirect = Redirect.instance();
            redirect.setParameter("id", messageId);

            String viewId = "";
            if (message instanceof RawMessage) {
                viewId = "/messages/raw.xhtml";
            }
            if (message instanceof HL7Message) {
                viewId = "/messages/hl7.xhtml";
            }
            if (message instanceof DicomMessage) {
                viewId = "/messages/dicom.xhtml";
            }
            if (message instanceof SyslogMessage) {
                viewId = "/messages/syslog.xhtml";
            }
            if (message instanceof HTTPMessage) {
                viewId = "/messages/http.xhtml";
            }

            redirect.setViewId(viewId);
            redirect.execute();
        }
    }

    public byte[] dicom2txt(DicomMessage dicomMessage) throws IOException {

        File dcmFile = dicomMessage.getFile();
        if (dcmFile.exists()) {

            String dcmDump = ApplicationConfigurationManager.instance().getDcmDumpPath();
            CommandLine commandLine = new CommandLine(dcmDump);
            commandLine.addArgument(dcmFile.getCanonicalPath());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            DefaultExecutor executor = new DefaultExecutor();
            executor.setStreamHandler(streamHandler);
            try {
                executor.execute(commandLine);
                byte[] result = outputStream.toByteArray();
                return result;
            } catch (ExecuteException e) {
                LOG.error("DCMTK is not installed or cannont execute " + dcmDump + " : " + e);
                FacesMessages.instance().add("DCMTK is not installed or cannot execute " + dcmDump + " : ");
            } catch (IOException e) {
                LOG.error("DCMTK is not installed or cannont open " + dcmDump + " : " + e);
                FacesMessages.instance().add("DCMTK is not installed or cannot open " + dcmDump + " : ");
            }
        } else {
            LOG.error("File doesn't exist !");
            FacesMessages.instance().add("File doesn't exist !");
        }
        return null;
    }

    public void downloadFilesForConnection() {
        int connectionId = message.getConnection().getId();
        HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + connectionId + ".zip" + "\"");

        File zipFile = new File("/tmp/archiveForConnection_" + connectionId + ".zip");
        byte[] buffer = new byte[8192];

        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        FileInputStream in = null;
        InputStream messageStream = null;
        FileOutputStream out = null;
        ServletOutputStream op = null;

        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);
            AbstractMessageQuery q = new AbstractMessageQuery();
            q.connection().id().eq(connectionId);
            List<AbstractMessage> messagesList = q.getList();
            for (AbstractMessage msg : messagesList) {
                if (msg.getMessageReceived() != null) {
                    LOG.info("Adding : " + msg.getId() + " in zip file " + connectionId + ".zip");
                    messageStream = msg.getMessageReceivedStream();
                    File tempFile = new File("/tmp/" + msg.getId().toString());
                    out = new FileOutputStream(tempFile);
                    IOUtils.copyLarge(messageStream, out);
                    in = new FileInputStream(tempFile.getCanonicalFile());

                    // begin writing a new ZIP entry, positions the stream to the start of the entry data
                    zos.putNextEntry(new ZipEntry(tempFile.getName()));
                    while (true) {

                        int length = in.read(buffer);
                        if (length < 0) {
                            break;
                        }
                        zos.write(buffer, 0, length);
                    }
                    zos.closeEntry();

                    // close the InputStream
                    in.close();
                    messageStream.close();
                    out.close();
                    tempFile.delete();
                }
            }
            // close the ZipOutputStream
            zos.close();
            fos.close();

            //Send the zip
            op = response.getOutputStream();
            Path path = Paths.get(zipFile.getPath());
            op.write(Files.readAllBytes(path));
            op.flush();
            op.close();

            facesContext.responseComplete();

            zipFile.delete();
        } catch (Exception e) {
            LOG.error("" + e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (fos != null) {
                    fos.close();
                }
                if (zos != null) {
                    zos.close();
                }
                if (messageStream != null) {
                    messageStream.close();
                }
                if (out != null) {
                    out.close();
                }
                if (op != null) {
                    op.close();
                }
            } catch (Exception e) {
                LOG.error("" + e.getMessage());
            }
        }
    }

    public void downloadFile(boolean inline) {
        HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();
        InputStream messageStream = message.getMessageReceivedStream();
        ServletOutputStream servletOutputStream = null;
        try {
            response.setContentType("text/plain");
            servletOutputStream = response.getOutputStream();
            if (!inline) {
                response.setHeader("Content-Disposition", "attachment;filename=\"" + message.getId() + "\"");
            }

            if (inline && message.getChannelType().getDiscriminator().equals("DicomMessage")) {
                byte[] result = dicom2txt((DicomMessage) message);
                if (result != null) {
                    response.setContentLength(result.length);
                    ByteArrayInputStream input = new ByteArrayInputStream(result);
                    IOUtils.copyLarge(input, servletOutputStream);
                } else {
                    IOUtils.write("Failed to download file, contact your administrator", servletOutputStream);
                }
            } else {
                response.setContentLength(message.getMessageLength());
                byte[] buf = new byte[8192];
                while (true) {

                    int length = messageStream.read(buf);
                    if (length < 0) {
                        break;
                    }
                    servletOutputStream.write(buf, 0, length);
                }
            }

        } catch (Exception e) {
            LOG.error("Failed to download file", e);
            FacesMessages.instance().add("Failed to download file");
            try {
                IOUtils.write("Failed to download file, contact your administrator", servletOutputStream);
            } catch (IOException e1) {
                LOG.error("Failed to send error to browser", e);
            }
        } finally {
            try {
                if (messageStream != null) {
                    messageStream.close();
                }
            } catch (IOException ignore) {
            }
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                }
            } catch (IOException ignore) {
            }

            facesContext.responseComplete();
        }
    }


    public String getMessageIndex(AbstractMessage message) {
        EntityManager em = EntityManagerService.provideEntityManager();
        String index = message.getIndex();
        if (index != null) {
            return index;
        } else {
            if (message != null && message.getConnection() != null && message.getConnection().getMessages() != null) {
                List<AbstractMessage> messages = message.getConnection().getSortedMessages();
                int i = 1;
                for (AbstractMessage abstractMessage : messages) {
                    if (abstractMessage.getId().equals(message.getId())) {
                        message.setIndex(Integer.toString(i));
                        em.merge(message);
                        em.flush();
                        return Integer.toString(i);
                    }
                    i++;
                }
            }
            message.setIndex("?");
            return "?";
        }
    }

    public Integer getMessageIndex() {
        String index = message.getIndex();
        if (index != null) {
            return Integer.valueOf(index);
        } else {
            AbstractMessage message = getMessage();
            if (message != null && message.getConnection() != null && message.getConnection().getMessages() != null) {
                List<AbstractMessage> messages = message.getConnection().getSortedMessages();
                int i = 1;
                for (AbstractMessage abstractMessage : messages) {
                    if (abstractMessage.getId().equals(message.getId())) {
                        messageIndex = i;
                        return messageIndex;
                    }
                    i++;
                }
            }
            return messageIndex;
        }
    }

    public void setMessageIndex(Integer messageIndex) {
        this.messageIndex = messageIndex;
    }

    public Integer getMessageIndexMax() {
        AbstractMessage message = getMessage();
        if (message != null && message.getConnection() != null && message.getConnection().getMessages() != null) {
            List<AbstractMessage> messages = message.getConnection().getSortedMessages();
            int i = 1;
            for (AbstractMessage abstractMessage : messages) {
                messageIndexMax = i++;
            }
        }
        return messageIndexMax;
    }

    public AbstractMessage getMessageByIndex() {
        return message.getConnection().getSortedMessages().get(messageIndex - 1);
    }

    public String prettyFormat(String input) {
        FormatBean fb = new FormatBean();
        String res = fb.prettyFormat(input, 2);
        return res;
    }

    public Boolean shouldDisplayMessageContent() {
        FormatBean fb = new FormatBean();
        return fb.shouldDisplayMessageContent(message);
    }

    public Boolean shouldDisplayMtomMessageContent() {
        FormatBean fb = new FormatBean();
        return fb.shouldDisplayMtomMessageContent(message);
    }

    public Boolean shouldDisplayDicomMessageContent() {
        FormatBean fb = new FormatBean();
        return fb.shouldDisplayDicomMessageContent(message);
    }

    public Boolean shouldDisplayHL7MessageContent() {
        FormatBean fb = new FormatBean();
        return fb.shouldDisplayHL7MessageContent(message);
    }

    public Boolean shouldDisplayRawMessageContent() {
        FormatBean fb = new FormatBean();
        return fb.shouldDisplayRawMessageContent(message, isDisplayInText());
    }

    public Boolean isDownloadable() {
        if (message == null || message.getMessageReceived() == null) {
            return false;
        }
        if (message.getMessageReceived().length > 0) {
            return true;
        }
        return false;
    }

    public String getNanos(AbstractMessage message) {
        if (message.getDateReceived() != null) {
            if (message.getDateReceived() instanceof Timestamp) {
                int nanos = ((Timestamp) message.getDateReceived()).getNanos();
                int micros = nanos / 1000;
                return StringUtils.leftPad(Integer.toString(micros), 6, '0');
            }
        }
        return "";
    }

    public String escape(String source) {
        String result = StringEscapeUtils.escapeHtml(source);
        return "<pre>" + result + "</pre>";
    }

    public void validate() {
        ApplicationConfigurationManagerLocal manager = ApplicationConfigurationManager.instance();
        String evsClientUrl = manager.getEvsClientUrl();
        String toolOid = manager.getProxyOid();
        EVSClientServletConnector.sendToValidation(message, extCtx, evsClientUrl, toolOid);
    }

    public void intResult() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String oid = null;
        message = getMessage();
        ArrayList<String> resultOidList = new ArrayList<String>();
        oid = EVSClientResults.getResultOidFromUrl(urlParams);
        if (oid != null) {
            if (message.getResultOid() != null) {
                resultOidList = message.getResultOid();
            }
            if (!(resultOidList.contains(oid))) {
                resultOidList.add(oid);
                message.setResultOid(resultOidList);
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                message = entityManager.merge(message);
                entityManager.flush();
            } else {
                LOG.error("This oid is already saved !");
            }
        }
    }

    public void removeResult(String oid) {
        message = getMessage();
        ArrayList<String> resultOidList = new ArrayList<String>();

        if (message.getResultOid() != null) {
            resultOidList = message.getResultOid();
        }
        if ((resultOidList.contains(oid))) {
            resultOidList.remove(oid);
            message.setResultOid(resultOidList);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            message = entityManager.merge(message);
            entityManager.flush();
        } else {
            LOG.error("This oid is already remove !");
        }
    }

    public String getValidationStatus(String oid) {
        return EVSClientResults.getValidationStatus(oid, getValidatorUrl());
    }

    public String getValidationDate(String oid) {
        return EVSClientResults.getValidationDate(oid, getValidatorUrl());
    }

    public String getValidationPermanentLink(String oid) {
        return EVSClientResults.getValidationPermanentLink(oid, getValidatorUrl());
    }

    public void redirectToMessage(AbstractMessage message) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

        try {
            if (message.getId() == null) {
                LOG.error("No id define for this message");
            }
            if (message instanceof HL7Message) {
                ec.redirect(ec.getRequestContextPath() + "/messages/hl7.seam?id=" + message.getId());
            }
            if (message instanceof RawMessage) {
                ec.redirect(ec.getRequestContextPath() + "/messages/raw.seam?id=" + message.getId());
            }
            if (message instanceof DicomMessage) {
                ec.redirect(ec.getRequestContextPath() + "/messages/dicom.seam?id=" + message.getId());
            }
            if (message instanceof SyslogMessage) {
                ec.redirect(ec.getRequestContextPath() + "/messages/syslog.seam?id=" + message.getId());
            }
            if (message instanceof HTTPMessage) {
                ec.redirect(ec.getRequestContextPath() + "/messages/http.seam?id=" + message.getId());
            }
        } catch (IOException e) {
            LOG.error("" + e);
        }
    }

    public String Base64Decoding(String stringToDecode) {

        LOG.info("Base64 Encoded String : " + stringToDecode);
        byte msg[] = new byte[50];
        msg = stringToDecode.getBytes(StandardCharsets.UTF_8);
        // decoding byte array into base64
        byte[] decoded = Base64.decodeBase64(msg);
        String result = new String(decoded, StandardCharsets.UTF_8);
        LOG.info("Base 64 Decoded  String : " + new String(decoded, StandardCharsets.UTF_8));

        return result;
    }

    public void retrieveMessage2() {

        String messageId = getMessage().getId().toString();
        if (messageId == null || messageId.trim().length() < 1) {
            return;
        }
        try {
            int i = Integer.valueOf(messageId);
            message = ProxyDAO.getMessageByID(i);
        } catch (Exception e) {
            message = null;
        }

        Redirect redirect = Redirect.instance();
        redirect.setParameter("id", messageId);

        String viewId = "";
        if (message instanceof RawMessage) {
            viewId = "/messages/raw.xhtml";
        }
        if (message instanceof HL7Message) {
            viewId = "/messages/hl7.xhtml";
        }
        if (message instanceof DicomMessage) {
            viewId = "/messages/dicom.xhtml";
        }
        if (message instanceof SyslogMessage) {
            viewId = "/messages/syslog.xhtml";
        }
        if (message instanceof HTTPMessage) {
            viewId = "/messages/http.xhtml";
        }
        redirect.setViewId(viewId);
        redirect.execute();
    }

    private String getValidatorUrl() {
        if (this.validatorUrl == null) {
            this.validatorUrl = ApplicationConfigurationManager.instance().getEvsClientUrl();
        }
        return this.validatorUrl;
    }

    public boolean isRawMessage() {
        if (message instanceof RawMessage) {
            return true;
        }
        return false;
    }

}
