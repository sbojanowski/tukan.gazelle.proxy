/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.evsclient.connector.utils.PartSourceUtils;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.Inet4Address;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

@Entity
@Table(name = "pxy_abstract_message")
@Inheritance
@DiscriminatorColumn(name = "pxy_abstract_message_type")
@SequenceGenerator(name = "pxy_message_sequence", sequenceName = "pxy_message_id_seq", allocationSize = 1)
public abstract class AbstractMessage implements Comparable<AbstractMessage>, Serializable, EVSClientValidatedObject {

    protected static final Charset UTF_8 = Charset.forName("UTF-8");
    protected static Logger log = LoggerFactory.getLogger(AbstractMessage.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pxy_message_sequence")
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_received")
    private Date dateReceived;

    @Column(name = "from_ip")
    private String fromIP;

    @Column(name = "local_port")
    private int localPort;

    @Column(name = "proxy_port")
    private int proxyPort;

    @Column(name = "to_ip")
    private String toIP;

    @Column(name = "remote_port")
    private int remotePort;

    @ManyToOne(fetch = FetchType.LAZY)
    private Connection connection;

    @Column(name = "proxy_side")
    private ProxySide proxySide;

    @Column(name = "messageReceived")
    @Basic(fetch = FetchType.LAZY)
    private byte[] messageReceived;

    @Column(name = "result_oid")
    private ArrayList<String> resultOid = new ArrayList<String>();

    @Column(name = "index")
    private String index;

    @Column(name = "index_int")
    private int indexInt;

    public AbstractMessage() {
        super();
    }

    public AbstractMessage(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
                           Integer remotePort, ProxySide proxySide) {
        this.dateReceived = timestamp;
        setFromIP(fromIP);
        setToIP(toIP);
        this.localPort = localPort;
        this.proxyPort = proxyPort;
        this.remotePort = remotePort;
        this.localPort = localPort;
        this.proxySide = proxySide;

    }

    public int getIndexInt() {
        return indexInt;
    }

    public void setIndexInt(int index_int) {
        this.indexInt = index_int;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
        if (index.equals("?")) {
            setIndex("0");
        }
        setIndexInt(Integer.valueOf(index));
    }

    public ArrayList<String> getResultOid() {
        return resultOid;
    }

    public void setResultOid(ArrayList<String> resultOid) {
        this.resultOid = resultOid;
    }

    public byte[] getMessageReceived() {
        return ArrayUtils.clone(messageReceived);
    }

    public void setMessageReceived(byte[] messageReceived) {
        this.messageReceived = ArrayUtils.clone(messageReceived);
    }

    public String getMessageReceivedAsString() {
        return new String(messageReceived, UTF_8);
    }

    public void setMessageReceivedAsString(String messageReceived) {
        this.messageReceived = messageReceived.getBytes(UTF_8);
    }

    public abstract ChannelType getChannelType();

    @Override
    public String getType() {
        return getChannelType().name();
    }

    public String getMessageReceivedWrapped() {
        if (messageReceived == null) {
            return "";
        }
        String sToUse = getMessageReceivedAsString();

        return WordUtils.wrap(sToUse.substring(0, Math.min(sToUse.length(), 200)), 60) + "...";
    }

    public String getWholeMessageReceivedWrapped() {
        if (messageReceived == null) {
            return "";
        }
        String sToUse = getMessageReceivedAsString();

        return WordUtils.wrap(sToUse, 50);
    }

    public String getDecodedMessageReceived() {
        if (messageReceived == null) {
            return "";
        }
        String sToUse = getMessageReceivedAsString();

        return sToUse;
    }

    public String getInfoGUI() {
        String sToUse = getMessageReceivedAsString();
        if (sToUse.length() > 20) {
            sToUse = sToUse.substring(0, 20) + "...";
        }
        return StringEscapeUtils.escapeHtml(sToUse);
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getFromIP() {
        return fromIP;
    }

    public void setFromIP(String fromIP) {
        try {
            Inet4Address address = (Inet4Address) Inet4Address.getByName(fromIP);
            this.fromIP = address.getHostAddress();
        } catch (Exception e) {
            this.fromIP = fromIP;
        }
    }

    public String getToIP() {
        return toIP;
    }

    public void setToIP(String toIP) {
        try {
            Inet4Address address = (Inet4Address) Inet4Address.getByName(toIP);
            this.toIP = address.getHostAddress();
        } catch (Exception e) {
            this.toIP = toIP;
        }
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProxySide getProxySide() {
        return proxySide;
    }

    public void setProxySide(ProxySide proxySide) {
        this.proxySide = proxySide;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateReceived == null) ? 0 : dateReceived.hashCode());
        result = prime * result + ((fromIP == null) ? 0 : fromIP.hashCode());
        result = prime * result + localPort;
        result = prime * result + Arrays.hashCode(messageReceived);
        result = prime * result + proxyPort;
        result = prime * result + ((proxySide == null) ? 0 : proxySide.hashCode());
        result = prime * result + remotePort;
        result = prime * result + ((toIP == null) ? 0 : toIP.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractMessage other = (AbstractMessage) obj;
        if (dateReceived == null) {
            if (other.dateReceived != null) {
                return false;
            }
        } else if (!dateReceived.equals(other.dateReceived)) {
            return false;
        }
        if (fromIP == null) {
            if (other.fromIP != null) {
                return false;
            }
        } else if (!fromIP.equals(other.fromIP)) {
            return false;
        }
        if (localPort != other.localPort) {
            return false;
        }
        if (!Arrays.equals(messageReceived, other.messageReceived)) {
            return false;
        }
        if (proxyPort != other.proxyPort) {
            return false;
        }
        if (proxySide != other.proxySide) {
            return false;
        }
        if (remotePort != other.remotePort) {
            return false;
        }
        if (toIP == null) {
            if (other.toIP != null) {
                return false;
            }
        } else if (!toIP.equals(other.toIP)) {
            return false;
        }
        return true;
    }

    public int getMessageLength() {
        if (messageReceived == null) {
            return 0;
        } else {
            return messageReceived.length;
        }
    }

    public InputStream getMessageReceivedStream() {
        return new ByteArrayInputStream(messageReceived);
    }

    public PartSource getPartSource(String messageType) {
        return new ByteArrayPartSource(messageType, messageReceived);
    }

    @Override
    public PartSource getPartSource() {
        return PartSourceUtils.buildEncoded64PartSource(messageReceived, getType());
    }

    @Override
    public String getUniqueKey() {
        return Integer.toString(this.id);
    }

    @Override
    public int compareTo(AbstractMessage o) {
        if (getDateReceived().equals(o.getDateReceived())) {
            return o.getId().compareTo(getId());
        }
        return getDateReceived().compareTo(o.getDateReceived());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AbstractMessage [id=");
        builder.append(id);
        builder.append(", dateReceived=");
        builder.append(dateReceived);
        builder.append(", fromIP=");
        builder.append(fromIP);
        builder.append(", localPort=");
        builder.append(localPort);
        builder.append(", proxyPort=");
        builder.append(proxyPort);
        builder.append(", toIP=");
        builder.append(toIP);
        builder.append(", remotePort=");
        builder.append(remotePort);
        builder.append(", connection=");
        builder.append(connection);
        builder.append(", proxySide=");
        builder.append(proxySide);
        builder.append(", messageReceived=");
        builder.append(getInfoGUI());
        builder.append("]");
        return builder.toString();
    }

}
