package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.apache.commons.io.HexDump;
import org.apache.commons.lang.StringEscapeUtils;
import org.jboss.seam.annotations.Name;

import javax.persistence.Entity;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;

@Entity
@Name("rawMessage")
public class RawMessage extends AbstractMessage {

    /**
     *
     */
    private static final long serialVersionUID = -6892672726705930948L;

    public RawMessage() {
        super();
    }

    public RawMessage(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
            Integer remotePort, ProxySide proxySide) {
        super(timestamp, fromIP, localPort, proxyPort, toIP, remotePort, proxySide);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.RAW;
    }

    public Object getRawMessageReceivedAsString() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            HexDump.dump(getMessageReceived(), 0, stream, 0);
            return new String(stream.toByteArray(),StandardCharsets.UTF_8);
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error("" + e);
        } catch (IllegalArgumentException e) {
            log.error("" + e);
        } catch (IOException e) {
            log.error("" + e);
        }

        return null;
    }

    @Override
    public String getInfoGUI() {
        String message = getMessageReceivedAsString();
        if (message.length() > 20) {
            message = message.substring(0, 20) + "...";
        }
        message = makeStringPrintable(message);
        return StringEscapeUtils.escapeHtml(message);
    }

    public String getDumpInText() {
        String message = getMessageReceivedAsString();
        message = makeStringPrintable(message);
        return StringEscapeUtils.escapeHtml(message);
    }

    public String makeStringPrintable(String str) {
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            str = str.replace(str.charAt(i), makeAsciiPrintable(str.charAt(i)));
        }
        return str;
    }

    public char makeAsciiPrintable(char ch) {
        return ch >= 32 && ch < 127 ? ch : '.';
    }
}
