package net.ihe.gazelle.proxy.util;

/**
 * Holds constants used in Gazelle Proxy
 *
 * @author tnabeel
 */
public abstract class ProxyConstants {

    public static final String JMS_PARAM_MESSAGE_ID = "msgId";
    public static final String JMS_PARAM_CONNECTION_ID = "connId";
    public static final String JMS_PARAM_PROXY_TYPE = "proxyType";
    public static final String JMS_PARAM_PROXY_HOST_NAME = "proxyHostName";
    public static final String JMS_PARAM_MESSAGE_DIRECTION = "messageDirection";
    public static final String JMS_PARAM_DATE_RECEIVED = "dateReceived";
    public static final String JMS_PARAM_SENDER_IP_ADDRESS = "senderIPAddress";
    public static final String JMS_PARAM_SENDER_PORT = "senderPort";
    public static final String JMS_PARAM_PROXY_PORT = "proxyPort";
    public static final String JMS_PARAM_RECEIVER_IP_ADDRESS = "receiverIPAddress";
    public static final String JMS_PARAM_RECEIVER_PORT = "receiverPort";
    public static final String JMS_PARAM_MESSAGE_CONTENTS = "messageContents";

}
