package net.ihe.gazelle.proxy.factory;

import net.ihe.gazelle.proxy.listeners.HL7EventListener;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyFactory;
import net.ihe.gazelle.proxy.netty.protocols.hl7.HL7Proxy;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.proxy.netty.ProxyFactory.class)
public class HL7ProxyFactory implements ProxyFactory {

    @Override
    public Proxy<?, ?> newProxy(ConnectionConfig connectionConfig) {
        HL7EventListener proxyEventListener = new HL7EventListener();
        return new HL7Proxy(proxyEventListener, connectionConfig);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.HL7;
    }

}
