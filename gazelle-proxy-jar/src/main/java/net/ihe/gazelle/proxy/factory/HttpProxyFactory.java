package net.ihe.gazelle.proxy.factory;

import net.ihe.gazelle.proxy.listeners.HttpEventListener;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyFactory;
import net.ihe.gazelle.proxy.netty.protocols.http.HttpProxy;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.proxy.netty.ProxyFactory.class)
public class HttpProxyFactory implements ProxyFactory {

    @Override
    public Proxy<?, ?> newProxy(ConnectionConfig connectionConfig) {
        HttpEventListener proxyEventListener = new HttpEventListener();
        return new HttpProxy(proxyEventListener, connectionConfig);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.HTTP;
    }

}
