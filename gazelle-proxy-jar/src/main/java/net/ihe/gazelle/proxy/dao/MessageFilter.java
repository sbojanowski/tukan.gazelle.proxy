package net.ihe.gazelle.proxy.dao;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;

public interface MessageFilter<T extends AbstractMessage> {

    void appendFilters(HQLQueryBuilder<T> criteria);

}
