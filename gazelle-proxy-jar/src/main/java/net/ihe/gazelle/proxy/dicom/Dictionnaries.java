package net.ihe.gazelle.proxy.dicom;

import com.pixelmed.dicom.SOPClass;
import com.pixelmed.network.MessageServiceElementCommand;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Dictionnaries {

    public static final Dictionnaries INSTANCE = new Dictionnaries();

    private Map<String, String> sopClassByUID;

    private Map<String, String> commandFields;

    public Dictionnaries() {
        super();
        createDescriptionsByUID();
        createCommandFields();
    }

    public Map<String, String> getCommandFields() {
        return commandFields;
    }

    public Map<String, String> getSopClassByUID() {
        return sopClassByUID;
    }

    private void createCommandFields() {
        commandFields = new TreeMap<String, String>();

        int[] commandFieldsIds = new int[] { 0x0001, 0x8001, 0x0010, 0x8010, 0x0020, 0x8020, 0x0021, 0x8021, 0x0030,
                0x8030, 0x0100, 0x8100, 0x0110, 0x8110, 0x0120, 0x8120, 0x0130, 0x8130, 0x0140, 0x8140, 0x0150, 0x8150,
                0x0FFF };

        for (int command : commandFieldsIds) {
            commandFields.put(MessageServiceElementCommand.toString(command),
                    MessageServiceElementCommand.toString(command));
        }

    }

    private void createDescriptionsByUID() {
        sopClassByUID = new TreeMap<String, String>();

        sopClassByUID.put(SOPClass.Verification, "Verification");

        sopClassByUID.put(SOPClass.ComputedRadiographyImageStorage, "Computed Radiography Image Storage");
        sopClassByUID
                .put(SOPClass.DigitalXRayImageStorageForPresentation, "Digital X-Ray Image Storage (For Presentation)");
        sopClassByUID
                .put(SOPClass.DigitalXRayImageStorageForProcessing, "Digital X-Ray Image Storage (For Processing)");
        sopClassByUID.put(SOPClass.DigitalMammographyXRayImageStorageForPresentation,
                "Digital Mammography X-Ray Image Storage (For Presentation)");
        sopClassByUID.put(SOPClass.DigitalMammographyXRayImageStorageForProcessing,
                "Digital Mammography X-Ray Image Storage (For Processing)");
        sopClassByUID.put(SOPClass.DigitalIntraoralXRayImageStorageForPresentation,
                "Digital Intraoral X-Ray Image Storage (For Presentation)");
        sopClassByUID.put(SOPClass.DigitalIntraoralXRayImageStorageForProcessing,
                "Digital Intraoral X-Ray Image Storage (For Processing)");
        sopClassByUID.put(SOPClass.CTImageStorage, "CT Image Storage");
        sopClassByUID
                .put(SOPClass.UltrasoundMultiframeImageStorageRetired, "Ultrasound Multiframe Image Storage (Retired)");
        sopClassByUID.put(SOPClass.UltrasoundMultiframeImageStorage, "Ultrasound Multiframe Image Storage");
        sopClassByUID.put(SOPClass.MRImageStorage, "MR Image Storage");
        sopClassByUID.put(SOPClass.EnhancedMRImageStorage, "Enhanced MR Image Storage");
        sopClassByUID.put(SOPClass.EnhancedMRColorImageStorage, "Enhanced MR Color Image Storage");
        sopClassByUID.put(SOPClass.NuclearMedicineImageStorageRetired, "Nuclear Medicine Image Storage (Retired)");
        sopClassByUID.put(SOPClass.UltrasoundImageStorageRetired, "Ultrasound Image Storage (Retired)");
        sopClassByUID.put(SOPClass.UltrasoundImageStorage, "Ultrasound Image Storage");
        sopClassByUID.put(SOPClass.EnhancedUSVolumeStorage, "Enhanced US Volume Storage");
        sopClassByUID.put(SOPClass.SecondaryCaptureImageStorage, "Secondary Capture Image Storage");
        sopClassByUID.put(SOPClass.MultiframeSingleBitSecondaryCaptureImageStorage,
                "Multiframe Single Bit Secondary Capture Image Storage");
        sopClassByUID.put(SOPClass.MultiframeGrayscaleByteSecondaryCaptureImageStorage,
                "Multiframe Grayscale Byte Secondary Capture Image Storage");
        sopClassByUID.put(SOPClass.MultiframeGrayscaleWordSecondaryCaptureImageStorage,
                "Multiframe Grayscale Word Secondary Capture Image Storage");
        sopClassByUID.put(SOPClass.MultiframeTrueColorSecondaryCaptureImageStorage,
                "Multiframe True Color Secondary Capture Image Storage");
        sopClassByUID.put(SOPClass.XRayAngiographicImageStorage, "X-Ray Angiographic Image Storage");
        sopClassByUID.put(SOPClass.EnhancedXAImageStorage, "Enhanced XA Image Storage");
        sopClassByUID.put(SOPClass.XRayRadioFlouroscopicImageStorage, "X-Ray Radio Flouroscopic Image Storage");
        sopClassByUID.put(SOPClass.EnhancedXRFImageStorage, "Enhanced XRF Image Storage");
        sopClassByUID.put(SOPClass.XRayAngiographicBiplaneImageStorage, "X-Ray Angiographic Biplane Image Storage");
        sopClassByUID.put(SOPClass.XRay3DAngiographicImageStorage, "X-Ray 3D Angiographic Image Storage");
        sopClassByUID.put(SOPClass.XRay3DCraniofacialImageStorage, "X-Ray 3D Craniofacial Image Storage");
        sopClassByUID.put(SOPClass.BreastTomosynthesisImageStorage, "Breast Tomosynthesis Image Storage");
        sopClassByUID.put(SOPClass.NuclearMedicineImageStorage, "Nuclear Medicine Image Storage");
        sopClassByUID.put(SOPClass.VisibleLightEndoscopicImageStorage, "Visible Light Endoscopic Image Storage");
        sopClassByUID.put(SOPClass.VideoEndoscopicImageStorage, "Video Endoscopic Image Storage");
        sopClassByUID.put(SOPClass.VisibleLightMicroscopicImageStorage, "Visible Light Microscopic Image Storage");
        sopClassByUID.put(SOPClass.VideoMicroscopicImageStorage, "Video Microscopic Image Storage");
        sopClassByUID.put(SOPClass.VisibleLightSlideCoordinatesMicroscopicImageStorage,
                "Visible Light Slide Coordinates Microscopic Image Storage");
        sopClassByUID.put(SOPClass.VisibleLightPhotographicImageStorage, "Visible Light Photographic Image Storage");
        sopClassByUID.put(SOPClass.VideoPhotographicImageStorage, "Video Photographic Image Storage");
        sopClassByUID.put(SOPClass.OphthalmicPhotography8BitImageStorage, "Ophthalmic Photography 8 Bit Image Storage");
        sopClassByUID
                .put(SOPClass.OphthalmicPhotography16BitImageStorage, "Ophthalmic Photography 16 Bit Image Storage");
        sopClassByUID.put(SOPClass.OphthalmicTomographyImageStorage, "Ophthalmic Tomography Image Storage");
        sopClassByUID.put(SOPClass.VLWholeSlideMicroscopyImageStorage, "VL Whole Slide Microscopy Image Storage");
        sopClassByUID.put(SOPClass.PETImageStorage, "PET Image Storage");
        sopClassByUID.put(SOPClass.EnhancedPETImageStorage, "Enhanced PET Image Storage");
        sopClassByUID.put(SOPClass.RTImageStorage, "RT Image Storage");
        sopClassByUID
                .put(SOPClass.IVOCTImageStorageForPresentation, "Intravascular OCT Image Storage (For Presentation)");
        sopClassByUID.put(SOPClass.IVOCTImageStorageForProcessing, "Intravascular OCT Image Storage (For Processing)");

        sopClassByUID.put(SOPClass.MediaStorageDirectoryStorage, "Media Storage Directory Storage");
        sopClassByUID.put(SOPClass.BasicTextSRStorage, "Basic Text SR Storage");
        sopClassByUID.put(SOPClass.EnhancedSRStorage, "Enhanced SR Storage");
        sopClassByUID.put(SOPClass.ComprehensiveSRStorage, "Comprehensive SR Storage");
        sopClassByUID.put(SOPClass.MammographyCADSRStorage, "Mammography CAD SR Storage");
        sopClassByUID.put(SOPClass.ChestCADSRStorage, "Chest CAD SR Storage");
        sopClassByUID.put(SOPClass.ProcedureLogStorage, "Procedure Log Storage");
        sopClassByUID.put(SOPClass.XRayRadiationDoseSRStorage, "X-Ray Radiation Dose SR Storage");
        sopClassByUID.put(SOPClass.ColonCADSRStorage, "Colon CAD SR Storage");
        sopClassByUID.put(SOPClass.ImplantationPlanSRStorage, "Implantation Plan SR Storage");
        sopClassByUID.put(SOPClass.MacularGridThicknessAndVolumeReportStorage,
                "Macular Grid Thickness and Volume Report Storage");
        sopClassByUID.put(SOPClass.KeyObjectSelectionDocumentStorage, "Key Object Selection Document Storage");

        sopClassByUID.put(SOPClass.GrayscaleSoftcopyPresentationStateStorage,
                "Grayscale Softcopy Presentation State Storage");
        sopClassByUID.put(SOPClass.ColorSoftcopyPresentationStateStorage, "Color Softcopy Presentation State Storage");
        sopClassByUID.put(SOPClass.PseudoColorSoftcopyPresentationStateStorage,
                "Pseudo-Color Softcopy Presentation State Storage");
        sopClassByUID
                .put(SOPClass.BlendingSoftcopyPresentationStateStorage, "Blending Softcopy Presentation State Storage");
        sopClassByUID.put(SOPClass.XAXRFGrayscaleSoftcopyPresentationStateStorage,
                "XA/XRF Grayscale Softcopy Presentation State Storage");

        sopClassByUID.put(SOPClass.TwelveLeadECGStorage, "Twelve Lead ECG Storage");
        sopClassByUID.put(SOPClass.GeneralECGStorage, "General ECG Storage");
        sopClassByUID.put(SOPClass.AmbulatoryECGStorage, "Ambulatory ECG Storage");
        sopClassByUID.put(SOPClass.HemodynamicWaveformStorage, "Hemodynamic Waveform Storage");
        sopClassByUID
                .put(SOPClass.CardiacElectrophysiologyWaveformStorage, "Cardiac Electrophysiology Waveform Storage");
        sopClassByUID.put(SOPClass.ArterialPulseWaveformStorage, "Arterial Pulse Waveform Storage");
        sopClassByUID.put(SOPClass.RespiratoryWaveformStorage, "Respiratory Waveform Storage");
        sopClassByUID.put(SOPClass.BasicVoiceStorage, "Basic Voice Storage");
        sopClassByUID.put(SOPClass.GeneralAudioWaveformStorage, "General Audio Waveform Storage");

        sopClassByUID.put(SOPClass.StandaloneOverlayStorage, "Standalone Overlay Storage");
        sopClassByUID.put(SOPClass.StandaloneCurveStorage, "Standalone Curve Storage");
        sopClassByUID.put(SOPClass.StandaloneModalityLUTStorage, "Standalone Modality LUT Storage");
        sopClassByUID.put(SOPClass.StandaloneVOILUTStorage, "Standalone VOI LUT Storage");
        sopClassByUID.put(SOPClass.StandalonePETCurveStorage, "Standalone PET Curve Storage");

        sopClassByUID.put(SOPClass.RTDoseStorage, "RT Dose Storage");
        sopClassByUID.put(SOPClass.RTStructureSetStorage, "RT Structure Set Storage");
        sopClassByUID.put(SOPClass.RTBeamsTreatmentRecordStorage, "RT Beams Treatment Record Storage");
        sopClassByUID.put(SOPClass.RTIonBeamsTreatmentRecordStorage, "RT Ion Beams Treatment Record Storage");
        sopClassByUID.put(SOPClass.RTPlanStorage, "RT Plan Storage");
        sopClassByUID.put(SOPClass.RTIonPlanStorage, "RT Ion Plan Storage");
        sopClassByUID.put(SOPClass.RTBrachyTreatmentRecordStorage, "RT Brachy Treatment Record Storage");
        sopClassByUID.put(SOPClass.RTTreatmentSummaryRecordStorage, "RT Treatment Summary Record Storage");
        sopClassByUID
                .put(SOPClass.RTBeamsDeliveryInstructionStorageTrial, "RT Beams Delivery Instruction Storage - Trial");
        sopClassByUID.put(SOPClass.RTBeamsDeliveryInstructionStorage, "RT Beams Delivery Instruction Storage");

        sopClassByUID.put(SOPClass.MRSpectroscopyStorage, "MR Spectroscopy Storage");

        sopClassByUID.put(SOPClass.RawDataStorage, "Raw Data Storage");

        sopClassByUID.put(SOPClass.SpatialRegistrationStorage, "Spatial Registration Storage");
        sopClassByUID.put(SOPClass.SpatialFiducialsStorage, "Spatial Fiducials Storage");
        sopClassByUID.put(SOPClass.DeformableSpatialRegistrationStorage, "Deformable Spatial Registration Storage");

        sopClassByUID.put(SOPClass.StereometricRelationshipStorage, "Stereometric Relationship Storage");
        sopClassByUID.put(SOPClass.RealWorldValueMappingStorage, "Real World Value Mapping Storage");

        sopClassByUID.put(SOPClass.EncapsulatedPDFStorage, "Encapsulated PDF Storage");
        sopClassByUID.put(SOPClass.EncapsulatedCDAStorage, "Encapsulated CDA Storage");

        sopClassByUID.put(SOPClass.StudyRootQueryRetrieveInformationModelFind,
                "Study Root Query Retrieve Information Model Find");
        sopClassByUID.put(SOPClass.StudyRootQueryRetrieveInformationModelMove,
                "Study Root Query Retrieve Information Model Move");

        sopClassByUID.put(SOPClass.SegmentationStorage, "Segmentation Storage");
        sopClassByUID.put(SOPClass.SurfaceSegmentationStorage, "Surface Segmentation Storage");

        sopClassByUID.put(SOPClass.LensometryMeasurementsStorage, "Lensometry Measurements Storage");
        sopClassByUID.put(SOPClass.AutorefractionMeasurementsStorage, "Autorefraction Measurements Storage");
        sopClassByUID.put(SOPClass.KeratometryMeasurementsStorage, "Keratometry Measurements Storage");
        sopClassByUID
                .put(SOPClass.SubjectiveRefractionMeasurementsStorage, "Subjective Refraction Measurements Storage");
        sopClassByUID.put(SOPClass.VisualAcuityMeasurementsStorage, "Visual Acuity Measurements Storage");
        sopClassByUID.put(SOPClass.SpectaclePrescriptionReportStorage, "Spectacle Prescription Report Storage");
        sopClassByUID.put(SOPClass.OphthalmicAxialMeasurementsStorage, "Ophthalmic Axial Measurements Storage");
        sopClassByUID.put(SOPClass.IntraocularLensCalculationsStorage, "Intraocular Lens Calculations Storage");
        sopClassByUID.put(SOPClass.OphthalmicVisualFieldStaticPerimetryMeasurementsStorage,
                "Ophthalmic Visual Field Static Perimetry Measurements Storage");
        sopClassByUID.put(SOPClass.OphthalmicThicknessMapStorage, "Ophthalmic Thickness Map Storage");

        sopClassByUID.put(SOPClass.ColorPaletteStorage, "Color Palette Storage");

        sopClassByUID.put(SOPClass.GenericImplantTemplateStorage, "Generic Implant Template Storage");
        sopClassByUID.put(SOPClass.ImplantAssemblyTemplateStorage, "Implant Assembly Template Storage");
        sopClassByUID.put(SOPClass.ImplantTemplateGroupStorage, "Implant Template Group Storage");

        sopClassByUID.put(SOPClass.BasicStructuredDisplayStorage, "Basic Structured Display Storage");

        sopClassByUID.put(SOPClass.PrivateGEPETRawDataStorage, "GE Private PET Raw Data Storage");
        sopClassByUID.put(SOPClass.PrivateGE3DModelStorage, "GE Private 3D Model Storage");
        sopClassByUID.put(SOPClass.PrivateGEeNTEGRAProtocolOrNMGenieStorage,
                "GE Private eNTEGRA Protocol or NM Genie Storage");
        sopClassByUID.put(SOPClass.PrivateGECollageStorage, "GE Private Collage Storage");

        sopClassByUID.put(SOPClass.PrivateSiemensCSANonImageStorage, "Siemens Private CSA Non-Image Storage");
        sopClassByUID.put(SOPClass.PrivateFujiCRImageStorage, "Fuji Private CR Image Storage");

        sopClassByUID.put(SOPClass.PrivatePhilipsCXImageStorage, "Philips Private CX Image Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsVolumeStorage, "Philips Private Volume Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsVolume2Storage, "Philips Private Volume 2 Storage");
        sopClassByUID.put(SOPClass.PrivatePhilips3DObjectStorage, "Philips Private 3D Object Storage");
        sopClassByUID.put(SOPClass.PrivatePhilips3DObject2Storage, "Philips Private 3D Object 2 Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsSurfaceStorage, "Philips Private Surface Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsSurface2Storage, "Philips Private Surface 2 Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsCompositeObjectStorage, "Philips Private Composite Object Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRCardioProfileStorage, "Philips Private MR Cardio Profile Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRCardioStorage, "Philips Private MR Cardio Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRCardio2Storage, "Philips Private MR Cardio 2 Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsCTSyntheticImageStorage, "Philips Private CT Synthetic Image Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRSyntheticImageStorage, "Philips Private MR Synthetic Image Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRCardioAnalysisStorage, "Philips Private MR Cardio Analysis Storage");
        sopClassByUID
                .put(SOPClass.PrivatePhilipsMRCardioAnalysis2Storage, "Philips Private MR Cardio Analysis Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsCXSyntheticImageStorage, "Philips Private CX Synthetic Image Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRSpectrumStorage, "Philips Private MR Spectrum Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRSeriesDataStorage, "Philips Private MR Series Data Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRColorImageStorage, "Philips Private MR Color Image Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsMRExamcardStorage, "Philips Private MR Examcard Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsSpecialisedXAStorage, "Philips Private Specialised XA Storage");
        sopClassByUID.put(SOPClass.PrivatePhilips3DPresentationStateStorage,
                "Philips Private 3D Presentation State Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsPerfusionStorage, "Philips Private Perfusion Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsPerfusionImageStorage, "Philips Private Perfusion Image Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsVRMLStorage, "Philips Private VRML Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsVolumeSetStorage, "Philips Private Volume Set Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsLiveRunStorage, "Philips Private Live Run Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsRunStorage, "Philips Private Run Storage");
        sopClassByUID.put(SOPClass.PrivatePhilipsReconstructionStorage, "Philips Private Reconstruction Storage");
        sopClassByUID.put(SOPClass.PrivatePMODMultiframeImageStorage, "PMOD Private Multiframe Image Storage");
        sopClassByUID.put(SOPClass.PrivatePixelMedLegacyConvertedEnhancedCTImageStorage,
                "Private PixelMed Legacy Converted Enhanced CT Image Storage");

        sopClassByUID.put(SOPClass.DICOSCTImageStorage, "DICOS CT Image Storage");
        sopClassByUID.put(SOPClass.DICOSDigitalXRayImageStorageForPresentation,
                "DICOS Digital X-Ray Image Storage - For Presentation");
        sopClassByUID.put(SOPClass.DICOSDigitalXRayImageStorageForProcessing,
                "DICOS Digital X-Ray Image Storage - For Processing");
        sopClassByUID.put(SOPClass.DICOSThreatDetectionReportStorage, "DICOS Threat Detection Report Storage");
        sopClassByUID.put(SOPClass.DICOS2DAITStorage, "DICOS 2D AIT Storage");
        sopClassByUID.put(SOPClass.DICOS3DAITStorage, "DICOS 3D AIT Storage");
        sopClassByUID.put(SOPClass.DICOSQuadrupoleResonanceStorage, "DICOS Quadrupole Resonance Storage");

        sopClassByUID = Collections.unmodifiableMap(sopClassByUID);
    }

}
