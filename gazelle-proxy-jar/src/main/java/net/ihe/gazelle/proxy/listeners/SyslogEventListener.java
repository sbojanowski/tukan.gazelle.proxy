package net.ihe.gazelle.proxy.listeners;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.model.message.RawMessage;
import net.ihe.gazelle.proxy.model.message.SyslogMessage;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import net.ihe.gazelle.proxy.netty.protocols.syslog.SyslogData;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class SyslogEventListener extends GazelleProxyEventListener<SyslogData, byte[]> {

    @Override
    public void onRequest(final Timestamp timeStamp, final SyslogData request, final String requesterIp,
            final int requesterPort, final int proxyProviderPort, final String responderIp, final int responderPort,
            final int requestChannelId, final int responseChannelId) {
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                        SyslogMessage syslogMessage = new SyslogMessage(timeStamp, requesterIp, requesterPort,
                                proxyProviderPort, responderIp, responderPort, request);
                        processMessage(entityManager, syslogMessage, requestChannelId, responseChannelId);
                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }
        }
    }

    @Override
    public void onResponse(final Timestamp timeStamp, final byte[] response, final String requesterIp,
            final int requesterPort, final int proxyProviderPort, final String responderIp, final int responderPort,
            final int requestChannelId, final int responseChannelId) {
        // Messages are coming from different threads!
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                        RawMessage rawmessage = new RawMessage(timeStamp, requesterIp, requesterPort, proxyProviderPort,
                                responderIp, responderPort, ProxySide.RESPONSE);
                        rawmessage.setMessageReceived(response);
                        processMessage(entityManager, rawmessage, requestChannelId, responseChannelId);
                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }
        }
    }
}
