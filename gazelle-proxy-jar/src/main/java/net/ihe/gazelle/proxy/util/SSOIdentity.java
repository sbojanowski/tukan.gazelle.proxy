package net.ihe.gazelle.proxy.util;

import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import org.apache.commons.lang.StringUtils;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.core.Events;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Startup;
import javax.faces.context.FacesContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import static org.jboss.seam.annotations.Install.APPLICATION;

@Name("org.jboss.seam.security.identity")
@Scope(ScopeType.SESSION)
@Install(precedence = APPLICATION)
@BypassInterceptors
@Startup
public class SSOIdentity extends Identity {

    public static final String AUTHENTICATED_USER = "org.jboss.seam.security.management.authenticatedUser";
    public static final String EVENT_USER_AUTHENTICATED = "org.jboss.seam.security.management.userAuthenticated";
    private static final long serialVersionUID = -631532323964539777L;
    private static final String SILENT_LOGIN = "org.jboss.seam.security.silentLogin";

    private static Logger log = LoggerFactory.getLogger(SSOIdentity.class);

    @Override
    public String login() {
        log.info("Start login...");
        try {
            preAuthenticate();
            if (ApplicationConfigurationManager.instance().isWorksWithoutCas() && !ApplicationConfigurationManager
                    .instance().getIpLogin()) {
                // Any user is granted as admin
                String username = "admin";
                acceptExternallyAuthenticatedPrincipal(new IpPrincipal(username));
                addRole("admin_role");
                getCredentials().setUsername(username);
                postAuthenticate();
                return "loggedIn";
            } else if (ApplicationConfigurationManager.instance().isWorksWithoutCas() && ApplicationConfigurationManager
                    .instance().getIpLogin()) {
                Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
                if (request instanceof ServletRequest) {
                    ServletRequest servletRequest = (ServletRequest) request;
                    String ipRegexp = ApplicationConfigurationManager.instance().getIpLoginAdmin();
                    if (ipRegexp == null) {
                        ipRegexp = "";
                    }
                    String remoteAddr = servletRequest.getRemoteAddr();
                    if (remoteAddr.matches(ipRegexp)) {
                        String username = "admin";
                        acceptExternallyAuthenticatedPrincipal(new IpPrincipal(username));
                        addRole("admin_role");
                        getCredentials().setUsername(username);
                        postAuthenticate();
                        return "loggedIn";
                    }
                }
            } else if (super.isLoggedIn()) {
                // If authentication has already occurred during this request
                // via a silent login, and login() is explicitly called then we
                // still want to raise the LOGIN_SUCCESSFUL event, and then
                // return.
                if (Contexts.isEventContextActive() && Contexts.getEventContext().isSet(SILENT_LOGIN)) {
                    if (Events.exists()) {
                        Events.instance().raiseEvent(EVENT_LOGIN_SUCCESSFUL);
                    }
                    return "loggedIn";
                }
                if (Events.exists()) {
                    Events.instance().raiseEvent(EVENT_ALREADY_LOGGED_IN);
                }
                return "loggedIn";
            }

            AttributePrincipal attributePrincipal = null;
            Principal casPrincipal = getCASPrincipal();
            if (casPrincipal instanceof AttributePrincipal) {
                attributePrincipal = (AttributePrincipal) casPrincipal;
            }

            if (attributePrincipal == null) {
                return super.login();
            } else if (casPrincipal.getName() != null) {
                preAuthenticate();

                String username = casPrincipal.getName();

                log.info("Found CAS principal for " + username + ": authenticated");
                Map attributes = attributePrincipal.getAttributes();
                Set<Map.Entry> entrySet = attributes.entrySet();
                for (Map.Entry object : entrySet) {
                    log.info("       " + object.getKey() + " = " + object.getValue());
                }

                acceptExternallyAuthenticatedPrincipal(casPrincipal);
                if (attributes.get("role_name") != null) {
                    String roles = (String) attributes.get("role_name");
                    StringTokenizer st = new StringTokenizer(roles, "[,]");
                    String role;
                    while (st.hasMoreElements()) {
                        role = (String) st.nextElement();
                        role = StringUtils.trimToNull(role);
                        if (role != null) {
                            addRole(role);
                        }
                    }
                }
                getCredentials().setUsername(username);
                postAuthenticate();
                return "loggedIn";

            }

        } catch (Throwable e) {
            unAuthenticate();
            throw new RuntimeException(e);
        }

        return null;
    }

    private Principal getCASPrincipal() {
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        final HttpSession session = request.getSession(false);
        final Assertion assertion = (Assertion) (session == null ?
                request.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION) :
                session.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION));
        return assertion == null ? null : assertion.getPrincipal();
    }

    @Override
    public boolean isLoggedIn() {
        if (!super.isLoggedIn()) {
            if (getCASPrincipal() != null) {
                login();
            }
        }
        return super.isLoggedIn();
    }

    @Override
    public void logout() {
        Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (request instanceof ServletRequest) {
            ServletRequest servletRequest = (ServletRequest) request;
            servletRequest.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
        }
        Object session = FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session instanceof HttpSession) {
            HttpSession httpSession = (HttpSession) session;
            httpSession.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
        }
        super.logout();
    }

}
