/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import net.ihe.gazelle.proxy.netty.protocols.syslog.SyslogData;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.Entity;
import javax.persistence.Lob;
import java.sql.Timestamp;

@Entity
@Name("syslogMessage")
public class SyslogMessage extends AbstractMessage implements java.io.Serializable {

    private static final long serialVersionUID = -2767782636271713844L;

    private Integer facility;

    @Lob
    @Type(type = "text")
    private String hostName;

    private Integer severity;

    @Lob
    @Type(type = "text")
    private String timestamp;

    @Lob
    @Type(type = "text")
    private String tag;

    @Lob
    @Type(type = "text")
    private String appName;

    @Lob
    @Type(type = "text")
    private String messageId;

    @Lob
    @Type(type = "text")
    private String procId;

    @Lob
    @Type(type = "text")
    private String payLoad;

    public SyslogMessage() {
        super();
        setProxySide(ProxySide.REQUEST);
    }

    public SyslogMessage(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
            Integer remotePort, SyslogData request) {
        super(timestamp, fromIP, localPort, proxyPort, toIP, remotePort, ProxySide.REQUEST);
        this.facility = request.getFacility();
        this.hostName = request.getHostName();
        this.severity = request.getSeverity();
        this.timestamp = request.getTimestamp();
        this.tag = request.getTag();
        this.appName = request.getAppName();
        this.messageId = request.getMessageId();
        this.procId = request.getProcId();
        this.payLoad = request.getPayload();
        setMessageReceivedAsString(request.getRawMessage());
    }

    public ChannelType getChannelType() {
        return ChannelType.SYSLOG;
    }

    public Integer getFacility() {
        return facility;
    }

    public void setFacility(Integer facility) {
        this.facility = facility;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Integer getSeverity() {
        return severity;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getProcId() {
        return procId;
    }

    public void setProcId(String procId) {
        this.procId = procId;
    }

    public String getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(String payLoad) {
        this.payLoad = payLoad;
    }

}
