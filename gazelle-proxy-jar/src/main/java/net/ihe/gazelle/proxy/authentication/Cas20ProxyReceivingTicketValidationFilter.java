/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.proxy.authentication;

import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import org.jasig.cas.client.proxy.Cas20ProxyRetriever;
import org.jasig.cas.client.proxy.CleanUpTimerTask;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorage;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorageImpl;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;
import org.jasig.cas.client.validation.TicketValidator;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.log.LogProvider;
import org.jboss.seam.log.Logging;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class Cas20ProxyReceivingTicketValidationFilter extends AbstractCasFilter {

    private static final LogProvider log = Logging.getLogProvider(Cas20ProxyReceivingTicketValidationFilter.class);

    private static final int DEFAULT_MILLIS_BETWEEN_CLEANUPS = 60000;
    protected boolean isCASEnabled = false;
    protected String casServerLoginUrl;
    private String proxyReceptorUrl;
    private Timer timer;
    private TimerTask timerTask;
    private int millisBetweenCleanUps;
    private ProxyGrantingTicketStorage proxyGrantingTicketStorage;
    private TicketValidator ticketValidator;
    private boolean redirectAfterValidation;
    private boolean exceptionOnValidationFailure;
    private boolean useSession;

    public Cas20ProxyReceivingTicketValidationFilter() {
        super();

        isCASEnabled = false;
        casServerLoginUrl = "http://tmp";

        setIgnoreInitConfiguration(true);
        setArtifactParameterName("ticket");
        setServiceParameterName("service");
        setEncodeServiceUrl(true);
        setServerName(null);
        setService("-");

        setProxyReceptorUrl(null);
        setProxyGrantingTicketStorage(new ProxyGrantingTicketStorageImpl());
        setMillisBetweenCleanUps(DEFAULT_MILLIS_BETWEEN_CLEANUPS);

        setExceptionOnValidationFailure(true);
        setRedirectAfterValidation(true);
        setUseSession(true);

        setTicketValidator(createValidator());
    }

    protected Cas20ServiceTicketValidator createValidator() {
        Cas20ServiceTicketValidator validator = new Cas20ServiceTicketValidator(casServerLoginUrl);
        validator.setProxyCallbackUrl(null);
        validator.setProxyGrantingTicketStorage(proxyGrantingTicketStorage);
        validator.setProxyRetriever(new Cas20ProxyRetriever(casServerLoginUrl));
        validator.setRenew(false);
        validator.setCustomParameters(new HashMap<String, Object>());
        return validator;
    }

    @Override
    protected void initInternal(FilterConfig filterConfig) throws ServletException {
        super.initInternal(filterConfig);
    }

    @Override
    public void init() {
        super.init();
        CommonUtils.assertNotNull(proxyGrantingTicketStorage, "proxyGrantingTicketStorage cannot be null.");
        if (timer == null) {
            timer = new Timer(true);
        }
        if (timerTask == null) {
            timerTask = new CleanUpTimerTask(proxyGrantingTicketStorage);
        }
        timer.schedule(timerTask, millisBetweenCleanUps, millisBetweenCleanUps);
        CommonUtils.assertNotNull(ticketValidator, "ticketValidator cannot be null.");
    }

    @Override
    public void destroy() {
        super.destroy();
        timer.cancel();
    }

    protected final boolean preFilter(ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String requestUri = request.getRequestURI();
        if (CommonUtils.isEmpty(proxyReceptorUrl) || !requestUri.endsWith(proxyReceptorUrl)) {
            return true;
        } else {
            CommonUtils.readAndRespondToProxyReceptorRequest(request, response, proxyGrantingTicketStorage);
            return false;
        }
    }

    public final void setProxyReceptorUrl(String proxyReceptorUrl) {
        this.proxyReceptorUrl = proxyReceptorUrl;
    }

    public void setProxyGrantingTicketStorage(ProxyGrantingTicketStorage storage) {
        proxyGrantingTicketStorage = storage;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public void setTimerTask(TimerTask timerTask) {
        this.timerTask = timerTask;
    }

    public void setMillisBetweenCleanUps(int millisBetweenCleanUps) {
        this.millisBetweenCleanUps = millisBetweenCleanUps;
    }

    protected void onSuccessfulValidation(HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse, Assertion assertion1) {
    }

    protected void onFailedValidation(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse) {
    }

    protected void reloadParameters(ServletRequest servletRequest) {
        // Create a seam context if needed
        boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }

        setService(ApplicationConfigurationManager.instance().getApplicationUrl());
        casServerLoginUrl = ApplicationConfigurationManager.instance().getCasUrl();
        if (!CommonUtils.isEmpty(casServerLoginUrl)) {
            setTicketValidator(createValidator());
        }
        isCASEnabled = !ApplicationConfigurationManager.instance().isWorksWithoutCas();

        if (createContexts) {
            Lifecycle.endCall();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        reloadParameters(servletRequest);
        if (isCASEnabled) {
            doFilterCAS(servletRequest, servletResponse, filterChain);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public final void doFilterCAS(ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {
        if (!preFilter(servletRequest, servletResponse, filterChain)) {
            return;
        }
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String ticket = CommonUtils.safeGetParameter(request, getArtifactParameterName());
        if (CommonUtils.isNotBlank(ticket)) {
            if (log.isDebugEnabled()) {
                log.debug("Attempting to validate ticket: " + ticket);
            }
            try {
                Assertion assertion = ticketValidator.validate(ticket, constructServiceUrl(request, response));
                if (log.isDebugEnabled()) {
                    log.debug("Successfully authenticated user: " + assertion.getPrincipal().getName());
                }
                request.setAttribute("_const_cas_assertion_", assertion);
                if (useSession) {
                    request.getSession().setAttribute("_const_cas_assertion_", assertion);
                }
                onSuccessfulValidation(request, response, assertion);
            } catch (TicketValidationException e) {
                response.setStatus(403);
                log.warn(e, e);
                onFailedValidation(request, response);
                if (exceptionOnValidationFailure) {
                    throw new ServletException(e);
                }
            }
            if (redirectAfterValidation) {
                log.debug("Redirecting after successful ticket validation.");
                response.sendRedirect(response.encodeRedirectURL(constructServiceUrl(request, response)));
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    public final void setTicketValidator(TicketValidator ticketValidator) {
        this.ticketValidator = ticketValidator;
    }

    public final void setRedirectAfterValidation(boolean redirectAfterValidation) {
        this.redirectAfterValidation = redirectAfterValidation;
    }

    public final void setExceptionOnValidationFailure(boolean exceptionOnValidationFailure) {
        this.exceptionOnValidationFailure = exceptionOnValidationFailure;
    }

    public final void setUseSession(boolean useSession) {
        this.useSession = useSession;
    }

}
