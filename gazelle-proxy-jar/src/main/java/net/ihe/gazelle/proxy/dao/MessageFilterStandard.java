package net.ihe.gazelle.proxy.dao;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.proxy.model.message.*;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

public class MessageFilterStandard<T extends AbstractMessage> implements MessageFilter<T> {

    private ChannelType messageType = ChannelType.HTTP;

    private String initiatorIP;
    private Integer initiatorPort;

    private String responderIP;
    private Integer responderPort;

    private Integer proxyPort;

    private Date dateFrom;
    private Date dateTo;

    private Integer connectionId;

    private ProxySide proxySide = null;

    private String dicomAffectedSopClassUID;
    private String dicomRequestedSopClassUID;
    private String dicomCommandField;

    private String hl7MessageType;
    private String httpMessageType;

    public String getInitiatorIP() {
        return initiatorIP;
    }

    public void setInitiatorIP(String initiatorIP) {
        this.initiatorIP = initiatorIP;
    }

    public Integer getInitiatorPort() {
        return initiatorPort;
    }

    public void setInitiatorPort(Integer initiatorPort) {
        this.initiatorPort = initiatorPort;
    }

    public String getResponderIP() {
        return responderIP;
    }

    public void setResponderIP(String responderIP) {
        this.responderIP = responderIP;
    }

    public Integer getResponderPort() {
        return responderPort;
    }

    public void setResponderPort(Integer responderPort) {
        this.responderPort = responderPort;
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(Integer proxyPort) {
        this.proxyPort = proxyPort;
    }

    public ProxySide getProxySide() {
        return proxySide;
    }

    public void setProxySide(ProxySide proxySide) {
        this.proxySide = proxySide;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public ChannelType getMessageType() {
        return messageType;
    }

    public void setMessageType(ChannelType messageType) {
        this.messageType = messageType;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public String getDicomAffectedSopClassUID() {
        return dicomAffectedSopClassUID;
    }

    public void setDicomAffectedSopClassUID(String dicomAffectedSopClassUID) {
        this.dicomAffectedSopClassUID = dicomAffectedSopClassUID;
    }

    public String getHl7MessageType() {
        return hl7MessageType;
    }

    public void setHl7MessageType(String hl7MessageType) {
        this.hl7MessageType = hl7MessageType;
    }

    public String getHttpMessageType() {
        return httpMessageType;
    }

    public void setHttpMessageType(String httpMessageType) {
        this.httpMessageType = httpMessageType;
    }

    public String getDicomRequestedSopClassUID() {
        return dicomRequestedSopClassUID;
    }

    public void setDicomRequestedSopClassUID(String dicomRequestedSopClassUID) {
        this.dicomRequestedSopClassUID = dicomRequestedSopClassUID;
    }

    public String getDicomCommandField() {
        return dicomCommandField;
    }

    public void setDicomCommandField(String dicomCommandField) {
        this.dicomCommandField = dicomCommandField;
    }

    public void reset() {
        initiatorIP = null;
        initiatorPort = null;
        responderIP = null;
        responderPort = null;
        proxyPort = null;
        dateFrom = null;
        dateTo = null;
        connectionId = null;
        proxySide = null;

        dicomAffectedSopClassUID = null;
        dicomRequestedSopClassUID = null;
        dicomCommandField = null;
        hl7MessageType = null;
        httpMessageType = null;
    }

    public void appendFilters(HQLQueryBuilder<T> criteria) {
        criteria.addRestriction(HQLRestrictions.eq("class", getMessageType().getDiscriminator()));
        criteria.setUseDistinctCount(false);
        criteria.setCachable(false);
        AbstractMessageQuery abstractMessageQuery = new AbstractMessageQuery(criteria);
        addEq(abstractMessageQuery.fromIP(), StringUtils.trimToNull(initiatorIP));
        addEq(abstractMessageQuery.localPort(), initiatorPort);

        addEq(abstractMessageQuery.proxyPort(), proxyPort);

        addEq(abstractMessageQuery.toIP(), StringUtils.trimToNull(responderIP));
        addEq(abstractMessageQuery.remotePort(), responderPort);

        addEq(abstractMessageQuery.connection().id(), connectionId);
        addEq(abstractMessageQuery.proxySide(), proxySide);

        if (dateFrom != null) {
            abstractMessageQuery.dateReceived().ge(dateFrom);
        }
        if (dateTo != null) {
            abstractMessageQuery.dateReceived().le(dateTo);
        }

        if (messageType == ChannelType.DICOM) {
            DicomMessageQuery dicomMessageQuery = new DicomMessageQuery(criteria);
            addEq(dicomMessageQuery.infoAffectedSOPClassUID(), dicomAffectedSopClassUID);
            addEq(dicomMessageQuery.infoRequestedSOPClassUID(), dicomRequestedSopClassUID);
            addEq(dicomMessageQuery.infoCommandField(), dicomCommandField);
        }
        if (messageType == ChannelType.HL7 && hl7MessageType != null) {
            HL7MessageQuery hl7MessageQuery = new HL7MessageQuery(criteria);
            addEq(hl7MessageQuery.hl7MessageType(), hl7MessageType);
        }
        if (messageType == ChannelType.HTTP && httpMessageType != null) {
            HTTPMessageQuery httpMessageQuery = new HTTPMessageQuery(criteria);
            addEq(httpMessageQuery.messageType(), httpMessageType);
        }

    }

    private <V> void addEq(HQLSafePathBasic<V> path, V value) {
        if (value != null) {
            path.eq(value);
        }
    }

}
