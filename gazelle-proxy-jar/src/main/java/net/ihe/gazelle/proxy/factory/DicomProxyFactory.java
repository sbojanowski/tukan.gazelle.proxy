package net.ihe.gazelle.proxy.factory;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.listeners.DicomEventListener;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyFactory;
import net.ihe.gazelle.proxy.netty.protocols.dicom.DicomProxy;
import org.kohsuke.MetaInfServices;

import javax.persistence.EntityManager;

@MetaInfServices(net.ihe.gazelle.proxy.netty.ProxyFactory.class)
public class DicomProxyFactory implements ProxyFactory {

    @Override
    public Proxy<?, ?> newProxy(ConnectionConfig connectionConfig) {
        DicomEventListener proxyEventListener = new DicomEventListener();
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        String storageDicom = ApplicationConfiguration.getValueOfVariable("storage_dicom", entityManager);
        return new DicomProxy(proxyEventListener, connectionConfig, storageDicom);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.DICOM;
    }

}
