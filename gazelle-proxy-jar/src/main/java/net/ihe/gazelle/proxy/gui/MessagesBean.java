package net.ihe.gazelle.proxy.gui;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.dao.MessageFilterStandard;
import net.ihe.gazelle.proxy.dao.ProxyDAO;
import net.ihe.gazelle.proxy.dicom.Dictionnaries;
import net.ihe.gazelle.proxy.model.message.*;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.international.StatusMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

@Name("messagesBean")
@Scope(ScopeType.PAGE)
public class MessagesBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 546021299438254227L;
    private final static String REGEX_OID = "^(0|[1-9]\\d*)\\.((0|[1-9]\\d*)\\.)*(0|[1-9]\\d*)$";
    private static Logger log = LoggerFactory.getLogger(MessagesBean.class);
    private transient HibernateMessageDataModel<AbstractMessage> messageDataModel;
    private MessageFilterStandard messageFilterStandard;

    @In
    private EntityManager entityManager;
    private String currentSelectDate;

    private static Map sortByComparator(Map unsortMap) {

        List list = new LinkedList(unsortMap.entrySet());

        // sort list based on comparator
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // put sorted list into map again
        // LinkedHashMap make sure order in which keys were inserted
        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public static Date stringToDate(String dateToConvert) {

        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
        Date date = null;
        try {
            date = sdf.parse(dateToConvert);
        } catch (Exception e) {
            log.error("" + e);
        }
        return date;
    }

    public void clearFilter() {
        messageFilterStandard.reset();
        rebuildDataModel();
    }

    public void refreshFilter() {
        rebuildDataModel();
    }

    @Create
    public void createModel() {
        messageFilterStandard = new MessageFilterStandard();
        rebuildDataModel();
    }

    @Remove
    @Destroy
    public void destroy() {
    }

    public Integer getConnectionId() {
        return messageFilterStandard.getConnectionId();
    }

    public void setConnectionId(Integer connectionId) {
        messageFilterStandard.setConnectionId(connectionId);
    }

    public Date getDateFrom() {
        return messageFilterStandard.getDateFrom();
    }

    public void setDateFrom(Date dateFrom) {
        messageFilterStandard.setDateFrom(dateFrom);
    }

    public Date getDateTo() {
        return messageFilterStandard.getDateTo();
    }

    public void setDateTo(Date dateTo) {
        messageFilterStandard.setDateTo(dateTo);
    }

    public String getInitiatorIP() {
        return messageFilterStandard.getInitiatorIP();
    }

    public void setInitiatorIP(String initiatorIP) {
        messageFilterStandard.setInitiatorIP(initiatorIP);
    }

    public Integer getInitiatorPort() {
        return messageFilterStandard.getInitiatorPort();
    }

    public void setInitiatorPort(Integer initiatorPort) {
        messageFilterStandard.setInitiatorPort(initiatorPort);
    }

    public HibernateDataModel<?> getMessageDataModel() {
        return messageDataModel;
    }

    public ChannelType getMessageType() {
        return messageFilterStandard.getMessageType();
    }

    public void setMessageType(ChannelType messageType) {
        messageFilterStandard.setMessageType(messageType);
        rebuildDataModel();
    }

    public List<ChannelType> getMessageTypes() {
        return Arrays.asList(ChannelType.values());
    }

    public Integer getProxyPort() {
        return messageFilterStandard.getProxyPort();
    }

    public void setProxyPort(Integer proxyPort) {
        messageFilterStandard.setProxyPort(proxyPort);
    }

    public ProxySide getProxySide() {
        return messageFilterStandard.getProxySide();
    }

    public void setProxySide(ProxySide proxySide) {
        messageFilterStandard.setProxySide(proxySide);
    }

    public List<ProxySide> getProxySides() {
        return Arrays.asList(ProxySide.values());
    }

    public String getResponderIP() {
        return messageFilterStandard.getResponderIP();
    }

    public void setResponderIP(String responderIP) {
        messageFilterStandard.setResponderIP(responderIP);
    }

    public Integer getResponderPort() {
        return messageFilterStandard.getResponderPort();
    }

    public void setResponderPort(Integer responderPort) {
        messageFilterStandard.setResponderPort(responderPort);
    }

    public boolean isDICOM() {
        return messageFilterStandard.getMessageType() == ChannelType.DICOM;
    }

    public boolean isHL7() {
        return messageFilterStandard.getMessageType() == ChannelType.HL7;
    }

    public boolean isHTTP() {
        return messageFilterStandard.getMessageType() == ChannelType.HTTP;
    }

    public String getDicomAffectedSopClassUID() {
        return messageFilterStandard.getDicomAffectedSopClassUID();
    }

    public void setDicomAffectedSopClassUID(String dicomAffectedSopClassUID) {
        messageFilterStandard.setDicomAffectedSopClassUID(dicomAffectedSopClassUID);
    }

    public String getHl7MessageType() {
        return messageFilterStandard.getHl7MessageType();
    }

    public void setHl7MessageType(String hl7MessageType) {
        messageFilterStandard.setHl7MessageType(hl7MessageType);
    }

    public String getHttpMessageType() {
        return messageFilterStandard.getHttpMessageType();
    }

    public void setHttpMessageType(String httpMessageType) {
        messageFilterStandard.setHttpMessageType(httpMessageType);
    }

    public SelectItem[] getDicomAffectedSopClassUIDs() {
        Map map = Dictionnaries.INSTANCE.getSopClassByUID();
        map = sortByComparator(map);
        return getSelectItems(map);
    }

    public SelectItem[] getHL7MessageTypes() {
        HL7MessageQuery query = new HL7MessageQuery();
        query.setCachable(true);
        final List<String> list = query.hl7MessageType().getListDistinct();
        return addItems(list);
    }

    public SelectItem[] getHTTPMessageTypes() {
        HTTPMessageQuery query = new HTTPMessageQuery();
        query.setCachable(true);
        final List<String> list = query.messageType().getListDistinct();
        return addItems(list);
    }

    private SelectItem[] addItems(final List<String> list) {
        list.removeAll(Collections.singleton(null));
        Collections.sort(list);
        list.add(0, "--");
        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (String messType : list) {
            result.add(new SelectItem(messType));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    private SelectItem[] getSelectItems(Map<Object, String> map) {
        SelectItem[] result = new SelectItem[map.size() + 1];
        int i = 0;
        result[i] = new SelectItem(null, "--");
        i++;
        Set<Entry<Object, String>> entrySet = map.entrySet();
        for (Entry<Object, String> entry : entrySet) {
            result[i] = new SelectItem(entry.getKey(), entry.getValue());
            i++;
        }
        return result;
    }

    public String getDicomRequestedSopClassUID() {
        return messageFilterStandard.getDicomRequestedSopClassUID();
    }

    public void setDicomRequestedSopClassUID(String dicomRequestedSopClassUID) {
        messageFilterStandard.setDicomRequestedSopClassUID(dicomRequestedSopClassUID);
    }

    public void validateDate() {
        Date dateFrom = messageFilterStandard.getDateFrom();
        Date dateTo = messageFilterStandard.getDateTo();

        if ((dateFrom != null) && (dateTo != null)) {
            if (dateTo.before(dateFrom)) {
                log.error("DateFrom is after DateTo please correct !");
                StatusMessages.instance()
                        .addToControlFromResourceBundleOrDefault("dateto", StatusMessage.Severity.ERROR, "test",
                                "Date to is before date from, please correct !");
                messageFilterStandard.setDateTo(dateFrom);
            }
        }
    }

    public String getCurrentSelectDate() {
        return currentSelectDate;
    }

    public void setCurrentSelectDate(String currentSelectDate) {
        this.currentSelectDate = currentSelectDate;
    }

    public void selectDate(String date) {
        if (date != null && !(date.equals("--")) && !(date.isEmpty())) {
            Date dateFrom, dateTo = null;
            dateFrom = new Date();
            Calendar cal = new GregorianCalendar();

            if (date.equals("today")) {
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("yesterday")) {
                if (cal.get(Calendar.DAY_OF_YEAR) == 1) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.MONTH, cal.get(Calendar.DECEMBER));
                    cal.set(Calendar.DAY_OF_MONTH, 31);
                } else {
                    cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - 1));
                }
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("this week")) {
                if (cal.get(Calendar.DAY_OF_YEAR) < 7) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.MONTH, cal.get(Calendar.DECEMBER));
                    cal.set(Calendar.DAY_OF_MONTH, 31 - (7 - cal.get(Calendar.DAY_OF_YEAR)));
                }
                cal.set(Calendar.DAY_OF_WEEK, 2);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("this month")) {
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("last week")) {
                if (cal.get(Calendar.WEEK_OF_YEAR) == 1) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.WEEK_OF_YEAR, 52);
                } else {
                    cal.set(Calendar.WEEK_OF_YEAR, (cal.get(Calendar.WEEK_OF_YEAR) - 1));
                }
                cal.set(Calendar.DAY_OF_WEEK, 2);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("last month")) {
                if (cal.get(Calendar.MONTH) == 0) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.MONTH, Calendar.DECEMBER);
                } else {
                    cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH) - 1));
                }
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("last hour")) {
                cal.set(Calendar.HOUR, (cal.get(Calendar.HOUR) - 1));
            }
            dateFrom = cal.getTime();

            cal.setTime(Calendar.getInstance().getTime());
            if (date.equals("yesterday")) {
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                dateTo = cal.getTime();
            }
            if (date.equals("last week")) {
                cal.set(Calendar.WEEK_OF_YEAR, cal.get(Calendar.WEEK_OF_YEAR));
                cal.set(Calendar.DAY_OF_WEEK, 2);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                dateTo = cal.getTime();
            }
            if (date.equals("last month")) {
                cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                dateTo = cal.getTime();
            }
            messageFilterStandard.setDateFrom(dateFrom);

            redirectToMessages(getMessageType().toString(), getProxyPort(), getResponderIP(), getResponderPort(),
                    getInitiatorIP(), getInitiatorPort(), getConnectionId(), dateFrom, dateTo,
                    getDicomAffectedSopClassUID(), getDicomCommandField(), getHl7MessageType(), getHttpMessageType());
        } else {
            log.error("Date problem");
        }

    }

    public SelectItem[] getDicomRequestedSopClassUIDs() {
        return getDicomAffectedSopClassUIDs();
    }

    public String getDicomCommandField() {
        return messageFilterStandard.getDicomCommandField();
    }

    public void setDicomCommandField(String dicomCommandField) {
        messageFilterStandard.setDicomCommandField(dicomCommandField);
    }

    public SelectItem[] getDicomCommandFields() {
        Map map = Dictionnaries.INSTANCE.getCommandFields();
        return getSelectItems(map);
    }

    public void rebuildDataModel() {
        messageDataModel = new HibernateMessageDataModel<AbstractMessage>(messageFilterStandard);
    }

    public void redirectToMessageFromURL() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String id = (String) fc.getExternalContext().getRequestParameterMap().get("id");
        AbstractMessage message = ProxyDAO.getMessageByID(Integer.valueOf(id));

        Redirect redirect = Redirect.instance();
        redirect.setParameter("id", id);

        String viewId = "";
        if (message instanceof HL7Message) {
            viewId = "/messages/hl7.xhtml";
        }
        if (message instanceof RawMessage) {
            viewId = "/messages/raw.xhtml";
        }
        if (message instanceof DicomMessage) {
            viewId = "/messages/dicom.xhtml";
        }
        if (message instanceof SyslogMessage) {
            viewId = "/messages/syslog.xhtml";
        }
        if (message instanceof HTTPMessage) {
            viewId = "/messages/http.xhtml";
        }

        redirect.setViewId(viewId);
        redirect.execute();
    }

    public String redirectToMessage(AbstractMessage message) {
        if (message.getId() == null) {
            return null;
        }
        if (message instanceof HL7Message) {
            return "/messages/hl7.seam?id=" + message.getId();
        }
        if (message instanceof RawMessage) {
            return "/messages/raw.seam?id=" + message.getId();
        }
        if (message instanceof DicomMessage) {
            return "/messages/dicom.seam?id=" + message.getId();
        }
        if (message instanceof SyslogMessage) {
            return "/messages/syslog.seam?id=" + message.getId();
        }
        if (message instanceof HTTPMessage) {
            return "/messages/http.seam?id=" + message.getId();
        }
        return null;
    }

    public void setMessageTypeWithString(String message) {
        if (message != null) {
            if (message.equals("DICOM")) {
                messageFilterStandard.setMessageType(ChannelType.DICOM);
            } else if (message.equals("HL7")) {
                messageFilterStandard.setMessageType(ChannelType.HL7);
            } else if (message.equals("SYSLOG")) {
                messageFilterStandard.setMessageType(ChannelType.SYSLOG);
            } else if (message.equals("HTTP")) {
                messageFilterStandard.setMessageType(ChannelType.HTTP);
            } else if (message.equals("RAW")) {
                messageFilterStandard.setMessageType(ChannelType.RAW);
            }
        } else {
            messageFilterStandard.setMessageType(ChannelType.HTTP);
        }

        rebuildDataModel();
    }

    public boolean validateInitiatorIP() {
        String ipPattern = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
        String hostname = getInitiatorIP();

        if (hostname != null) {

            if (hostname.matches(ipPattern)) {
                return true;
            } else {
                log.error("You must enter a good format IP address !");
                StatusMessages.instance()
                        .addToControlFromResourceBundleOrDefault("initiatorIP", StatusMessage.Severity.ERROR, "test",
                                "You must enter a valid IP address !");
                return false;
            }
        } else {
            log.error("You must enter a good format IP address !");
            StatusMessages.instance()
                    .addToControlFromResourceBundleOrDefault("initiatorIP", StatusMessage.Severity.ERROR, "test",
                            "You must enter a valid IP address !");
            return false;
        }
    }

    public boolean validateResponderIP() {
        String ipPattern = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
        String hostname = null;
        hostname = getResponderIP();

        if (hostname != null) {

            if (hostname.matches(ipPattern)) {
                return true;
            } else {
                log.error("You must enter a good format IP address !");
                StatusMessages.instance()
                        .addToControlFromResourceBundleOrDefault("responderIP", StatusMessage.Severity.ERROR, "test",
                                "You must enter a valid IP address !");
                return false;
            }
        } else {
            log.error("You must enter a good format IP address !");
            StatusMessages.instance()
                    .addToControlFromResourceBundleOrDefault("responderIP", StatusMessage.Severity.ERROR, "test",
                            "You must enter a valid IP address !");
            return false;
        }
    }

    public boolean validateOID() {
        String oid = getDicomAffectedSopClassUID();

        if (oid != null) {
            if (oid.matches(REGEX_OID)) {
                return true;
            } else {
                log.error("You must enter a valid UID !");
                StatusMessages.instance()
                        .addToControlFromResourceBundleOrDefault("oidValueId", StatusMessage.Severity.ERROR, "test",
                                "You must enter a valid UID !");
                return false;
            }
        } else {
            log.error("You must enter a valid UID !");
            StatusMessages.instance()
                    .addToControlFromResourceBundleOrDefault("oidValueId", StatusMessage.Severity.ERROR, "test",
                            "You must enter a valid UID !");
            return false;
        }

    }

    public void redirectToMessages(String messageType, Integer proxyProviderPort, String proxyConsumerHost,
                                   Integer proxyConsumerPort, String initiatorsIP, Integer initiatorsPort, Integer connectionId, Date dateFrom,
                                   Date dateTo, String dicomAffectedSopClassUID, String dicomCommandField, String hl7MessageType,
                                   String httpMessageType) {

        init(messageType, proxyProviderPort, proxyConsumerHost, proxyConsumerPort, initiatorsIP, initiatorsPort,
                connectionId, dateFrom, dateTo, dicomAffectedSopClassUID, dicomCommandField, hl7MessageType,
                httpMessageType);

        log.info("messageType:" + getMessageType());
        log.info("proxyProviderPort:" + getProxyPort());
        log.info("proxyConsumerHost:" + getResponderIP());
        log.info("proxyConsumerPort:" + getResponderPort());
        log.info("initiatorsIP:" + getInitiatorIP());
        log.info("initiatorsPort:" + getInitiatorPort());
        log.info("connectionId:" + getConnectionId());
        log.info("dateFrom:" + getDateFrom());
        log.info("dateTo:" + getDateTo());
        log.info("SOP:" + getDicomAffectedSopClassUID());
        log.info("cmd:" + getDicomCommandField());
        log.info("hl7MessageType:" + getHl7MessageType());
        log.info("httpMessageType:" + getHttpMessageType());

        StringBuilder builder = new StringBuilder("");
        builder.append("/messages.seam?");
        builder.append("&messageType=");
        builder.append(this.getMessageType());

        if (getProxyPort() != null) {
            builder.append("&proxyProviderPort=");
            builder.append(this.getProxyPort());
        }
        if (getResponderIP() != null) {
            builder.append("&proxyConsumerHost=");
            builder.append(this.getResponderIP());
        }
        if (getResponderPort() != null) {
            builder.append("&proxyConsumerPort=");
            builder.append(this.getResponderPort());
        }
        if (getInitiatorIP() != null) {
            builder.append("&initiatorsIP=");
            builder.append(this.getInitiatorIP());
        }
        if (getInitiatorPort() != null) {
            builder.append("&initiatorsPort=");
            builder.append(this.getInitiatorPort());
        }
        if (getConnectionId() != null) {
            builder.append("&connectionId=");
            builder.append(this.getConnectionId());
        }
        if (getDateFrom() != null) {
            builder.append("&dateFrom=");
            builder.append(this.getDateFrom());
        }
        if (getDateTo() != null) {
            builder.append("&dateTo=");
            builder.append(this.getDateTo());
        }
        if (getDicomAffectedSopClassUID() != null) {
            builder.append("&dicomAffectedSopClassUID=");
            builder.append(this.getDicomAffectedSopClassUID());
        }
        if (getDicomCommandField() != null) {
            builder.append("&dicomCommandField=");
            builder.append(this.getDicomCommandField());
        }
        if (getHl7MessageType() != null) {
            builder.append("&hl7MessageType=");
            builder.append(this.getHl7MessageType());
        }
        if (getHttpMessageType() != null) {
            builder.append("&httpMessageType=");
            try {
                setHttpMessageType(URLEncoder.encode(getHttpMessageType(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                log.error("Failed to encode URL ! : " + e);
            }

            builder.append(this.getHttpMessageType());
        }
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + builder.toString());
        } catch (IOException e) {
            log.error("" + e);
        }

    }

    public void initFromUrl() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String messageType = null;
        Integer proxyProviderPort = null;
        String proxyConsumerHost = null;
        Integer proxyConsumerPort = null;
        String initiatorsIP = null;
        Integer initiatorsPort = null;
        Integer connectionId = null;
        Date dateFrom = null;
        Date dateTo = null;
        String dicomAffectedSopClassUID = null;
        String dicomCommandField = null;
        String hl7MessageType = null;
        String httpMessageType = null;

        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey("messageType")) {
                messageType = urlParams.get("messageType");
            }
            if (urlParams.containsKey("proxyProviderPort")) {
                proxyProviderPort = Integer.decode(urlParams.get("proxyProviderPort"));
            }
            if (urlParams.containsKey("proxyConsumerHost")) {
                proxyConsumerHost = urlParams.get("proxyConsumerHost");
            }
            if (urlParams.containsKey("proxyConsumerPort")) {
                proxyConsumerPort = Integer.decode(urlParams.get("proxyConsumerPort"));
            }
            if (urlParams.containsKey("initiatorsIP")) {
                initiatorsIP = urlParams.get("initiatorsIP");
            }
            if (urlParams.containsKey("initiatorsPort")) {
                initiatorsPort = Integer.decode(urlParams.get("initiatorsPort"));
            }
            if (urlParams.containsKey("connectionId")) {
                connectionId = Integer.decode(urlParams.get("connectionId"));
            }
            if (urlParams.containsKey("dateFrom")) {
                dateFrom = stringToDate(urlParams.get("dateFrom"));
            }
            if (urlParams.containsKey("dateTo")) {
                dateTo = stringToDate(urlParams.get("dateTo"));
            }
            if (urlParams.containsKey("dicomAffectedSopClassUID")) {
                dicomAffectedSopClassUID = urlParams.get("dicomAffectedSopClassUID");
            }
            if (urlParams.containsKey("dicomCommandField")) {
                dicomCommandField = urlParams.get("dicomCommandField");
            }
            if (urlParams.containsKey("hl7MessageType")) {
                hl7MessageType = urlParams.get("hl7MessageType");
            }
            if (urlParams.containsKey("httpMessageType")) {
                httpMessageType = urlParams.get("httpMessageType");
            }
            init(messageType, proxyProviderPort, proxyConsumerHost, proxyConsumerPort, initiatorsIP, initiatorsPort,
                    connectionId, dateFrom, dateTo, dicomAffectedSopClassUID, dicomCommandField, hl7MessageType,
                    httpMessageType);
        }
    }

    public void init(String messageType, Integer proxyProviderPort, String proxyConsumerHost, Integer proxyConsumerPort,
                     String initiatorsIP, Integer initiatorsPort, Integer connectionId, Date dateFrom, Date dateTo,
                     String dicomAffectedSopClassUID, String dicomCommandField, String hl7MessageType, String httpMessageType) {
        createModel();
        if (messageType != null) {
            if (messageType.equals("DICOM")) {
                setMessageType(ChannelType.DICOM);
            } else if (messageType.equals("HL7")) {
                setMessageType(ChannelType.HL7);
            } else if (messageType.equals("SYSLOG")) {
                setMessageType(ChannelType.SYSLOG);
            } else if (messageType.equals("HTTP")) {
                setMessageType(ChannelType.HTTP);
            } else if (messageType.equals("RAW")) {
                setMessageType(ChannelType.RAW);
            }
        } else {
            setMessageType(ChannelType.HTTP);
        }

        if (proxyProviderPort != null && proxyProviderPort > 0) {
            setProxyPort(proxyProviderPort);
        }
        if (proxyConsumerHost != null && !(proxyConsumerHost.isEmpty())) {
            setResponderIP(proxyConsumerHost);
        }
        if (proxyConsumerPort != null && proxyConsumerPort > 0) {
            setResponderPort(proxyConsumerPort);
        }
        if (initiatorsIP != null && !(initiatorsIP.isEmpty())) {
            setInitiatorIP(initiatorsIP);
        }
        if (initiatorsPort != null && initiatorsPort > 0) {
            setInitiatorPort(initiatorsPort);
        }
        if (connectionId != null && connectionId > 0) {
            setConnectionId(connectionId);
        }
        if (dateFrom != null) {
            setDateFrom(dateFrom);
        }
        if (dateTo != null) {
            setDateTo(dateTo);
        }
        if (dicomAffectedSopClassUID != null && !(dicomAffectedSopClassUID.isEmpty())) {
            setDicomAffectedSopClassUID(dicomAffectedSopClassUID);
        }
        if (dicomCommandField != null && !(dicomCommandField.isEmpty())) {
            setDicomCommandField(dicomCommandField);
        }
        if (hl7MessageType != null && !(hl7MessageType.isEmpty()) && !hl7MessageType.equals("--")) {
            setHl7MessageType(hl7MessageType);
        }
        if (httpMessageType != null && !(httpMessageType.isEmpty()) && !httpMessageType.equals("--")) {
            setHttpMessageType(httpMessageType);
        }
    }

    public String copyToClipboard(AbstractMessage message) {
        String url = ApplicationConfigurationManager.instance().getApplicationUrl();
        StringBuffer sb = new StringBuffer(url);
        sb.append(redirectToMessage(message));
        FacesMessages.instance().add(Severity.INFO, "Permanent link is copied in the clipboard");
        return sb.toString();
    }

}
