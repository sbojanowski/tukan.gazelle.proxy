package net.ihe.gazelle.proxy.admin.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("applicationConfiguration")
@Table(name = "app_configuration", schema = "public")
@SequenceGenerator(name = "app_configuration_sequence", sequenceName = "app_configuration_id_seq", allocationSize = 1)
public class ApplicationConfiguration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5444985974251904654L;

    private static Logger log = LoggerFactory.getLogger(ApplicationConfiguration.class);

    // attributes
    // ////////////////////////////////////////////////////////////////////////////

    /**
     * Id of this object
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_configuration_sequence")
    private Integer id;

    @Column(name = "variable", unique = true)
    private String variable;

    @Column(name = "value")
    private String value;

    // constructors
    // ////////////////////////////////////////////////////////////////////////////

    public ApplicationConfiguration() {
    }

    public ApplicationConfiguration(String variable, String value) {
        super();
        this.variable = variable;
        this.value = value;
    }

    // getters and setters
    // //////////////////////////////////////////////////////////////////////

    public static String getValueOfVariable(String variable) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return getValueOfVariable(variable, entityManager);
    }

    public static String getValueOfVariable(String variable, EntityManager entityManager) {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(
                new HQLQueryBuilder<ApplicationConfiguration>(entityManager, ApplicationConfiguration.class));
        query.variable().eq(variable);
        ApplicationConfiguration conf = query.getUniqueResult();
        if (conf == null) {
            return null;
        } else {
            return conf.getValue();
        }
    }

    public static ApplicationConfiguration getApplicationConfigurationByVariable(String variable) {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery();
        query.variable().eq(variable);
        return query.getUniqueResult();
    }

    /**
     * @param variable : string
     * @return true only if the value of the variable fixed to "true"
     */
    public static Boolean getBooleanValue(String variable) {
        String val = getValueOfVariable(variable);
        if (val == null) {
            return false;
        }
        try {
            return Boolean.valueOf(val);
        } catch (Exception e) {
            return false;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    // hashcode and equals
    // //////////////////////////////////////////////////////////////////////

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    // methods
    // ///////////////////////////////////////////////////////////////////////////////////

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((variable == null) ? 0 : variable.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ApplicationConfiguration other = (ApplicationConfiguration) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        if (variable == null) {
            if (other.variable != null) {
                return false;
            }
        } else if (!variable.equals(other.variable)) {
            return false;
        }
        return true;
    }

}
