package net.ihe.gazelle.proxy.admin.gui;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

@Name("applicationManager")
@Scope(ScopeType.PAGE)
public class ApplicationManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ApplicationConfiguration preference = null;

    public List<ApplicationConfiguration> getAllPreferences() {
        HQLQueryBuilder<ApplicationConfiguration> builder = new HQLQueryBuilder<ApplicationConfiguration>(
                EntityManagerService.provideEntityManager(), ApplicationConfiguration.class);
        builder.addOrder("variable", true);
        return builder.getList();
    }

    public void savePreference(ApplicationConfiguration inPreference) {
        ApplicationConfigurationQuery q = new ApplicationConfigurationQuery();
        q.variable().eq(inPreference.getVariable());
        if (q.getCount() == 0) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(inPreference);
            entityManager.flush();
            preference = null;
            // reset preferences with scope application
            ApplicationConfigurationManager.instance().resetApplicationConfiguration();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preference " + inPreference.getVariable() + " added");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Preference name \"" + inPreference.getVariable() + "\" already exist");
        }
    }

    public void updatePreference(ApplicationConfiguration inPreference) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(inPreference);
        entityManager.flush();
        preference = null;
        // reset preferences with scope application
        ApplicationConfigurationManager.instance().resetApplicationConfiguration();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preference " + inPreference.getVariable() + " updated");
    }

    public void removeSelectedPreference() {
        String preferenceName;
        if (this.preference != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            this.preference = entityManager.find(ApplicationConfiguration.class, this.preference.getId());
            if (this.preference != null) {
                preferenceName = this.preference.getVariable();
                entityManager.remove(this.preference);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preference '" + preferenceName + "' deleted.");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There was no Preference selected for deletion!");
        }
    }

    public void createNewPreference() {
        preference = new ApplicationConfiguration();
    }

    public ApplicationConfiguration getPreference() {
        return preference;
    }

    public void setPreference(ApplicationConfiguration preference) {
        this.preference = preference;
    }
}
