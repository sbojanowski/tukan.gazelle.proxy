package net.ihe.gazelle.proxy.factory;

import net.ihe.gazelle.proxy.listeners.RawEventListener;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyFactory;
import net.ihe.gazelle.proxy.netty.protocols.raw.RawProxy;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.proxy.netty.ProxyFactory.class)
public class RawProxyFactory implements ProxyFactory {

    @Override
    public Proxy<?, ?> newProxy(ConnectionConfig connectionConfig) {
        RawEventListener proxyEventListener = new RawEventListener();
        return new RawProxy(proxyEventListener, connectionConfig);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.RAW;
    }

}
