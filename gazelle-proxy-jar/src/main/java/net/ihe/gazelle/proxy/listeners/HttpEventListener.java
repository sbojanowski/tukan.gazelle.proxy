package net.ihe.gazelle.proxy.listeners;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.model.message.HTTPMessage;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import net.ihe.gazelle.proxy.netty.channel.ProxyTools;
import org.apache.log4j.Logger;
import org.jboss.netty.handler.codec.http.HttpMessage;

import javax.persistence.EntityManager;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Set;

public class HttpEventListener extends SameEventListener<HttpMessage> {

    private static final Charset UTF_8 = Charset.forName("UTF-8");
    public static Logger log = Logger.getLogger(HttpEventListener.class);

    public HttpEventListener() {
        super();
    }

    public void saveMessage(final Timestamp timeStamp, final HttpMessage message, final String requesterIp,
            final int requesterPort, final int proxyPort, final String responderIp, final int responderPort,
            final int requestChannelId, final int responseChannelId, final ProxySide side) {

        // Messages are coming from different threads!
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                        HTTPMessage soapMessage = new HTTPMessage(timeStamp, requesterIp, requesterPort, proxyPort,
                                responderIp, responderPort, side);

                        soapMessage.setHeaders(message.toString().getBytes(UTF_8));

                        if (message.getContent().readable() && message.getContent().readableBytes() > 0) {
                            extractContent(message, soapMessage);
                        } else {
                            soapMessage.setMessageReceived(new byte[0]);
                        }

                        processMessage(entityManager, soapMessage, requestChannelId, responseChannelId);
                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }

        }
    }

    private void extractContent(HttpMessage message, HTTPMessage soapMessage) {
        String mediaTypeString = null;

        Set<String> headerNames = message.getHeaderNames();
        for (String headerName : headerNames) {
            if (headerName.toLowerCase().equals("content-type")) {
                String header = message.getHeader(headerName);
                int semicol = header.indexOf(';');
                if (semicol >= 0) {
                    header = header.substring(0, semicol);
                }
                mediaTypeString = header;
            }
        }

        soapMessage.setMessageType(mediaTypeString);
        soapMessage.setMessageReceived(ProxyTools.getBytes(message.getContent().copy()));
    }

}
