/*
message * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import java.sql.Timestamp;

@Entity
@Name("hl7Message")
public class HL7Message extends AbstractMessage implements java.io.Serializable {

    private static final long serialVersionUID = 1417618530596352149L;

    @Column(name = "hl7_messagetype")
    private String hl7MessageType;

    @Column(name = "hl7_version")
    private String hl7Version;

    public HL7Message() {
        super();
    }

    public HL7Message(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
                      Integer remotePort, ProxySide proxySide) {
        super(timestamp, fromIP, localPort, proxyPort, toIP, remotePort, proxySide);
    }


    public String getHl7MessageType() {
        if (hl7MessageType == null) {
            String message = getMessageReceivedAsString();
            extractInfoFromMessage(message);
        }
        return hl7MessageType;
    }

    public void setHl7MessageType(String messageType) {
        this.hl7MessageType = messageType;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(String hl7Version) {
        this.hl7Version = hl7Version;
    }

    public ChannelType getChannelType() {
        return ChannelType.HL7;
    }

    public void extractInfoFromMessage(String message) {
        EntityManager em = EntityManagerService.provideEntityManager();
        hl7Version = null;
        hl7MessageType = null;
        if (message.startsWith("MSH")) {
            char separator = message.charAt(3);
            String delims = "[" + separator + "]";
            String[] tokens = message.split(delims);
            setHl7MessageType(tokens[8]);
            setHl7Version(tokens[11]);
            em.merge(this);
            em.flush();
        }
    }

    @Override
    public String getInfoGUI() {
        return getHl7MessageType();
    }

}
