package net.ihe.gazelle.proxy.dao;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.AbstractMessageQuery;

public class ProxyDAO {

    public static AbstractMessage getMessageByID(Integer id) {
        AbstractMessageQuery abstractMessageQuery = new AbstractMessageQuery();
        abstractMessageQuery.id().eq(id);
        return abstractMessageQuery.getUniqueResult();
    }

}
