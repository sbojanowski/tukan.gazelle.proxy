package net.ihe.gazelle.proxy.listeners;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.model.message.HL7Message;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class HL7EventListener extends SameEventListener<String> {

    public HL7EventListener() {
        super();
    }

    protected void saveMessage(final Timestamp timeStamp, final String message, final String requesterIp,
            final int requesterPort, final int proxyPort, final String responderIp, final int responderPort,
            final int requestChannelId, final int responseChannelId, final ProxySide side) {
        // Messages are coming from different threads!
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {

                        HL7Message hl7message = new HL7Message(timeStamp, requesterIp, requesterPort, proxyPort,
                                responderIp, responderPort, side);
                        hl7message.setMessageReceivedAsString(message);

                        processMessage(entityManager, hl7message, requestChannelId, responseChannelId);
                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }
        }
    }
}
