package net.ihe.gazelle.proxy.model.message;

import javax.persistence.EntityManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class HTTPMessageQuery extends HTTPMessageEntity<HTTPMessage> implements HQLQueryBuilderInterface<HTTPMessage> {

	public HTTPMessageQuery() {
		super("this", new HQLQueryBuilder(HTTPMessage.class));
	}

	public HTTPMessageQuery(HQLQueryBuilder<?> queryBuilder) {
		super("this", queryBuilder);
	}
	
	public HTTPMessageQuery(EntityManager entityManager) {
		super("this", new HQLQueryBuilder(entityManager,HTTPMessage.class));
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
		return queryBuilder.getCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HTTPMessage> getList() {
		return (List<HTTPMessage>) queryBuilder.getList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HTTPMessage> getListNullIfEmpty() {
		return (List<HTTPMessage>) queryBuilder.getListNullIfEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPMessage getUniqueResult() {
		return (HTTPMessage) queryBuilder.getUniqueResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HQLStatistic<HTTPMessage>> getListWithStatistics(List<HQLStatisticItem> items) {
		List<?> result = queryBuilder.getListWithStatistics(items);
		return (List<HQLStatistic<HTTPMessage>>) result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> getListWithStatisticsItems(List<HQLStatisticItem> items, HQLStatistic<HTTPMessage> item,
			int statisticItemIndex) {
		HQLStatistic safeItem = item;
		List<Object> result = queryBuilder.getListWithStatisticsItems(items, safeItem, statisticItemIndex);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFirstResult(int firstResult) {
		queryBuilder.setFirstResult(firstResult);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxResults(int maxResults) {
		queryBuilder.setMaxResults(maxResults);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCachable() {
		return queryBuilder.isCachable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCachable(boolean cachable) {
		queryBuilder.setCachable(cachable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addRestriction(HQLRestriction restriction) {
		queryBuilder.addRestriction(restriction);
	}

}

