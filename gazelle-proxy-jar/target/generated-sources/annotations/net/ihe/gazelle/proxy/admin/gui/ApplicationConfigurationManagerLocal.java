package net.ihe.gazelle.proxy.admin.gui;

@javax.ejb.Local
public interface ApplicationConfigurationManagerLocal {

	void destroy();

	java.lang.String getApplicationUrl();

	java.lang.String getCasUrl();

	java.lang.String getDcmDumpPath();

	java.lang.String getDicomStorage();

	java.lang.String getDocumentation();

	java.lang.String getEvsClientUrl();

	java.lang.Boolean getIpLogin();

	java.lang.String getIpLoginAdmin();

	java.lang.String getIssueTracker();

	int getMaxProxyPort();

	int getMinProxyPort();

	java.lang.String getProxyIpAddresses();

	java.lang.String getProxyOid();

	java.lang.String getReleaseNoteUrl();

	boolean isJmsCommunicationEnabled();

	boolean isUserAllowedAsAdmin();

	boolean isWorksWithoutCas();

	java.lang.String loginByIP();

	void resetApplicationConfiguration();

}
