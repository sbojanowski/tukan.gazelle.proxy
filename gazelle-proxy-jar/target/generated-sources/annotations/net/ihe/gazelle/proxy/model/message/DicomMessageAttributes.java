package net.ihe.gazelle.proxy.model.message;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class DicomMessageAttributes<T extends DicomMessage> extends net.ihe.gazelle.proxy.model.message.AbstractMessageAttributes<T> {

	public DicomMessageAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return DicomMessage.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to fileCommandSet of type byte[]
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<byte[]> fileCommandSet() {
		return new HQLSafePathBasic<byte[]>(this, path + ".fileCommandSet", queryBuilder, byte[].class);
	}

	/**
	 * @return Path to infoAffectedSOPClassUID of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> infoAffectedSOPClassUID() {
		return new HQLSafePathBasicString<String>(this, path + ".infoAffectedSOPClassUID", queryBuilder, String.class);
	}

	/**
	 * @return Path to infoCommandField of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> infoCommandField() {
		return new HQLSafePathBasicString<String>(this, path + ".infoCommandField", queryBuilder, String.class);
	}

	/**
	 * @return Path to infoRequestedSOPClassUID of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> infoRequestedSOPClassUID() {
		return new HQLSafePathBasicString<String>(this, path + ".infoRequestedSOPClassUID", queryBuilder, String.class);
	}

	/**
	 * @return Path to fileTransfertSyntax of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> fileTransfertSyntax() {
		return new HQLSafePathBasicString<String>(this, path + ".fileTransfertSyntax", queryBuilder, String.class);
	}



}

