package net.ihe.gazelle.proxy.admin.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class HomeAttributes<T extends Home> extends HQLSafePathEntity<T> {

	public HomeAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Home.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to homeTitle of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> homeTitle() {
		return new HQLSafePathBasicString<String>(this, path + ".homeTitle", queryBuilder, String.class);
	}

	/**
	 * @return Path to mainContent of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> mainContent() {
		return new HQLSafePathBasicString<String>(this, path + ".mainContent", queryBuilder, String.class);
	}

	/**
	 * @return Path to iso3Language of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> iso3Language() {
		return new HQLSafePathBasicString<String>(this, path + ".iso3Language", queryBuilder, String.class);
	}



}

