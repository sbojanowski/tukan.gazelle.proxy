package net.ihe.gazelle.proxy.model.message;

import java.util.ArrayList;
import java.util.Date;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class AbstractMessageAttributes<T extends AbstractMessage> extends HQLSafePathEntity<T> {

	public AbstractMessageAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return AbstractMessage.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to proxySide of type net.ihe.gazelle.proxy.netty.channel.ProxySide
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<ProxySide> proxySide() {
		return new HQLSafePathBasic<ProxySide>(this, path + ".proxySide", queryBuilder, ProxySide.class);
	}

	/**
	 * @return Path to toIP of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> toIP() {
		return new HQLSafePathBasicString<String>(this, path + ".toIP", queryBuilder, String.class);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to index of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> index() {
		return new HQLSafePathBasicString<String>(this, path + ".index", queryBuilder, String.class);
	}

	/**
	 * @return Path to connection of type net.ihe.gazelle.proxy.model.message.Connection
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public ConnectionEntity<Connection> connection() {
		return new ConnectionEntity<Connection>(path + ".connection", queryBuilder);
	}

	/**
	 * @return Path to indexInt of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> indexInt() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".indexInt", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to fromIP of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> fromIP() {
		return new HQLSafePathBasicString<String>(this, path + ".fromIP", queryBuilder, String.class);
	}

	/**
	 * @return Path to resultOid of type java.util.ArrayList
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<ArrayList> resultOid() {
		return new HQLSafePathBasic<ArrayList>(this, path + ".resultOid", queryBuilder, ArrayList.class);
	}

	/**
	 * @return Path to messageReceived of type byte[]
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<byte[]> messageReceived() {
		return new HQLSafePathBasic<byte[]>(this, path + ".messageReceived", queryBuilder, byte[].class);
	}

	/**
	 * @return Path to remotePort of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> remotePort() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".remotePort", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to localPort of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> localPort() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".localPort", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to proxyPort of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> proxyPort() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".proxyPort", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to dateReceived of type java.util.Date
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicDate<Date> dateReceived() {
		return new HQLSafePathBasicDate<Date>(this, path + ".dateReceived", queryBuilder, Date.class);
	}



}

