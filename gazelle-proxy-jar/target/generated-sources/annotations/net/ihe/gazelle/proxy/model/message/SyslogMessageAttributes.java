package net.ihe.gazelle.proxy.model.message;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class SyslogMessageAttributes<T extends SyslogMessage> extends net.ihe.gazelle.proxy.model.message.AbstractMessageAttributes<T> {

	public SyslogMessageAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return SyslogMessage.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to appName of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> appName() {
		return new HQLSafePathBasicString<String>(this, path + ".appName", queryBuilder, String.class);
	}

	/**
	 * @return Path to timestamp of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> timestamp() {
		return new HQLSafePathBasicString<String>(this, path + ".timestamp", queryBuilder, String.class);
	}

	/**
	 * @return Path to procId of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> procId() {
		return new HQLSafePathBasicString<String>(this, path + ".procId", queryBuilder, String.class);
	}

	/**
	 * @return Path to facility of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> facility() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".facility", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to tag of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> tag() {
		return new HQLSafePathBasicString<String>(this, path + ".tag", queryBuilder, String.class);
	}

	/**
	 * @return Path to hostName of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> hostName() {
		return new HQLSafePathBasicString<String>(this, path + ".hostName", queryBuilder, String.class);
	}

	/**
	 * @return Path to severity of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> severity() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".severity", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to messageId of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> messageId() {
		return new HQLSafePathBasicString<String>(this, path + ".messageId", queryBuilder, String.class);
	}

	/**
	 * @return Path to payLoad of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> payLoad() {
		return new HQLSafePathBasicString<String>(this, path + ".payLoad", queryBuilder, String.class);
	}



}

