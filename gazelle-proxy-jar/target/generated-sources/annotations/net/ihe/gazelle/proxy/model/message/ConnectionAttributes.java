package net.ihe.gazelle.proxy.model.message;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ConnectionAttributes<T extends Connection> extends HQLSafePathEntity<T> {

	public ConnectionAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Connection.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to uuid of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> uuid() {
		return new HQLSafePathBasicString<String>(this, path + ".uuid", queryBuilder, String.class);
	}

	/**
	 * @return Path to messages of type net.ihe.gazelle.proxy.model.message.AbstractMessage
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = true, notNull = false)
	public AbstractMessageAttributes<AbstractMessage> messages() {
		return new AbstractMessageAttributes<AbstractMessage>(path + ".messages", queryBuilder);
	}



}

