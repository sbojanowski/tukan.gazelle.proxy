package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.*;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("proxyBean")
public class ProxyBean implements ProxyLocal, Comparator<ProxyBean> {

    public static Logger log = Logger.getLogger(ProxyBean.class);

    private List<Proxy<?, ?>> listOfProxies;

    private String ips;

    @Create
    public void startProxy() {
        listOfProxies = new ArrayList<Proxy<?, ?>>();

        StringBuilder sb = new StringBuilder("");
        try {
            boolean first = true;
            Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) e.nextElement();
                Enumeration<InetAddress> e2 = ni.getInetAddresses();
                while (e2.hasMoreElements()) {
                    InetAddress ip = (InetAddress) e2.nextElement();
                    if (ip instanceof Inet4Address) {
                        String hostName = ip.getHostName();
                        if (!hostName.startsWith("localhost")) {
                            if (!first) {
                                sb.append(", ");
                            }
                            sb.append(ip.getHostAddress());
                            sb.append(" (");
                            sb.append(hostName);
                            sb.append(")");
                            first = false;
                        }
                    }
                }
            }
        } catch (Throwable e) {
            log.error(e);
        }
        ips = sb.toString();
    }

    Comparator<Proxy<?, ?>> comparator = new Comparator<Proxy<?, ?>>() {

        @Override
        public int compare(Proxy<?, ?> c1, Proxy<?, ?> c2) {
            String host1, host2;
            if (c1 == null || c1.getProxyConsumerHost() == null) {
                host1 = "";
            } else {
                host1 = c1.getProxyConsumerHost();
            }
            if (c2 == null || c2.getProxyConsumerHost() == null) {
                host2 = "";
            } else {
                host2 = c2.getProxyConsumerHost();
            }
            return host1.compareTo(host2);
        }
    };

    public List<Proxy<?, ?>> getListOfProxies() {
        if (listOfProxies != null) {
            Collections.sort(listOfProxies, comparator);
        } else {
            listOfProxies = new ArrayList<Proxy<?, ?>>();
        }
        return listOfProxies;
    }

    public GazelleListDataModel<Proxy<?, ?>> getListOfProxies2() {
        List<Proxy<?, ?>> res = getListOfProxies();
        GazelleListDataModel<Proxy<?, ?>> dm = new GazelleListDataModel<Proxy<?, ?>>(res);
        return dm;
    }

    @Destroy
    public void stopProxy() {
        List<Integer> ports = new ArrayList<Integer>();
        for (Proxy<?, ?> proxy : listOfProxies) {
            ports.add(Integer.valueOf(proxy.getProxyProviderPort()));
        }

        for (Integer port : ports) {
            stopChannel(port);
        }
    }

    public Proxy<?, ?> startChannel(ConnectionConfig connectionConfig) {
        if (connectionConfig.getChannelType() != null) {
            Proxy<?, ?> proxy = connectionConfig.getChannelType().getProxy(connectionConfig);
            proxy.start();
            if (!proxy.iscException()) {
                listOfProxies.add(proxy);
            } else {
                FacesMessages.instance().add(Severity.ERROR,
                        "Failed to start " + proxy.getProxyConsumerHost() + " on port " + proxy.getProxyProviderPort());
            }
            return proxy;
        } else {
            return null;
        }
    }

    public void stopChannel(Integer localPort) {
        Proxy<?, ?> proxyChannel = findProxyWithLocalPort(localPort);
        if (proxyChannel != null) {
            proxyChannel.stop();
            listOfProxies.remove(proxyChannel);
        }
    }

    private Proxy<?, ?> findProxyWithLocalPort(int localPort) {
        for (Proxy<?, ?> proxyChannel : listOfProxies) {
            if (proxyChannel.getProxyProviderPort() == localPort) {
                return proxyChannel;
            }
        }
        return null;
    }

    public String getIps() {
        return ips;
    }

    public boolean channelExists(int localPort) {
        Proxy<?, ?> findProxyWithLocalPort = findProxyWithLocalPort(localPort);
        return findProxyWithLocalPort != null;
    }

    @Override
    public int compare(ProxyBean o1, ProxyBean o2) {
        // TODO Auto-generated method stub
        return 0;
    }

}
