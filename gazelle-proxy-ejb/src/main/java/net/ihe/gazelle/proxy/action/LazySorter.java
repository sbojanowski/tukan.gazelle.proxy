package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;

public class LazySorter implements Comparator<Proxy<?, ?>> {

    private static Logger log = LoggerFactory.getLogger(LazySorter.class);

    private String sortField;

    private SortOrder sortOrder;

    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public int compare(Proxy<?, ?> conf1, Proxy<?, ?> conf2) {
        try {
            Object value1 = Proxy.class.getField(this.sortField).get(conf1);
            Object value2 = Proxy.class.getField(this.sortField).get(conf2);
            log.info(sortField + " 1 original : " + value1 + " port : " + conf1.getProxyProviderPort());
            log.info(sortField + " 2 original : " + value2 + " port : " + conf2.getProxyProviderPort());
            if (value1 == null) {
                conf1.getChannelType();
                conf1.getProxyConsumerHost();
                conf1.getProxyConsumerPort();
                conf1.getProxyProviderPort();
                value1 = Proxy.class.getField(this.sortField).get(conf1);
                log.info(sortField + " 1 init : " + value1 + " port : " + conf1.getProxyProviderPort());
            }
            if (value2 == null) {
                conf2.getChannelType();
                conf2.getProxyConsumerHost();
                conf2.getProxyConsumerPort();
                conf2.getProxyProviderPort();
                value2 = Proxy.class.getField(this.sortField).get(conf2);
                log.info(sortField + " 2 init : " + value2 + " port : " + conf2.getProxyProviderPort());
            }

            int value = ((Comparable) value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (Exception e) {
            log.error("" + e);
            throw new RuntimeException();
        }
    }
}
