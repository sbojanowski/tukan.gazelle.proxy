package net.ihe.gazelle.proxy.ws;

import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.proxy.model.tm.Configuration;
import net.ihe.gazelle.proxy.model.tm.TestInstance;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.soap.SOAPException;
import java.util.List;

public interface IProxyForTM {

    @WebMethod
    public void startAllChannels(
            @WebParam(name = "configurationsList")
            List<Configuration> configurations) throws SOAPException;

    @WebMethod
    public void startTestInstance(
            @WebParam(name = "testInstance")
            TestInstance testInstance) throws HibernateFailure;

    @WebMethod
    public void markTestStep(
            @WebParam(name = "testStepId")
            int testStepId) throws HibernateFailure;

    @WebMethod
    public String getMinProxyPort();

    @WebMethod
    public String getMaxProxyPort();

}