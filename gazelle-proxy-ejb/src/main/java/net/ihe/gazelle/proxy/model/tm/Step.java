package net.ihe.gazelle.proxy.model.tm;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tm_step", schema = "public")
@XmlAccessorType(XmlAccessType.NONE)
public class Step {

    @Id
    @GeneratedValue
    private int id;

    @XmlElement(name = "id")
    private int tmId;

    @XmlElement
    private Integer stepIndex;

    @ManyToOne(fetch = FetchType.EAGER)
    private TestInstance testInstance;

    @ManyToOne
    private AbstractMessage message;

    @ManyToMany
    @JoinTable(name = "tm_step_sender")
    private List<Configuration> senders;

    @Transient
    @XmlElement
    private List<Integer> senderIds;

    @ManyToMany
    @JoinTable(name = "tm_step_receiver")
    private List<Configuration> receivers;

    @Transient
    @XmlElement
    private List<Integer> receiverIds;

    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTmId() {
        return tmId;
    }

    public void setTmId(int tmId) {
        this.tmId = tmId;
    }

    public TestInstance getTestInstance() {
        return testInstance;
    }

    public void setTestInstance(TestInstance testInstance) {
        this.testInstance = testInstance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AbstractMessage getMessage() {
        return message;
    }

    public void setMessage(AbstractMessage message) {
        this.message = message;
    }

    public List<Configuration> getSenders() {
        return senders;
    }

    public void setSenders(List<Configuration> senders) {
        this.senders = senders;
    }

    public List<Integer> getSenderIds() {
        return senderIds;
    }

    public void setSenderIds(List<Integer> senderIds) {
        this.senderIds = senderIds;
    }

    public List<Configuration> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<Configuration> receivers) {
        this.receivers = receivers;
    }

    public List<Integer> getReceiverIds() {
        return receiverIds;
    }

    public void setReceiverIds(List<Integer> receiverIds) {
        this.receiverIds = receiverIds;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + tmId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Step other = (Step) obj;
        if (tmId != other.tmId) {
            return false;
        }
        return true;
    }

}
