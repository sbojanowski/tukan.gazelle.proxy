package net.ihe.gazelle.proxy.ws;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.proxy.action.ProxyLocal;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.dao.ProxyDAOTM;
import net.ihe.gazelle.proxy.model.tm.Configuration;
import net.ihe.gazelle.proxy.model.tm.Step;
import net.ihe.gazelle.proxy.model.tm.TestInstance;
import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.*;
import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@WebService
@Stateless
@HandlerChain(file = "handlers.xml")
public class ProxyForTM implements IProxyForTM {

    private static Logger log = LoggerFactory.getLogger(ProxyForTM.class);

    public ProxyForTM() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ihe.gazelle.proxy.ws.IProxyForTM#startAllChannels(java.util.List)
     */
    @WebMethod
    public void startAllChannels(
            @WebParam(name = "configurationsList")
                    List<Configuration> configurations) throws SOAPException {
        if (null == configurations){
            throw new SOAPException("You must provide a configuration");
        }
        for (Configuration configuration : configurations) {
            configuration.setTestInstance(null);
            startChannel(configuration);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ihe.gazelle.proxy.ws.IProxyForTM#startTestInstance(net.ihe.gazelle
     * .proxy.model.tm.TestInstance)
     */
    @WebMethod
    public void startTestInstance(
            @WebParam(name = "testInstance")
                    TestInstance testInstance) throws HibernateFailure {
        if (null == testInstance){
            throw new HibernateFailure("You must provide a configuration");
        }
        Calendar endTI = Calendar.getInstance();
        testInstance.setDate(endTI.getTime());

        endTI.add(Calendar.YEAR, 1);

        List<Step> steps = testInstance.getSteps();
        List<Configuration> configurations = testInstance.getConfigurations();

        if (steps != null) {
            for (Step step : steps) {
                // never stopped
                step.setDate(null);

                step.setTestInstance(testInstance);
                step.setMessage(null);
                List<Integer> ids = step.getReceiverIds();
                List<Configuration> stepConfigurations = new ArrayList<Configuration>();
                if (ids != null) {
                    stepConfigurations = getStepConfigurations(configurations, ids, true);
                }
                step.setReceivers(stepConfigurations);

                ids = step.getSenderIds();
                stepConfigurations = new ArrayList<Configuration>();
                if (ids != null) {
                    stepConfigurations = getStepConfigurations(configurations, ids, false);
                }
                step.setSenders(stepConfigurations);
            }
        }
        if (configurations != null) {
            for (int i = 0; i < configurations.size(); i++) {
                Configuration configuration = configurations.get(i);
                if (configuration != null) {
                    configuration.setTestInstance(testInstance);
                    startChannel(configuration);
                }
            }
        }

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.persist(testInstance);

    }

    private List<Configuration> getStepConfigurations(List<Configuration> configurations, List<Integer> ids,
                                                      boolean receiver) {
        List<Configuration> stepConfigurations = new ArrayList<Configuration>();
        for (Integer id : ids) {
            Configuration stepConfiguration = getConfigurationByTmId(id, configurations);
            if (stepConfiguration != null) {
                if (receiver) {
                    if (stepConfiguration.getProxyPort() > 0) {
                        stepConfigurations.add(stepConfiguration);
                    }
                } else {
                    stepConfigurations.add(stepConfiguration);
                }
            }
        }
        return stepConfigurations;
    }

    private void startChannel(Configuration configuration) {
        if (configuration.getProxyPort() > 0 && configuration.getProxyPort() < 65536 && configuration.getPort() > 0
                && configuration.getPort() < 65536) {
            ProxyLocal proxyBean = (ProxyLocal) Component.getInstance("proxyBean");
            if (!proxyBean.channelExists(configuration.getProxyPort())) {
                proxyBean.startChannel(new TMConfiguration(configuration));
            }
        }
    }

    private Configuration getConfigurationByTmId(int id, List<Configuration> configurations) {
        for (Configuration configuration : configurations) {
            if (configuration.getTmId() == id) {
                return configuration;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ihe.gazelle.proxy.ws.IProxyForTM#markTestStep(int)
     */
    @WebMethod
    public void markTestStep(
            @WebParam(name = "testStepId")
                    int testStepId) {
        ProxyDAOTM.updateStepDate(testStepId);
    }

    @WebMethod
    public String getMinProxyPort() {
        return ApplicationConfiguration.getValueOfVariable("min_proxy_port");
    }

    @WebMethod
    public String getMaxProxyPort() {
        return ApplicationConfiguration.getValueOfVariable("max_proxy_port");
    }

    @WebMethod
    public
    @WebResult(name = "channels")
    List<Configuration> getAllActiveChannels() {
        List<Configuration> list = new ArrayList<Configuration>();
        ProxyLocal proxyBean = (ProxyLocal) Component.getInstance("proxyBean");
        List<Proxy<?, ?>> proxies = proxyBean.getListOfProxies();
        for (Proxy<?, ?> proxy : proxies) {
            Configuration connConfig = new Configuration();
            connConfig.setType(proxy.getChannelType());
            connConfig.setProxyPort(proxy.getProxyProviderPort());
            connConfig.setHost(proxy.getProxyConsumerHost());
            connConfig.setPort(proxy.getProxyConsumerPort());
            list.add(connConfig);
        }
        return list;
    }

    /**
     * Returns true if the provided proxy port is being listened on; false otherwise
     *
     * @param proxyPort : int
     * @return if channel exist
     */
    @WebMethod
    public boolean channelStarted(
            @WebParam(name = "proxyPort")
                    int proxyPort) {
        ProxyLocal proxyBean = (ProxyLocal) Component.getInstance("proxyBean");
        return proxyBean.channelExists(proxyPort);
    }
}
