/**
 * Copyright 2009 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.action;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfigSimple;
import net.ihe.gazelle.proxy.netty.Proxy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Name("channelManagerBean")
@Scope(ScopeType.PAGE)
public class ChannelManagerBean implements Serializable {

    private static final long serialVersionUID = 1879075873564126607L;

    private static Logger log = LoggerFactory.getLogger(ChannelManagerBean.class);
    @In
    Identity identity;
    @In
    private ProxyLocal proxyBean;
    private ConnectionConfigSimple configuration;
    private int channelToStop;

    public ChannelManagerBean() {
        super();
        this.configuration = new ConnectionConfigSimple();
        this.configuration.setLocalPort(ApplicationConfigurationManager.instance().getMinProxyPort());
    }

    public ChannelType getMessageType() {
        return this.configuration.getChannelType();
    }

    public void setMessageType(ChannelType messageType) {
        this.configuration.setChannelType(messageType);
    }

    public ChannelType[] getMessageTypes() {
        return ChannelType.values();
    }

    public Integer getProxyPort() {
        return this.configuration.getProxyProviderPort();
    }

    public void setProxyPort(Integer proxyPort) {
        this.configuration.setLocalPort(proxyPort);
    }

    public String getResponderIP() {
        return this.configuration.getProxyConsumerHost();
    }

    public void setResponderIP(String responderIP) {
        this.configuration.setRemoteHost(responderIP.trim());
    }

    public Integer getResponderPort() {
        return this.configuration.getProxyConsumerPort();
    }

    public void setResponderPort(Integer responderPort) {
        this.configuration.setRemotePort(responderPort);
    }

    public void setChannelToStop(int proxyPort) {
        channelToStop = proxyPort;
    }

    public String startChannel() {
        ApplicationConfigurationManagerLocal manager = ApplicationConfigurationManager.instance();
        if (manager.isUserAllowedAsAdmin()) {
            if (startSimpleChannel(configuration)) {
                return "/channels.xhtml";
            } else {
                return null;
            }
        } else {
            FacesMessages.instance().add(Severity.ERROR, "You do not have the permission to do this");
        }
        return null;
    }

    private boolean startSimpleChannel(ConnectionConfigSimple config) {
        try {
            proxyBean.startChannel(config);
            FacesMessages.instance().add(Severity.INFO, "Channel started");
            return true;
        } catch (Throwable e) {
            log.error("Failed to start channel on port " + config.getProxyProviderPort(), e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Failed to start channel on port " + config.getProxyProviderPort() + ": " + e.getMessage());
            return false;
        }
    }

    public void stopChannel(int localPort) {
        ApplicationConfigurationManagerLocal manager = ApplicationConfigurationManager.instance();
        if (manager.isUserAllowedAsAdmin()) {
            try {
                proxyBean.stopChannel(localPort);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Channel successfully stopped");
            } catch (Throwable e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Failed to stop channel : " + ExceptionUtils.getFullStackTrace(e));
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You do not have the permission to do this");
        }
    }

    public void portToStop(int portToStop) {
        if (portToStop > 0) {
            setChannelToStop(portToStop);
        }
    }

    public void stopChannelReal() {
        stopChannel(channelToStop);
    }

    public void stopAllChannels() {
        if (!channelListIsEmpty()) {
            List<Proxy<?, ?>> lp = proxyBean.getListOfProxies();
            int nbOfChannelsToStop = lp.size();
            for (int i = 0; i < nbOfChannelsToStop; i++) {
                stopChannel(lp.get(0).getProxyProviderPort());
            }
            FacesMessages.instance().clearGlobalMessages();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "All channels are now closed !");
        } else {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR, "Failed to stop channel : no proxy channel open !");
        }
    }

    public void configFileUploadListener(FileUploadEvent event) {
        ApplicationConfigurationManagerLocal manager = ApplicationConfigurationManager.instance();
        if (manager.isUserAllowedAsAdmin()) {
            if (event.getUploadedFile().getContentType().contains("xml")) {
                // TODO
            } else {
                ColumnPositionMappingStrategy<ConnectionConfigSimple> strategy = new ColumnPositionMappingStrategy<ConnectionConfigSimple>();
                strategy.setType(ConnectionConfigSimple.class);
                // set the fields to bind to the CSV columns
                String[] columns = new String[]{"channelTypeAsString", "localPort", "remoteHost", "remotePort"};
                strategy.setColumnMapping(columns);
                CsvToBean<ConnectionConfigSimple> csv = new CsvToBean<ConnectionConfigSimple>();
                FileInputStream fileInputStream = null;
                InputStreamReader inputStreamReader = null;
                CSVReader csvReader = null;
                try {
                    File file = new File("test");
                    FileUtils.writeByteArrayToFile(file, event.getUploadedFile().getData());

                    fileInputStream = new FileInputStream(file);
                    inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
                    csvReader = new CSVReader(inputStreamReader);
                    List<ConnectionConfigSimple> configurations = csv.parse(strategy, csvReader);
                    int min = ApplicationConfigurationManager.instance().getMinProxyPort();
                    int max = ApplicationConfigurationManager.instance().getMaxProxyPort();
                    int channelsOpened = 0;
                    for (ConnectionConfigSimple conf : configurations) {
                        conf.setChannelType(ChannelType.getEnumValue(conf.getChannelTypeAsString()));
                        if (conf.getProxyProviderPort() >= min && conf.getProxyProviderPort() <= max) {
                            boolean ok = startSimpleChannel(conf);
                            FacesMessages.instance().clearGlobalMessages();
                            if (ok) {
                                channelsOpened++;
                            }
                        } else {
                            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                                    "Port " + conf.getProxyProviderPort() + " is out of range [" + min + ", " + max
                                            + "]");
                        }
                    }
                    FacesMessages.instance().add(channelsOpened + " channels have been opened on proxy");
                } catch (FileNotFoundException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Application has not been able to process the uploaded file: " + e.getMessage());
                    log.error(e.getMessage(), e);
                } catch (RuntimeException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Application has not been able to process the uploaded file: " + e.getMessage());
                    log.error(e.getMessage(), e);
                } catch (IOException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Application has not been able to process the uploaded file: " + e.getMessage());
                    log.error(e.getMessage(), e);
                } finally {
                    try {
                        if (fileInputStream != null) {
                            fileInputStream.close();
                        }
                        if (inputStreamReader != null) {
                            inputStreamReader.close();
                        }
                        if (csvReader != null) {
                            csvReader.close();
                        }
                    } catch (IOException e) {
                        log.error(e.getMessage());
                    }
                }
            }
        }
    }

    public boolean channelListIsEmpty() {
        List<Proxy<?, ?>> lp = proxyBean.getListOfProxies();
        return (lp == null || lp.isEmpty());
    }

    public void download() {
        if (!channelListIsEmpty()) {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            ServletOutputStream servletOutputStream = null;
            response.setContentType("text/csv");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=\"proxyChannels.csv\"");

            try {
                servletOutputStream = response.getOutputStream();
                OutputStreamWriter streamWriter = new OutputStreamWriter(servletOutputStream, StandardCharsets.UTF_8);
                CSVWriter writer = new CSVWriter(streamWriter);
                for (Proxy<?, ?> proxy : proxyBean.getListOfProxies()) {
                    writer.writeNext(proxy.getConnectionConfigAsArray());
                }
                writer.close();
                streamWriter.close();
                servletOutputStream.close();
                FacesContext.getCurrentInstance().responseComplete();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public void goHome() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/home.seam");
        } catch (IOException e) {
            log.error("" + e);
        }
    }
}
