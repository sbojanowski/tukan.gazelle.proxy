package net.ihe.gazelle.proxy.ws;

import net.ihe.gazelle.proxy.model.tm.Configuration;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import org.jboss.netty.channel.ChannelHandler;

import java.util.ArrayList;
import java.util.List;

public class TMConfiguration implements ConnectionConfig {

    private Configuration configuration;

    public TMConfiguration(Configuration configuration) {
        super();
        this.configuration = configuration;
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyProvider() {
        return new ArrayList<ChannelHandler>();
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyConsumer() {
        return new ArrayList<ChannelHandler>();
    }

    @Override
    public int getProxyProviderPort() {
        return configuration.getProxyPort();
    }

    @Override
    public String getProxyConsumerHost() {
        return configuration.getHost();
    }

    @Override
    public int getProxyConsumerPort() {
        return configuration.getPort();
    }

    @Override
    public ChannelType getChannelType() {
        return configuration.getType();
    }

}
