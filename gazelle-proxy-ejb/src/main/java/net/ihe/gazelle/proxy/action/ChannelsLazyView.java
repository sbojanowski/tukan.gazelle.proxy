package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

@Name("channelsLazyView")
@Scope(ScopeType.PAGE)
public class ChannelsLazyView extends LazyDataModel<Proxy<?, ?>> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8037377555209647566L;

    private LazyDataModel<Proxy<?, ?>> lazyModel;

    private Proxy<?, ?> selectedConfig;

    public Proxy<?, ?> getSelectedConfig() {
        return selectedConfig;
    }

    public void setSelectedConfig(Proxy<?, ?> selectedConfig) {
        this.selectedConfig = selectedConfig;
    }

    @PostConstruct
    public void init() {
        ProxyLocal proxyBean = (ProxyLocal) Component.getInstance("proxyBean");
        List<Proxy<?, ?>> data = proxyBean.getListOfProxies();
        lazyModel = new LazyProxyListDataModel(data);
    }

    public LazyDataModel<Proxy<?, ?>> getLazyModel() {
        return lazyModel;
    }

    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Proxy configuration selected",
                Integer.toString(((Proxy<?, ?>) event.getObject()).getProxyConsumerPort()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Proxy configuration Unselected",
                Integer.toString(((Proxy<?, ?>) event.getObject()).getProxyConsumerPort()));

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    @Override
    public Object getRowKey(Proxy<?, ?> object) {
        return "" + object.getProxyProviderPort();
    }

    @Override
    public Proxy<?, ?> getRowData(String rowKey) {
        List<Proxy<?, ?>> confs = (List<Proxy<?, ?>>) getWrappedData();
        for (Proxy<?, ?> conf : confs) {
            String proxyport = Integer.toString(conf.getProxyProviderPort());
            if (proxyport.equals(rowKey)) {
                return conf;
            }
        }
        return super.getRowData(rowKey);
    }

}
