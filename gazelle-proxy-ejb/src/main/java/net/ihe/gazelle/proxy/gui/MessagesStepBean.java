package net.ihe.gazelle.proxy.gui;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.proxy.dao.MessageFilterStep;
import net.ihe.gazelle.proxy.dao.ProxyDAO;
import net.ihe.gazelle.proxy.dao.ProxyDAOTM;
import net.ihe.gazelle.proxy.model.message.*;
import net.ihe.gazelle.proxy.model.tm.Step;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.Redirect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;

@Name("messagesStepBean")
@Scope(ScopeType.PAGE)
public class MessagesStepBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4415807034648117829L;

    private static Logger log = LoggerFactory.getLogger(MessagesStepBean.class);

    private transient HibernateMessageDataModel<AbstractMessage> messageDataModel;
    private MessageFilterStep messageFilterStep;

    @In
    private EntityManager entityManager;

    private Step step = null;
    private String currentSelectDate;

    public void clearFilter() {
        rebuildDataModel();
    }

    public void refreshFilter() {
        rebuildDataModel();
    }

    @Create
    public void createModel() {
        messageFilterStep = new MessageFilterStep(step);
        rebuildDataModel();
    }

    @Remove
    @Destroy
    public void destroy() {
    }

    public HibernateDataModel<?> getMessageDataModel() {
        return messageDataModel;
    }

    public List<ChannelType> getMessageTypes() {
        return Arrays.asList(ChannelType.values());
    }

    public List<ProxySide> getProxySides() {
        return Arrays.asList(ProxySide.values());
    }

    public SelectItem[] getDicomAffectedSopClassUIDs() {
        List<String> result = new ArrayList<String>();
        Collections.sort(result);
        return getSelectItems(result);
    }

    private SelectItem[] getSelectItems(List<String> list) {
        SelectItem[] result = new SelectItem[list.size() + 1];
        int i = 0;
        result[i] = new SelectItem(null, "--");
        i++;
        for (String option : list) {
            result[i] = new SelectItem(option, option);
            i++;
        }
        return result;
    }

    public SelectItem[] getDicomRequestedSopClassUIDs() {
        return getDicomAffectedSopClassUIDs();
    }

    public SelectItem[] getDicomCommandFields() {
        List<String> result = new ArrayList<String>();
        Collections.sort(result);
        return getSelectItems(result);
    }

    public boolean isFilterDates() {
        return messageFilterStep.isFilterDates();
    }

    public void setFilterDates(boolean filterDates) {
        messageFilterStep.setFilterDates(filterDates);
    }

    public boolean isFilterPathFrom() {
        return messageFilterStep.isFilterPathFrom();
    }

    public void setFilterPathFrom(boolean filterPathFrom) {
        messageFilterStep.setFilterPathFrom(filterPathFrom);
    }

    public String getStepFilter() {
        return messageFilterStep.getStepFilter();
    }

    private void rebuildDataModel() {
        messageDataModel = new HibernateMessageDataModel<AbstractMessage>(messageFilterStep);
    }

    public void redirectToMessageFromURL() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String id = (String) fc.getExternalContext().getRequestParameterMap().get("id");
        AbstractMessage message = ProxyDAO.getMessageByID(Integer.valueOf(id));

        Redirect redirect = Redirect.instance();
        redirect.setParameter("id", id);

        String viewId = "";
        if (message instanceof HL7Message) {
            viewId = "/messages/hl7.xhtml";
        }
        if (message instanceof RawMessage) {
            viewId = "/messages/raw.xhtml";
        }
        if (message instanceof DicomMessage) {
            viewId = "/messages/dicom.xhtml";
        }
        if (message instanceof SyslogMessage) {
            viewId = "/messages/syslog.xhtml";
        }
        if (message instanceof HTTPMessage) {
            viewId = "/messages/http.xhtml";
        }

        redirect.setViewId(viewId);
        redirect.execute();
    }

    public String redirectToMessage(AbstractMessage message) {
        if (message.getId() == null) {
            return null;
        }
        if (message instanceof HL7Message) {
            return "/messages/hl7.seam?id=" + message.getId();
        }
        if (message instanceof RawMessage) {
            return "/messages/raw.seam?id=" + message.getId();
        }
        if (message instanceof DicomMessage) {
            return "/messages/dicom.seam?id=" + message.getId();
        }
        if (message instanceof SyslogMessage) {
            return "/messages/syslog.seam?id=" + message.getId();
        }
        if (message instanceof HTTPMessage) {
            return "/messages/http.seam?id=" + message.getId();
        }
        return null;
    }

    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String testStepId = params.get("id");
        List<Step> steps = ProxyDAOTM.getStepsByTMId(Integer.valueOf(testStepId));
        if (steps.size() > 0) {
            step = steps.get(0);
        }
        messageFilterStep = new MessageFilterStep(step);

        rebuildDataModel();
    }

    public boolean isFilterPathReceiver() {
        return messageFilterStep.isFilterPathReceiver();
    }

    public void setFilterPathReceiver(boolean filterPathReceiver) {
        messageFilterStep.setFilterPathReceiver(filterPathReceiver);
    }

    public boolean isFilterPersoDate() {
        return messageFilterStep.isFilterPersoDate();
    }

    public void setFilterPersoDate(boolean filterPersoDate) {
        messageFilterStep.setFilterPersoDate(filterPersoDate);
    }

    public Date getStartDate() {
        return messageFilterStep.startDate;
    }

    public void setStartDate(Date startDate) {
        messageFilterStep.setStartDate(startDate);
    }

    public Date getEndDate() {
        return messageFilterStep.endDate;
    }

    public void setEndDate(Date endDate) {
        messageFilterStep.setEndDate(endDate);
    }

    public void dateSelection(String value) {
        if (value.equals("1")) {
            setFilterPersoDate(false);
        } else {
            setFilterDates(false);
        }
    }

    public String getCurrentSelectDate() {
        return currentSelectDate;
    }

    public void setCurrentSelectDate(String currentSelectDate) {
        this.currentSelectDate = currentSelectDate;
    }

    public void resetFilter() {
        setFilterPathFrom(false);
        setFilterPathReceiver(false);
        setFilterDates(false);
        setFilterPersoDate(false);
    }

    public void selectDate(String date) {
        if (date != null && !(date.equals("--")) && !(date.isEmpty())) {
            Date startDate, endDate = null;
            startDate = new Date();
            Calendar cal = new GregorianCalendar();

            if (date.equals("today")) {
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("yesterday")) {
                if (cal.get(Calendar.DAY_OF_YEAR) == 1) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.MONTH, cal.get(Calendar.DECEMBER));
                    cal.set(Calendar.DAY_OF_MONTH, 31);
                } else {
                    cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - 1));
                }
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("this week")) {
                if (cal.get(Calendar.DAY_OF_YEAR) < 7) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.MONTH, cal.get(Calendar.DECEMBER));
                    cal.set(Calendar.DAY_OF_MONTH, 31 - (7 - cal.get(Calendar.DAY_OF_YEAR)));
                }
                cal.set(Calendar.DAY_OF_WEEK, 2);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("this month")) {
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("last week")) {
                if (cal.get(Calendar.WEEK_OF_YEAR) == 1) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.WEEK_OF_YEAR, 52);
                } else {
                    cal.set(Calendar.WEEK_OF_YEAR, (cal.get(Calendar.WEEK_OF_YEAR) - 1));
                }
                cal.set(Calendar.DAY_OF_WEEK, 2);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("last month")) {
                if (cal.get(Calendar.MONTH) == 0) {
                    cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) - 1));
                    cal.set(Calendar.MONTH, cal.get(Calendar.DECEMBER));
                } else {
                    cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH) - 1));
                }
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
            }
            if (date.equals("last hour")) {
                cal.set(Calendar.HOUR, (cal.get(Calendar.HOUR) - 1));
            }
            startDate = cal.getTime();
            endDate = new Date();

            cal = new GregorianCalendar();
            if (date.equals("yesterday")) {
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                endDate = cal.getTime();
            }
            if (date.equals("last week")) {
                cal.set(Calendar.WEEK_OF_YEAR, cal.get(Calendar.WEEK_OF_YEAR));
                cal.set(Calendar.DAY_OF_WEEK, 2);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                endDate = cal.getTime();
            }
            if (date.equals("last month")) {
                cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                endDate = cal.getTime();
            }
            messageFilterStep.setStartDate(startDate);
            messageFilterStep.setEndDate(endDate);

        } else {
            log.error("Date problem");
        }

    }
}
