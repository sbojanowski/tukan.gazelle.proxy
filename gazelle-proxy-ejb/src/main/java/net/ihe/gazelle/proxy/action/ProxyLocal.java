package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ProxyLocal {

    public Proxy<?, ?> startChannel(ConnectionConfig tlsConfig);

    public void stopChannel(Integer localPort);

    public boolean channelExists(int localPort);

    List<Proxy<?, ?>> getListOfProxies();

}
