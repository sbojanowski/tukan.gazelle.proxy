package net.ihe.gazelle.proxy;

import net.ihe.gazelle.proxy.action.ProxyBean;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfigSimple;
import net.ihe.gazelle.proxy.netty.Proxy;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestProxyDB {

    @Test
    public void test_start_channel() {
        // Port 0 assign a random open port
        Proxy<?, ?> proxy = ChannelType.HTTP
                .getProxy(new ConnectionConfigSimple(0, "127.0.0.1", 8080, ChannelType.HTTP));
        proxy.start();
        assertTrue(proxy.isOpen());
        proxy.stop();
        assertFalse(proxy.isOpen());
        assertEquals(":0 -> 127.0.0.1:8080", proxy.getInfo());
    }

    @Test
    public void test_start_all_channels() {
        ProxyBean pb = new ProxyBean();
        pb.startProxy();
        ConnectionConfigSimple connectionConfig = new ConnectionConfigSimple(0, "127.0.0.1", 8080, ChannelType.HTTP);
        pb.startChannel(connectionConfig);
        assertEquals(1, pb.getListOfProxies().size());
        assertTrue(pb.channelExists(0));
        pb.stopChannel(0);
        assertFalse(pb.channelExists(0));
        pb.stopProxy();
        assertEquals(0, pb.getListOfProxies().size());
    }
}
