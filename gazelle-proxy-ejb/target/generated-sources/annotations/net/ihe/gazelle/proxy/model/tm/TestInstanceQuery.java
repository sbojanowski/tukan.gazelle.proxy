package net.ihe.gazelle.proxy.model.tm;

import javax.persistence.EntityManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class TestInstanceQuery extends TestInstanceEntity<TestInstance> implements HQLQueryBuilderInterface<TestInstance> {

	public TestInstanceQuery() {
		super("this", new HQLQueryBuilder(TestInstance.class));
	}

	public TestInstanceQuery(HQLQueryBuilder<?> queryBuilder) {
		super("this", queryBuilder);
	}
	
	public TestInstanceQuery(EntityManager entityManager) {
		super("this", new HQLQueryBuilder(entityManager,TestInstance.class));
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
		return queryBuilder.getCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TestInstance> getList() {
		return (List<TestInstance>) queryBuilder.getList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TestInstance> getListNullIfEmpty() {
		return (List<TestInstance>) queryBuilder.getListNullIfEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TestInstance getUniqueResult() {
		return (TestInstance) queryBuilder.getUniqueResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HQLStatistic<TestInstance>> getListWithStatistics(List<HQLStatisticItem> items) {
		List<?> result = queryBuilder.getListWithStatistics(items);
		return (List<HQLStatistic<TestInstance>>) result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> getListWithStatisticsItems(List<HQLStatisticItem> items, HQLStatistic<TestInstance> item,
			int statisticItemIndex) {
		HQLStatistic safeItem = item;
		List<Object> result = queryBuilder.getListWithStatisticsItems(items, safeItem, statisticItemIndex);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFirstResult(int firstResult) {
		queryBuilder.setFirstResult(firstResult);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxResults(int maxResults) {
		queryBuilder.setMaxResults(maxResults);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCachable() {
		return queryBuilder.isCachable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCachable(boolean cachable) {
		queryBuilder.setCachable(cachable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addRestriction(HQLRestriction restriction) {
		queryBuilder.addRestriction(restriction);
	}

}

