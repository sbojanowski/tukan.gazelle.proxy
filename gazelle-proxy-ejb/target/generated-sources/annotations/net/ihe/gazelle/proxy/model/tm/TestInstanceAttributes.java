package net.ihe.gazelle.proxy.model.tm;

import java.util.Date;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class TestInstanceAttributes<T extends TestInstance> extends HQLSafePathEntity<T> {

	public TestInstanceAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return TestInstance.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to configurations of type net.ihe.gazelle.proxy.model.tm.Configuration
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = true, notNull = false)
	public ConfigurationAttributes<Configuration> configurations() {
		return new ConfigurationAttributes<Configuration>(path + ".configurations", queryBuilder);
	}

	/**
	 * @return Path to steps of type net.ihe.gazelle.proxy.model.tm.Step
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = true, notNull = false)
	public StepAttributes<Step> steps() {
		return new StepAttributes<Step>(path + ".steps", queryBuilder);
	}

	/**
	 * @return Path to date of type java.util.Date
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicDate<Date> date() {
		return new HQLSafePathBasicDate<Date>(this, path + ".date", queryBuilder, Date.class);
	}

	/**
	 * @return Path to tmId of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> tmId() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".tmId", queryBuilder, Integer.class);
	}



}

