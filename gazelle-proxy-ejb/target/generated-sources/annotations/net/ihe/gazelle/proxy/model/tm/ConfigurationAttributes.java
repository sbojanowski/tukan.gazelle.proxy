package net.ihe.gazelle.proxy.model.tm;

import net.ihe.gazelle.proxy.netty.ChannelType;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ConfigurationAttributes<T extends Configuration> extends HQLSafePathEntity<T> {

	public ConfigurationAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Configuration.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to port of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> port() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".port", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to id of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to host of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> host() {
		return new HQLSafePathBasicString<String>(this, path + ".host", queryBuilder, String.class);
	}

	/**
	 * @return Path to name of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> name() {
		return new HQLSafePathBasicString<String>(this, path + ".name", queryBuilder, String.class);
	}

	/**
	 * @return Path to testInstance of type net.ihe.gazelle.proxy.model.tm.TestInstance
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public TestInstanceEntity<TestInstance> testInstance() {
		return new TestInstanceEntity<TestInstance>(path + ".testInstance", queryBuilder);
	}

	/**
	 * @return Path to proxyPort of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> proxyPort() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".proxyPort", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to type of type net.ihe.gazelle.proxy.netty.ChannelType
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<ChannelType> type() {
		return new HQLSafePathBasic<ChannelType>(this, path + ".type", queryBuilder, ChannelType.class);
	}

	/**
	 * @return Path to tmId of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> tmId() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".tmId", queryBuilder, Integer.class);
	}



}

