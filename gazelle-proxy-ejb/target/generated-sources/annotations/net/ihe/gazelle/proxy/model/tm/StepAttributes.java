package net.ihe.gazelle.proxy.model.tm;

import java.util.Date;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.AbstractMessageEntity;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class StepAttributes<T extends Step> extends HQLSafePathEntity<T> {

	public StepAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Step.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to message of type net.ihe.gazelle.proxy.model.message.AbstractMessage
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public AbstractMessageEntity<AbstractMessage> message() {
		return new AbstractMessageEntity<AbstractMessage>(path + ".message", queryBuilder);
	}

	/**
	 * @return Path to id of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to senders of type net.ihe.gazelle.proxy.model.tm.Configuration
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = false, notNull = false)
	public ConfigurationAttributes<Configuration> senders() {
		return new ConfigurationAttributes<Configuration>(path + ".senders", queryBuilder);
	}

	/**
	 * @return Path to testInstance of type net.ihe.gazelle.proxy.model.tm.TestInstance
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public TestInstanceEntity<TestInstance> testInstance() {
		return new TestInstanceEntity<TestInstance>(path + ".testInstance", queryBuilder);
	}

	/**
	 * @return Path to date of type java.util.Date
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicDate<Date> date() {
		return new HQLSafePathBasicDate<Date>(this, path + ".date", queryBuilder, Date.class);
	}

	/**
	 * @return Path to receivers of type net.ihe.gazelle.proxy.model.tm.Configuration
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = false, notNull = false)
	public ConfigurationAttributes<Configuration> receivers() {
		return new ConfigurationAttributes<Configuration>(path + ".receivers", queryBuilder);
	}

	/**
	 * @return Path to stepIndex of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> stepIndex() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".stepIndex", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to tmId of type Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> tmId() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".tmId", queryBuilder, Integer.class);
	}



}

