// main.js

function addCopyToClipBoard(idButton){
var client = new ZeroClipboard(document.getElementById(idButton));

client.on("ready", function(readyEvent) {
	// alert( "ZeroClipboard SWF is ready!" );

	client.on("aftercopy", function(event) {
		// `this` === `client`
		// `event.target` === the element that was clicked
//		event.target.style.display = "none";
		alert("Copied permanent link to clipboard: " + event.data["text/plain"]);
	});
});
}