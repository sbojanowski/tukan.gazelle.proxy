function savePasswordLocal() {
	if (localStorage) {
		var checked = document.getElementById("login:rememberMe");
		if (checked && checked.checked) {
			localStorage['gazelleUsername'] = document
					.getElementById("login:username").value;
			localStorage['gazellePassword'] = document
					.getElementById("login:passwordNotHashed").value;
		}
	}
}

function loadPasswordLocal() {
	if (localStorage) {
		var checked = document.getElementById("login:rememberMe");
		if (checked && checked.checked) {
			document.getElementById("login:username").value = localStorage['gazelleUsername'];
			document.getElementById("login:passwordNotHashed").value = localStorage['gazellePassword'];
		}
	}
}

gazelleAddToBodyOnLoad(loadPasswordLocal);
