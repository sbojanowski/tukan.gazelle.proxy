/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 *			File : gazelleEncryption.js
 *			This file contains encryption methods for Gazelle
 *
 *		@author		Jean-Renan Chatel / INRIA Rennes IHE development Project
 *		@see			> 	Jchatel@irisa.fr  -  http://www.ihe-europe.org
 */
function getFakePassword(password) {
	var aaa = "";
	for (var i = 0; i < password.length; i++) {
		aaa = aaa + "a";
	}
	return aaa;
}

function hashGazelleAuthentication() {
	var password = document.getElementById('loginPanel:login:passwordField:passwordNotHashed').value;
	var hashedPassword = hex_md5(password);
	document.getElementById("loginPanel:login:password").value = hashedPassword;

	document.getElementById("loginPanel:login:passwordField:passwordNotHashed").value = getFakePassword(password);
}

function hashGazelleAuthenticationTop() {
	var password = document.getElementById('mbf:passwordNotHashed').value;
	var hashedPassword = hex_md5(password);
	document.getElementById("mbf:password").value = hashedPassword;

	document.getElementById("mbf:passwordNotHashed").value = getFakePassword(password);
}

/*
 * Hashing process for Gazelle, encrypting the 2 entered passwords before
 * submitting (used for user registration)
 */
function hashGazelleRegistration() {
	var password1 = document
			.getElementById('register:passwordDecoration:userPasswordNotHashed').value;
	var password2 = document
			.getElementById('register:passwordConfirmationDecoration:userPasswordConfirmationNotHashed').value;

	var hashedPassword1 = hex_md5(password1);
	var hashedPassword2 = hex_md5(password2);
	document.getElementById("register:passwordDecoration:userPassword").value = hashedPassword1;
	document
			.getElementById("register:passwordConfirmationDecoration:userPasswordConfirmation").value = hashedPassword2;

	document
			.getElementById('register:passwordDecoration:userPasswordNotHashed').value = getFakePassword(password1);
	document
			.getElementById('register:passwordConfirmationDecoration:userPasswordConfirmationNotHashed').value = getFakePassword(password2);
}

/*
 * Hashing process for Gazelle, encrypting the 2 entered passwords before
 * submitting. This method is used when user changes password, in pages : -
 * changeUserPassword.seam - changeUserPasswordAfterRegistration.seam
 *
 */
function hashGazelleChangeUserPassword(password1, password2) {
	var password1 = document
			.getElementById('changeUserPasswordForm:passwordDecoration:passwordInputNotHashed').value;
	var password2 = document
			.getElementById('changeUserPasswordForm:passwordConfirmationDecoration:passwordConfirmationInputNotHashed').value;

	var hashedPassword1 = hex_md5(password1);
	var hashedPassword2 = hex_md5(password2);
	document
			.getElementById("changeUserPasswordForm:passwordDecoration:passwordInput").value = hashedPassword1;
	document
			.getElementById("changeUserPasswordForm:passwordConfirmationDecoration:passwordConfirmationInput").value = hashedPassword2;

	document
			.getElementById('changeUserPasswordForm:passwordDecoration:passwordInputNotHashed').value = getFakePassword(password1);
	document
			.getElementById('changeUserPasswordForm:passwordConfirmationDecoration:passwordConfirmationInputNotHashed').value = getFakePassword(password2);
}

/*
 * Hashing process for Gazelle, encrypting the 3 entered passwords before
 * submitting. This method is used when user changes password, in page : -
 * changePasswordCurrentLoggedUser.seam
 *
 */
function hashGazelleChangeCurrentLoggedUserPassword() {
	var passwordOld = document
			.getElementById('register:currentPasswordDecoration:oldPasswordInputNotHashed').value;
	var password1 = document
			.getElementById('register:passwordDecoration:passwordInputNotHashed').value;
	var password2 = document
			.getElementById('register:passwordConfirmationDecoration:passwordConfirmationInputNotHashed').value;

	var hashedPasswordOld = hex_md5(passwordOld);
	var hashedPassword1 = hex_md5(password1);
	var hashedPassword2 = hex_md5(password2);
	document
			.getElementById("register:currentPasswordDecoration:oldPasswordInput").value = hashedPasswordOld;
	document.getElementById("register:passwordDecoration:passwordInput").value = hashedPassword1;
	document
			.getElementById("register:passwordConfirmationDecoration:passwordConfirmationInput").value = hashedPassword2;

	document
			.getElementById('register:currentPasswordDecoration:oldPasswordInputNotHashed').value = getFakePassword(passwordOld);
	document
			.getElementById('register:passwordDecoration:passwordInputNotHashed').value = getFakePassword(password1);
	document
			.getElementById('register:passwordConfirmationDecoration:passwordConfirmationInputNotHashed').value = getFakePassword(password2);

}