var url = document.URL;
url = url.substr(url.indexOf("//") + 2);
url = url.substr(url.indexOf("/") + 1);
url = url.substr(0, url.indexOf("/"));
var fineuploadUrl = '/' + url + '/fineupload';

function createFineUploader(divId, beanName, id, param, isMultiple, extensions,
		maxSize, extraDropzones, uploadCaption, cancelCaption, failCaption,
		dragZoneCaption, formatProgressCaption) {
	var uploader = {};
	var validation = {
		sizeLimit : maxSize
	};
	var dragAndDrop = {};
	var params = {
		element : document.getElementById(divId),
		request : {
			endpoint : fineuploadUrl,
			params : {
				'id' : id,
				'beanName' : beanName,
				'param' : param
			}
		},
		multiple : isMultiple,
		text : {
			uploadButton : uploadCaption,
			cancelButton : cancelCaption,
			failUpload : failCaption,
			dragZone : dragZoneCaption,
			formatProgress : formatProgressCaption
		},
		failedUploadTextDisplay : {
			mode : 'custom',
			maxChars : 200,
			responseProperty : 'error',
			enableTooltip : true
		},
		validation : validation,
		dragAndDrop : dragAndDrop,
		callbacks : {
			onComplete : function(idFineUploader, fileName, responseJSON) {
				if (uploader.getInProgress() == 0) {
					var rerenderFunction = "rerenderupload" + id + "()";
					eval(rerenderFunction);
				}
				if (responseJSON && responseJSON.success) {
					var elementToHide = uploader
							.getItemByFileId(idFineUploader);
					if (elementToHide) {
						elementToHide.style.display = "none";
					}
				}
			}
		}
	};
	if (extraDropzones) {
		var extraDropzonesArray = extraDropzones.split(',');
		if (extraDropzonesArray.length > 0) {
			var extraDropzonesElements = {};
			for ( var i = 0; i < extraDropzonesArray.length; i++) {
				extraDropzonesElements[i] = document
						.getElementById(extraDropzonesArray[i]);
			}
			dragAndDrop['extraDropzones'] = extraDropzonesElements;
		}
	}
	if (extensions) {
		var extensionArray = extensions.split(',');
		if (extensionArray.length > 0) {
			validation['allowedExtensions'] = extensionArray;
			validation['acceptFiles'] = extensions;
		}
	}
	uploader = new qq.FineUploader(params);
}
