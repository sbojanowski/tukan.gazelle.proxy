package net.ihe.gazelle.log4j;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.AbstractFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * <b>Class Description : </b>GazelleLog4J<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 22/03/16
 */
@Scope(ScopeType.APPLICATION)
@Name("net.ihe.gazelle.GazelleLog4J")
@Install
@BypassInterceptors
@Filter
public class GazelleLog4J extends AbstractFilter {

    private static Logger log = LoggerFactory.getLogger(GazelleLog4J.class);

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException,
            ServletException {

        HttpServletRequest servletRequest = (HttpServletRequest) req;
        String queryString = "";
        if (servletRequest.getQueryString() != null) {
            queryString = "?" + servletRequest.getQueryString();
        }
        MDC.put("path", servletRequest.getServletPath() + queryString);
        HttpSession session = ((HttpServletRequest) servletRequest).getSession(false);
        String ipAddress = ((HttpServletRequest) servletRequest).getRemoteAddr();

        if (session != null) {
            Object attribute = session.getAttribute("org.jboss.seam.security.identity");
            if (attribute instanceof Identity) {
                Identity identity = (Identity) attribute;
                Credentials credentials = identity.getCredentials();

                String username = credentials != null ? credentials.getUsername() : null;
                if (username != null && ipAddress != null) {
                    MDC.put("username", username + " : " + ipAddress);
                } else if (username != null) {
                    MDC.put("username", username);
                } else if (ipAddress != null) {
                    MDC.put("username", ipAddress);
                }
            }
        }
        filterChain.doFilter(req, resp);
    }
}