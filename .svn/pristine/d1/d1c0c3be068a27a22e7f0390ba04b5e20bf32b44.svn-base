package net.ihe.gazelle.proxy.gui;

import net.ihe.gazelle.proxy.model.message.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by gthomazon on 01/04/16.
 */
public class FormatBean implements ErrorListener, ErrorHandler {

    private static Logger log = LoggerFactory.getLogger(FormatBean.class);

    public String prettyFormat(String input, int indent) {
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            parser.getXMLReader().setErrorHandler(this);
            SAXSource xmlInput = new SAXSource(parser.getXMLReader(), new InputSource(new StringReader(input)));

            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", indent);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setErrorListener(this);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(xmlInput, xmlOutput);
            return xmlOutput.getWriter().toString();
        } catch (Exception e) {
            log.warn("Error when formatting content : " + e.getMessage() + "\nContent not formatted");
            return input;
        }
    }

    @Override
    public void warning(TransformerException e) throws TransformerException {
    }

    @Override
    public void error(TransformerException e) throws TransformerException {
    }

    @Override
    public void fatalError(TransformerException e) throws TransformerException {
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
    }

    public Boolean shouldDisplayMessageContent(AbstractMessage message) {
        if (message instanceof HTTPMessage) {
            HTTPMessage httpMessage = (HTTPMessage) message;
            if (httpMessage.getMessageReceived() == null) {
                return false;
            }

            String messageType = httpMessage.getMessageType();
            if (messageType == null || messageType.equals("")) {
                messageType = "application/octet-stream";
            }
            if (!messageType.toLowerCase().endsWith("xop+xml") && (messageType.toLowerCase().endsWith("xml") || messageType.toLowerCase().endsWith("html") || messageType
                    .toLowerCase().endsWith("fhir"))) {
                return true;
            }
        }
        if (message instanceof SyslogMessage) {
            if (message.getMessageLength() > 0) {
                return true;
            }
        }
        return false;
    }

    public Boolean shouldDisplayMtomMessageContent(AbstractMessage message) {
        if (message instanceof HTTPMessage) {
            HTTPMessage httpMessage = (HTTPMessage) message;
            if (httpMessage.getMessageReceived() == null) {
                return false;
            }
            String messageType = httpMessage.getMessageType();
            if (messageType == null || messageType.equals("")) {
                return false;
            }
            if (messageType.toLowerCase().endsWith("xml") || messageType.toLowerCase().endsWith("html") || messageType
                    .toLowerCase().endsWith("fhir")) {
                return false;
            }
            return true;
        }
        return false;
    }

    public Boolean shouldDisplayDicomMessageContent(AbstractMessage message) {
        if (message instanceof DicomMessage) {
            DicomMessage dicomMessage = (DicomMessage) message;
            if (dicomMessage.getDicomMessageReceivedAsString() != null) {
                return true;
            }
        }
        return false;
    }

    public Boolean shouldDisplayHL7MessageContent(AbstractMessage message) {
        if (message instanceof HL7Message) {
            HL7Message hl7Message = (HL7Message) message;
            if (hl7Message.getMessageReceivedAsString() != null) {
                return true;
            }
        }
        return false;
    }

    public Boolean shouldDisplayRawMessageContent(AbstractMessage message, boolean displayInText) {
        if (message instanceof RawMessage && !displayInText) {
            RawMessage rawMessage = (RawMessage) message;
            if (rawMessage.getRawMessageReceivedAsString() != null) {
                return true;
            }
        }
        return false;
    }
}
