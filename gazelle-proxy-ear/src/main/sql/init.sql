INSERT INTO app_configuration (id, variable, value) VALUES (1, 'evs_client_url', 'http://gazelle.ihe.net/EVSClient/');
INSERT INTO app_configuration (id, variable, value) VALUES (2, 'storage_dicom', '/opt/proxy/DICOM');
--Not use for the moment
--INSERT INTO app_configuration (id, variable, value) VALUES (3, 'cas_service', 'http://gazelle.ihe.net/proxy/');
INSERT INTO app_configuration (id, variable, value) VALUES (4, 'application_works_without_cas', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (5, 'application_release_notes_url', 'http://gazelle.ihe.net/jira/browse/PROXY#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration (id, variable, value) VALUES (6, 'application_documentation', 'http://gazelle.ihe.net/content/proxy-0');
INSERT INTO app_configuration (id, variable, value) VALUES (7, 'application_issue_tracker', 'http://gazelle.ihe.net/jira/browse/PROXY');
INSERT INTO app_configuration (id, variable, value) VALUES (8, 'proxy_ip_addresses', 'to be defined by an admin user');
INSERT INTO app_configuration (id, variable, value) VALUES (10, 'min_proxy_port', '10000');
INSERT INTO app_configuration (id, variable, value) VALUES (11, 'max_proxy_port', '11000');
INSERT INTO app_configuration (id, variable, value) VALUES (12, 'time_zone', 'Europe/Paris');
INSERT INTO app_configuration (id, variable, value) VALUES (13, 'application_url', 'http://localhost:8080/proxy');
INSERT INTO app_configuration (id, variable, value) VALUES (14, 'cas_url', 'https://gazelle.ihe.net/cas');
INSERT INTO app_configuration (id, variable, value) VALUES (15, 'proxy_oid', '1.1.1.1.1');
INSERT INTO app_configuration (id, variable, value) VALUES (16, 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (17, 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) VALUES (18, 'dcmdump_path', '/usr/bin/dcmdump');
INSERT INTO app_configuration (id, variable, value) VALUES (19, 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable, value) VALUES (20, 'jms_communication_is_enabled', 'false');


SELECT pg_catalog.setval('app_configuration_id_seq', 20, true);
