CREATE INDEX from_ip_index ON pxy_abstract_message(from_ip);
CREATE INDEX local_port_index ON pxy_abstract_message(local_port);
CREATE INDEX proxy_port_index ON pxy_abstract_message(proxy_port);
CREATE INDEX proxy_side_index ON pxy_abstract_message(proxy_side);
CREATE INDEX remote_port_index ON pxy_abstract_message(remote_port);
CREATE INDEX result_oid_index ON pxy_abstract_message(result_oid);
CREATE INDEX to_ip_index ON pxy_abstract_message(to_ip);
CREATE INDEX appname_index ON pxy_abstract_message(appname);
CREATE INDEX facility_index ON pxy_abstract_message(facility);
CREATE INDEX hostname_index ON pxy_abstract_message(hostname);
CREATE INDEX messageid_index ON pxy_abstract_message(messageid);
CREATE INDEX severity_index ON pxy_abstract_message(severity);
CREATE INDEX timestamp_index ON pxy_abstract_message(timestamp);
CREATE INDEX date_received_index ON pxy_abstract_message(date_received);
CREATE INDEX connection_id_index ON pxy_abstract_message(connection_id);
CREATE INDEX index_index ON pxy_abstract_message(index);
CREATE INDEX index_int_index ON pxy_abstract_message(index_int);
CREATE INDEX pxy_abstract_message_type_index ON pxy_abstract_message(pxy_abstract_message_type);


CREATE INDEX host_index ON  tm_configuration(host);
CREATE INDEX name_index ON  tm_configuration(name);
CREATE INDEX port_index ON  tm_configuration(port);
CREATE INDEX proxyport_index ON  tm_configuration(proxyport);
CREATE INDEX tmid_index ON  tm_configuration(tmid);
CREATE INDEX type_index ON  tm_configuration(type);
CREATE INDEX testinstance_id_index ON  tm_configuration(testinstance_id);

CREATE INDEX stepindex_index ON  tm_step(stepindex);
CREATE INDEX tmid_index1 ON  tm_step(tmid);
CREATE INDEX testinstance_id_index1 ON  tm_step(testinstance_id);
CREATE INDEX message_id_index ON  tm_step(message_id);

CREATE INDEX id_index ON  pxy_connection(id);
CREATE INDEX uuid_index ON  pxy_connection(uuid);


CREATE INDEX dicom_commandfield_index ON pxy_abstract_message(dicom_commandfield);

CREATE INDEX dicom_requestedsopclassuid_index ON pxy_abstract_message(dicom_requestedsopclassuid);

CREATE INDEX dicom_affectedsopclassuid_index ON pxy_abstract_message(dicom_affectedsopclassuid);


CREATE INDEX dicom_filecommandset_index ON pxy_abstract_message(dicom_filecommandset);

CREATE INDEX dicom_filetransfertsyntax_index ON pxy_abstract_message(dicom_filetransfertsyntax);
CREATE INDEX app_configuration_id_index ON app_configuration(id);