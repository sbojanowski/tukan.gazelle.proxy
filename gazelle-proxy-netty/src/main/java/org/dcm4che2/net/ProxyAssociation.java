package org.dcm4che2.net;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.net.pdu.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ProxyAssociation implements Runnable {

    static final Logger log = LoggerFactory.getLogger(ProxyAssociation.class);

    private AAssociateRQAC associateAC;
    private String storageFolder;
    private InputStream in;
    private ProxyPDUDecoder decoder;
    private State state;

    public ProxyAssociation(String storageFolder, InputStream in) {
        super();
        this.storageFolder = storageFolder;
        this.in = in;
        this.state = State.STA1;
    }

    private void logMethod(String methodName, Object... params) {
        System.out.print(">> " + methodName);
        for (Object object : params) {
            System.out.print(" ");
            if (object != null) {
                System.out.print(object.toString());
            } else {
                System.out.print("null");
            }
        }
        System.out.println();
    }

    public void receivedAssociateRJ(AAssociateRJ aAssociateRJ) {
        logMethod("receivedAssociateRJ", aAssociateRJ);
    }

    public void receivedReleaseRQ() {
        logMethod("receivedReleaseRQ");
    }

    public void receivedReleaseRP() {
        logMethod("receivedReleaseRP");
    }

    public void receivedAbort(AAbort aAbort) {
        logMethod("receivedAbort", aAbort);
    }

    public void receivedAssociateRQ(AAssociateRQ associateRQ) {
        logMethod("receivedAssociateRQ", associateRQ);
    }

    public void receivedAssociateAC(AAssociateAC associateAC) {
        this.associateAC = associateAC;
        logMethod("receivedAssociateAC", associateAC);
    }

    public void receivedPDataTF() {
        logMethod("receivedPDataTF");
    }

    public void onDimseRSP(DicomObject cmd, DicomObject data) {
        logMethod("onDimseRSP", cmd, data);
    }

    public void onDimseRQ(int pcid, DicomObject cmd, ProxyPDUDecoder proxyPDUDecoder, String tsuid) {
        logMethod("onDimseRQ", pcid, cmd, tsuid);
    }

    public void abort(AAbort aa) {
        logMethod("abort", aa);
    }

    public AAssociateRQAC getAssociateAC() {
        return associateAC;
    }

    public List<ProxyDicomMessage> getMessages() {
        return new ArrayList<ProxyDicomMessage>();
    }

    @Override
    public void run() {
        this.decoder = new ProxyPDUDecoder(this, in);
        try {
            while (!(state == State.STA1 || state == State.STA13)) {
                decoder.nextPDU();
            }
        } catch (AAbort aa) {
            abort(aa);
        } catch (IOException e) {
            // normal!
        } catch (Throwable e) {
            log.error("", e);
        }
    }
}
