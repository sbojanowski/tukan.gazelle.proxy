package net.ihe.gazelle.proxy.netty.channel;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;

/**
 * The Class ProxyHandlerConsumer
 * Process data to real server, all work is done in ProxyHandler.
 */
public class ProxyHandlerConsumer<REQU, RESP> extends ProxyHandler {

    protected ProxyHandlerProvider<REQU, RESP> proxyHandlerProvider;

    public ProxyHandlerConsumer(ProxyHandlerProvider<REQU, RESP> proxyHandlerProvider, Channel providerChannel) {
        super();
        this.providerChannel = providerChannel;
        this.proxyHandlerProvider = proxyHandlerProvider;
    }

    @Override
    public void setChannels(Channel providerChannel, Channel consumerChannel) {
        super.setChannels(providerChannel, consumerChannel);
        this.proxiedChannel = providerChannel;
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelConnected(ctx, e);

        setChannels(providerChannel, e.getChannel());
        proxyHandlerProvider.setChannels(providerChannel, e.getChannel());

        providerChannel.setReadable(true);
    }
}
