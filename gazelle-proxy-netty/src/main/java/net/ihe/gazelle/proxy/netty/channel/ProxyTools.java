package net.ihe.gazelle.proxy.netty.channel;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.util.CharsetUtil;

public class ProxyTools {

    /**
     * Closes the specified channel after all queued write requests are flushed.
     * @param ch : channel
     */
    public static void closeOnFlush(Channel ch) {
        if (ch.isConnected()) {
            ch.write(ChannelBuffers.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }
    }

    public static String dump(ChannelBuffer buffer) {
        return dump(buffer, buffer.readerIndex(), buffer.readableBytes());
    }

    public static String dump(ChannelBuffer buffer, int fromIndex, int length) {
        if (length < 0) {
            throw new IllegalArgumentException("length: " + length);
        }
        if (length == 0) {
            return "";
        }

        byte[] buf = getBytes(buffer, fromIndex, length);

        return new String(buf, CharsetUtil.UTF_8);
    }

    public static byte[] getBytes(ChannelBuffer buffer) {
        return getBytes(buffer, buffer.readerIndex(), buffer.readableBytes());
    }

    public static byte[] getBytes(ChannelBuffer buffer, int fromIndex, int length) {
        int endIndex = fromIndex + length;
        byte[] buf = new byte[length];

        int srcIdx = fromIndex;
        int dstIdx = 0;
        for (; srcIdx < endIndex; srcIdx++, dstIdx++) {
            buf[dstIdx] = buffer.getByte(srcIdx);
        }
        return buf;
    }

}
