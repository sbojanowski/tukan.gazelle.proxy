package net.ihe.gazelle.proxy.netty.protocols.hl7;

import ca.uhn.hl7v2.llp.MinLLPWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;

public class AppHL7Client {

    private static Logger log = LoggerFactory.getLogger(AppHL7Client.class);

    public static void main(String args[]) {
        Socket s = null;
        try {
            s = new Socket("127.0.0.1", 10013);
            MinLLPWriter out = new MinLLPWriter(s.getOutputStream());
            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < 100000; i++) {
                sb.append(i).append("Some message!\r");
            }
            String longMessage = sb.toString();
            out.writeMessage(longMessage);
            out.writeMessage("Some message2.");
            out.writeMessage(longMessage);
            out.writeMessage("Some message4.");
            out.writeMessage(longMessage);
            out.close();
        } catch (Exception e) {
            log.error("" + e);
        } finally {
            try {
                if (s != null) {
                    s.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

}
