package net.ihe.gazelle.proxy.netty.tools;

import net.ihe.gazelle.proxy.netty.ProxyEventListenerToStream;
import org.jboss.netty.handler.codec.http.HttpMessage;

import java.io.PrintStream;

public class HttpEventListenerSimple extends ProxyEventListenerToStream<HttpMessage, HttpMessage> {

    public HttpEventListenerSimple(PrintStream printStream) {
        super(printStream);
    }

    protected String decodeMessage(HttpMessage message) {
        return message.toString();
    }

    @Override
    protected String decodeResponse(HttpMessage response) {
        return decodeMessage(response);
    }

    @Override
    protected String decodeRequest(HttpMessage request) {
        return decodeMessage(request);
    }

}
