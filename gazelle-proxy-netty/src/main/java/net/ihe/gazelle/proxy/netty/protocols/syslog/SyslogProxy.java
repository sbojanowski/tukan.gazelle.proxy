package net.ihe.gazelle.proxy.netty.protocols.syslog;

import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import net.ihe.gazelle.proxy.netty.channel.ProxyTools;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandler;

import java.util.ArrayList;
import java.util.List;

public class SyslogProxy extends Proxy<SyslogData, byte[]> {

    public SyslogProxy(ProxyEventListener<SyslogData, byte[]> proxyEventListener, ConnectionConfig connectionConfig) {
        super(proxyEventListener, connectionConfig);
    }

    public List<ChannelHandler> getDecodersForRequest() {
        List<ChannelHandler> channels = new ArrayList<ChannelHandler>();

        // Decode syslog messages
        channels.add(new SyslogFrameDecoder());

        return channels;
    }

    public List<ChannelHandler> getDecodersForResponse() {
        return new ArrayList<ChannelHandler>();
    }

    @Override
    public SyslogData handleRequest(Object message) {
        if (message instanceof SyslogData) {
            SyslogData syslogData = (SyslogData) message;
            return syslogData;
        }
        return null;
    }

    @Override
    public byte[] handleResponse(Object message) {
        if (message instanceof ChannelBuffer) {
            ChannelBuffer cb = (ChannelBuffer) message;
            return ProxyTools.getBytes(cb);
        }
        return null;
    }

}
