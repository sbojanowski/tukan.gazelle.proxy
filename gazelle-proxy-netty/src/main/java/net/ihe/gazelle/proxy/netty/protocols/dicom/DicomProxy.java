package net.ihe.gazelle.proxy.netty.protocols.dicom;

import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.Association;
import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessage;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import org.jboss.netty.channel.ChannelHandler;

import java.util.ArrayList;
import java.util.List;

public class DicomProxy extends Proxy<DimseMessage, DimseMessage> {

    private String storageFolder;
    private Association association;
    private DicomDecoder dicomDecoder;

    public DicomProxy(ProxyEventListener<DimseMessage, DimseMessage> proxyEventListener,
                      ConnectionConfig connectionConfig, String storageFolder) {
        super(proxyEventListener, connectionConfig);
        setStorageFolder(storageFolder);
        setAssociation(new Association());
    }

    public Association getAssociation() {
        return association;
    }

    public void setAssociation(Association association) {
        this.association = association;
    }

    public DicomDecoder getDicomDecoder() {
        return dicomDecoder;
    }

    public void setDicomDecoder(DicomDecoder dicomDecoder) {
        this.dicomDecoder = dicomDecoder;
    }

    public String getStorageFolder() {
        return storageFolder;
    }

    public void setStorageFolder(String storageFolder) {

        this.storageFolder = storageFolder;
    }

    public List<ChannelHandler> getDecodersForRequest() {
        List<ChannelHandler> channels = new ArrayList<ChannelHandler>();

        // Decode Dicom messages
        channels.add(new DicomDecoder(getAssociation(), getStorageFolder()));

        return channels;
    }

    public List<ChannelHandler> getDecodersForResponse() {
        return getDecodersForRequest();
    }

    @Override
    public DimseMessage handleRequest(Object message) {
        if (message instanceof DimseMessage) {
            DimseMessage result = (DimseMessage) message;
            return result;
        }
        return null;
    }

    @Override
    public DimseMessage handleResponse(Object message) {
        return handleRequest(message);
    }

}
