package net.ihe.gazelle.proxy.netty;

import org.jboss.netty.channel.ChannelHandler;

import java.util.List;

public interface ConnectionConfig {

    // Add specific handlers when a client connects to the server proxy (mainly
    // for TLS)
    List<ChannelHandler> getHandlersForProxyProvider();

    // Add specific handlers when server proxy connects to the forwarded server
    // (mainly for TLS)
    List<ChannelHandler> getHandlersForProxyConsumer();

    /**
     * Returns the port open on proxy side
     *
     * @return the port open on proxy side
     */
    int getProxyProviderPort();

    /**
     * Returns the host name / IP address of the remote host to contact
     *
     * @return the host name / IP address of the remote host to contact
     */
    String getProxyConsumerHost();

    /**
     * Returns the port of the remote host to contact
     *
     * @return the port of the remote host to contact
     */
    int getProxyConsumerPort();

    /**
     * Returns the type of channel to open (HL7, Dicom...)
     *
     * @return the type of channel to open (HL7, Dicom...)
     */
    ChannelType getChannelType();

}