package net.ihe.gazelle.proxy.netty.basictls;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfigSimple;
import org.jboss.netty.channel.ChannelHandler;

import java.util.ArrayList;
import java.util.List;

public class ConnectionConfigSimpleTls extends ConnectionConfigSimple {

    private TlsConfig tlsConfig;

    public ConnectionConfigSimpleTls(int proxyProviderPort, String proxyConsumerHost, int proxyConsumerPort,
            ChannelType channelType, TlsConfig tlsConfig) {
        super(proxyProviderPort, proxyConsumerHost, proxyConsumerPort, channelType);
        this.tlsConfig = tlsConfig;
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyProvider() {
        List<ChannelHandler> result = new ArrayList<ChannelHandler>();
        result.addAll(tlsConfig.getServerChannelHandlers());
        result.addAll(super.getHandlersForProxyProvider());
        return result;
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyConsumer() {
        List<ChannelHandler> result = new ArrayList<ChannelHandler>();
        result.addAll(tlsConfig.getClientChannelHandlers());
        result.addAll(super.getHandlersForProxyConsumer());
        return result;
    }
}
