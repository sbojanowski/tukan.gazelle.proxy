package net.ihe.gazelle.proxy.netty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ServiceLoader;

@XmlType(name = "ChannelType")
@XmlEnum
@XmlRootElement(name = "ChannelType")
public enum ChannelType {

    @XmlEnumValue("DICOM")
    DICOM("DicomMessage", "DICOM"),

    @XmlEnumValue("HL7")
    HL7("HL7Message", "HL7"),

    @XmlEnumValue("SYSLOG")
    SYSLOG("SyslogMessage", "SYSLOG"),

    @XmlEnumValue("HTTP")
    HTTP("HTTPMessage", "HTTP"),

    @XmlEnumValue("RAW")
    RAW("RawMessage", "RAW");

    private String discriminator;
    private String identifier;

    ChannelType(String discriminator, String identifier) {
        this.discriminator = discriminator;
        this.identifier = identifier;
    }

    public static ChannelType getEnumValue(String idenfier) {
        for (ChannelType type : values()) {
            if (type.getIdentifier().equals(idenfier)) {
                return type;
            } else {
                continue;
            }
        }
        return null;
    }

    public String getDiscriminator() {
        return discriminator;
    }

    public Proxy<?, ?> getProxy(ConnectionConfig connectionConfig) {
        ServiceLoader<ProxyFactory> serviceLoader = ServiceLoader.load(ProxyFactory.class);
        for (ProxyFactory proxyFactory : serviceLoader) {
            if (proxyFactory.getChannelType().equals(this)) {
                return proxyFactory.newProxy(connectionConfig);
            }
        }
        return null;
    }

    public String getIdentifier() {
        return identifier;
    }

}
