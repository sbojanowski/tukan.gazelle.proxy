package net.ihe.gazelle.proxy.netty;

public interface ProxyFactory {

    Proxy<?, ?> newProxy(ConnectionConfig connectionConfig);

    ChannelType getChannelType();

}
