package net.ihe.gazelle.proxy.netty.basictls;

import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.handler.ssl.SslHandler;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import java.util.ArrayList;
import java.util.List;

public class TlsConfig {

    private static final String PROTOCOL = "TLS";
    private static final TrustManager[] ALWAYS_TRUST_MANAGER = new TrustManager[] { new AlwaysTrustManager() };

    private SSLContext clientContext;
    private SSLContext serverContext;

    public TlsConfig(TlsCredentials serverCredentials, boolean clientTls, TlsCredentials clientCredentials) {
        super();

        if (clientTls) {
            clientContext = createContext(clientCredentials);
        } else {
            clientContext = null;
        }

        if (serverCredentials != null) {
            serverContext = createContext(serverCredentials);
        } else {
            serverContext = null;
        }
    }

    private SSLContext createContext(TlsCredentials credentials) {
        SSLContext context = null;
        try {
            context = SSLContext.getInstance(PROTOCOL);
            KeyManager[] keyManagers = null;
            if (credentials != null) {
                KeyManager keyManager = new TlsKeyManager(credentials);
                keyManagers = new KeyManager[] { keyManager };
            }
            context.init(keyManagers, ALWAYS_TRUST_MANAGER, null);
        } catch (Exception e) {
            throw new Error("Failed to initialize the SSLContext", e);
        }
        return context;
    }

    private ChannelHandler getChannelHandler(SSLEngine engine) {
        SslHandler sslHandler = new SslHandler(engine);
        sslHandler.setEnableRenegotiation(true);
        return sslHandler;
    }

    public List<ChannelHandler> getServerChannelHandlers() {
        List<ChannelHandler> result = new ArrayList<ChannelHandler>(1);
        if (serverContext != null) {
            SSLEngine engine = serverContext.createSSLEngine();
            engine.setUseClientMode(false);
            result.add(getChannelHandler(engine));
        }
        return result;
    }

    public List<ChannelHandler> getClientChannelHandlers() {
        List<ChannelHandler> result = new ArrayList<ChannelHandler>(1);
        if (clientContext != null) {
            SSLEngine engine = clientContext.createSSLEngine();
            engine.setUseClientMode(true);
            result.add(getChannelHandler(engine));
        }
        return result;
    }

}
