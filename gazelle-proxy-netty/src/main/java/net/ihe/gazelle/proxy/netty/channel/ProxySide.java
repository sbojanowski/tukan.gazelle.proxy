package net.ihe.gazelle.proxy.netty.channel;

public enum ProxySide {

    REQUEST(true),

    RESPONSE(false);

    private boolean request;

    ProxySide(boolean request) {
        this.request = request;
    }

    public boolean isRequest() {
        return request;
    }

}
