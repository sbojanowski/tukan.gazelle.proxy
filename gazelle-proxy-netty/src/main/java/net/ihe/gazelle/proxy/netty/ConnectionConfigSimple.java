package net.ihe.gazelle.proxy.netty;

import org.jboss.netty.channel.ChannelHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "ConnectionConfig")
@XmlAccessorType(XmlAccessType.NONE)
public class ConnectionConfigSimple implements ConnectionConfig, Comparable<ConnectionConfigSimple> {

    private static Logger log = LoggerFactory.getLogger(ConnectionConfigSimple.class);

    /**
     * the port open on proxy side
     */
    @XmlElement(name = "ProxyPort")
    protected int proxyProviderPort;
    /**
     * the remote host to connect to
     */
    @XmlElement(name = "RemoteHost")
    protected String proxyConsumerHost;
    /**
     * the port of the remote host
     */
    @XmlElement(name = "RemotePort")
    protected int proxyConsumerPort;

    @XmlElement(name = "ChannelType")
    protected ChannelType channelType;

    private String channelTypeAsString;

    public ConnectionConfigSimple(int proxyProviderPort, String proxyConsumerHost, int proxyConsumerPort,
            ChannelType channelType) {
        super();
        this.proxyProviderPort = proxyProviderPort;
        this.proxyConsumerHost = proxyConsumerHost;
        this.proxyConsumerPort = proxyConsumerPort;
        this.channelType = channelType;
    }

    public ConnectionConfigSimple() {
        super();
    }

    public int getProxyProviderPort() {
        return proxyProviderPort;
    }

    public void setLocalPort(int localPort) {
        this.proxyProviderPort = localPort;
    }

    public String getProxyConsumerHost() {
        return proxyConsumerHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.proxyConsumerHost = remoteHost;
    }

    public int getProxyConsumerPort() {
        return proxyConsumerPort;
    }

    public void setRemotePort(int remotePort) {
        this.proxyConsumerPort = remotePort;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyProvider() {
        return new ArrayList<ChannelHandler>();
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyConsumer() {
        return new ArrayList<ChannelHandler>();
    }

    public String getChannelTypeAsString() {
        return channelTypeAsString;
    }

    public void setChannelTypeAsString(String channelTypeAsString) {
        this.channelTypeAsString = channelTypeAsString;
    }

    public String[] toStringArray() {
        return new String[] { channelType.getIdentifier(), Integer.toString(proxyProviderPort), proxyConsumerHost,
                Integer.toString(proxyConsumerPort) };
    }

    @Override
    public int compareTo(ConnectionConfigSimple o) {
        if (o != null) {
            return Integer.valueOf(this.getProxyProviderPort()).compareTo(Integer.valueOf(o.getProxyProviderPort()));
        }
        return -1;
    }

}
