package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;

import java.util.List;

import static org.jboss.netty.channel.Channels.pipeline;

public class PipelineFactoryConsumer<REQU, RESP> implements ChannelPipelineFactory {

    private Proxy<REQU, RESP> proxy;
    private Channel providerChannel;
    private ProxyHandlerProvider<REQU, RESP> proxyHandlerProvider;

    public PipelineFactoryConsumer(Proxy<REQU, RESP> proxy, ProxyHandlerProvider<REQU, RESP> proxyHandlerProvider,
            Channel providerChannel) {
        super();
        this.proxy = proxy;
        this.proxyHandlerProvider = proxyHandlerProvider;
        this.providerChannel = providerChannel;
    }

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline p = pipeline();

        int i = 0;
        List<ChannelHandler> connectionHandlers = proxy.getHandlersForProxyConsumer();
        if (connectionHandlers != null) {
            for (ChannelHandler channelHandler : connectionHandlers) {
                p.addLast("handler" + i, channelHandler);
                i++;
            }
        }

        final ProxyHandlerConsumer<REQU, RESP> outboundHandler = new ProxyHandlerConsumer<REQU, RESP>(
                proxyHandlerProvider, providerChannel);
        p.addLast("handler", outboundHandler);

        List<ChannelHandler> decoderChannels = proxy.getDecodersForResponse();
        for (ChannelHandler channelHandler : decoderChannels) {
            p.addLast("handler" + i, channelHandler);
            i++;
        }

        // send the response to the listener
        ProxyLoggerResponse<REQU, RESP> proxyLogger = new ProxyLoggerResponse<REQU, RESP>(proxy, outboundHandler);
        p.addLast("logger", proxyLogger);

        return p;
    }

}
