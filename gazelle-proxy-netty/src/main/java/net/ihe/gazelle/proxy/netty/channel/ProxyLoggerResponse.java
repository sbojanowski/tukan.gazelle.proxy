package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;

import java.sql.Timestamp;

public class ProxyLoggerResponse<REQU, RESP> extends ProxyLogger<REQU, RESP> {

    public ProxyLoggerResponse(Proxy<REQU, RESP> proxy, ProxyHandler matchingHandler) {
        super(proxy, matchingHandler);
    }

    @Override
    protected void processMessage(Timestamp timeStamp, Object message) {
        RESP response = proxy.handleResponse(message);
        if (response != null) {
            proxy.getProxyEventListener()
                    .onResponse(timeStamp, response, matchingHandler.requesterIp, matchingHandler.requesterPort,
                            proxy.getProxyProviderPort(), proxy.getProxyConsumerHost(), proxy.getProxyConsumerPort(),
                            matchingHandler.providerChannel.getId(), matchingHandler.consumerChannel.getId());
        }
    }
}
