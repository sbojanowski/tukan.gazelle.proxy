package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;

import java.sql.Timestamp;

public class ProxyLoggerRequest<REQU, RESP> extends ProxyLogger<REQU, RESP> {

    public ProxyLoggerRequest(Proxy<REQU, RESP> proxy, ProxyHandler matchingHandler) {
        super(proxy, matchingHandler);
    }

    @Override
    protected void processMessage(Timestamp timeStamp, Object message) {
        REQU request = proxy.handleRequest(message);
        if (request != null) {
            proxy.getProxyEventListener()
                    .onRequest(timeStamp, request, matchingHandler.requesterIp, matchingHandler.requesterPort,
                            proxy.getProxyProviderPort(), proxy.getProxyConsumerHost(), proxy.getProxyConsumerPort(),
                            matchingHandler.providerChannel.getId(), matchingHandler.consumerChannel.getId());
        }
    }

}
