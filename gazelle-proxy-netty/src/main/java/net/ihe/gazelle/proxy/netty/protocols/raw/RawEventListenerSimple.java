package net.ihe.gazelle.proxy.netty.protocols.raw;

import net.ihe.gazelle.proxy.netty.ProxyEventListenerToStream;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class RawEventListenerSimple extends ProxyEventListenerToStream<byte[], byte[]> {

    public RawEventListenerSimple(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected String decodeResponse(byte[] response) {
        return new String(response, StandardCharsets.UTF_8);
    }

    @Override
    protected String decodeRequest(byte[] request) {
        return new String(request,StandardCharsets.UTF_8);
    }

}
