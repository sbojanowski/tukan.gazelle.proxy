package net.ihe.gazelle.proxy.netty;

import java.io.PrintStream;
import java.sql.Timestamp;

public abstract class ProxyEventListenerToStream<REQU, RESP> implements ProxyEventListener<REQU, RESP> {

    private PrintStream printStream;

    public ProxyEventListenerToStream(PrintStream printStream) {
        super();
        this.printStream = printStream;
    }

    public void onRequest(Timestamp timeStamp, REQU request, String requesterIp, int requesterPort,
            int proxyProviderPort, String responderIp, int responderPort, int providerChannelId,
            int consumerChannelId) {
        synchronized (this) {
            printStream.println(providerChannelId + " ->->->->->->->->->->->->->->->->->->->->->->->");
            printStream.println(
                    " " + requesterIp + ":" + requesterPort + " -" + proxyProviderPort + "> " + responderIp + ":"
                            + responderPort);
            printStream.println(providerChannelId + " ->->->->->->->->->->->->->->->->->->->->->->->");
            printStream.println(decodeRequest(request));
            printStream.println(providerChannelId + " ->->->->->->->->->->->->->->->->->->->->->->->");
        }
    }

    public void onResponse(Timestamp timeStamp, RESP response, String requesterIp, int requesterPort,
            int proxyProviderPort, String responderIp, int responderPort, int providerChannelId,
            int consumerChannelId) {
        synchronized (this) {
            printStream.println(providerChannelId + " <-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-");
            printStream.println(
                    " " + requesterIp + ":" + requesterPort + " <" + proxyProviderPort + "- " + responderIp + ":"
                            + responderPort);
            printStream.println(providerChannelId + " <-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-");
            printStream.println(decodeResponse(response));
            printStream.println(providerChannelId + " <-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-");
        }
    }

    protected abstract String decodeResponse(RESP response);

    protected abstract String decodeRequest(REQU request);

}
