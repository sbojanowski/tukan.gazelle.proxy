package net.ihe.gazelle.proxy.netty.basictls;

import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

public class TlsCredentials {

    private X509Certificate[] certificateChain;
    private PrivateKey privateKey;

    public TlsCredentials(InputStream keyStoreStream, char[] keyStorePassword, String alias,
            char[] privateKeyPassword) {
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(keyStoreStream, keyStorePassword);

            Certificate[] certificates = ks.getCertificateChain(alias);
            certificateChain = new X509Certificate[certificates.length];
            System.arraycopy(certificates, 0, certificateChain, 0, certificates.length);

            Key key = ks.getKey(alias, privateKeyPassword);
            privateKey = (PrivateKey) key;
        } catch (Exception e) {
            throw new Error("Failed to load credentials from keystore", e);
        }
    }

    public TlsCredentials(InputStream p12Stream, char[] keyStorePassword) {
        try {
            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(p12Stream, keyStorePassword);

            Enumeration<String> aliases = ks.aliases();
            for (; aliases.hasMoreElements(); ) {
                String alias = aliases.nextElement();

                Certificate[] certificates = ks.getCertificateChain(alias);
                certificateChain = new X509Certificate[certificates.length];
                System.arraycopy(certificates, 0, certificateChain, 0, certificates.length);

                Key key = ks.getKey(alias, keyStorePassword);
                privateKey = (PrivateKey) key;
            }

        } catch (Exception e) {
            throw new Error("Failed to load credentials from keystore", e);
        }
    }

    public TlsCredentials(X509Certificate[] certificateChain, PrivateKey privateKey) {
        super();
        this.certificateChain = certificateChain.clone();
        this.privateKey = privateKey;
    }

    public X509Certificate[] getCertificateChain() {
        return certificateChain.clone();
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

}
