package net.ihe.gazelle.proxy.netty;

import net.ihe.gazelle.proxy.netty.channel.PipelineFactoryProvider;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelException;
import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.channel.socket.ClientSocketChannelFactory;
import org.jboss.netty.channel.socket.ServerSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Proxy<REQU, RESP> {

    private static final ExecutorService executor = Executors.newCachedThreadPool();
    // Set up the client socket factory (for connecting to remote host).
    public static final ClientSocketChannelFactory clientChannelFactory = new NioClientSocketChannelFactory(executor,
            executor);
    public static final ServerSocketChannelFactory serverChannelFactory = new NioServerSocketChannelFactory(executor,
            executor);
    private static Logger log = LoggerFactory.getLogger(Proxy.class);
    public int proxyProviderPort;
    public String proxyConsumerHost;
    public int proxyConsumerPort;
    public ChannelType channelType;
    private transient Channel serverChannel;
    private ConnectionConfig connectionConfig;
    private transient ProxyEventListener<REQU, RESP> proxyEventListener;
    private boolean cException = false;

    public Proxy(ProxyEventListener<REQU, RESP> proxyEventListener, ConnectionConfig connectionConfig) {
        super();

        this.serverChannel = null;
        this.connectionConfig = connectionConfig;
        this.proxyEventListener = proxyEventListener;
    }

    public abstract List<ChannelHandler> getDecodersForResponse();

    public abstract List<ChannelHandler> getDecodersForRequest();

    public abstract REQU handleRequest(Object message);

    public abstract RESP handleResponse(Object message);

    public int getProxyProviderPort() {
        setProxyProviderPort(connectionConfig.getProxyProviderPort());
        return proxyProviderPort;
    }

    public void setProxyProviderPort(int proxyProviderPort) {
        this.proxyProviderPort = proxyProviderPort;
    }

    public String getProxyConsumerHost() {
        setProxyConsumerHost(connectionConfig.getProxyConsumerHost());
        return proxyConsumerHost;
    }

    public void setProxyConsumerHost(String proxyConsumerHost) {
        this.proxyConsumerHost = proxyConsumerHost;
    }

    public int getProxyConsumerPort() {
        setProxyConsumerPort(connectionConfig.getProxyConsumerPort());
        return proxyConsumerPort;
    }

    public void setProxyConsumerPort(int proxyConsumerPort) {
        this.proxyConsumerPort = proxyConsumerPort;
    }

    public ChannelType getChannelType() {
        setChannelType(connectionConfig.getChannelType());
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public List<ChannelHandler> getHandlersForProxyProvider() {
        if (connectionConfig != null) {
            return connectionConfig.getHandlersForProxyProvider();
        } else {
            return new ArrayList<ChannelHandler>(0);
        }
    }

    public List<ChannelHandler> getHandlersForProxyConsumer() {
        if (connectionConfig != null) {
            return connectionConfig.getHandlersForProxyConsumer();
        } else {
            return new ArrayList<ChannelHandler>(0);
        }
    }

    public ProxyEventListener<REQU, RESP> getProxyEventListener() {
        return proxyEventListener;
    }

    public boolean isOpen() {
        if (serverChannel != null && serverChannel.isOpen()) {
            return true;
        }
        return false;
    }

    public boolean iscException() {
        return cException;
    }

    public void setcException(boolean cException) {
        this.cException = cException;
    }

    public void start() {
        if (serverChannel != null) {
            throw new IllegalStateException("Already started");
        }
        // Configure the TCP/IP server.
        ServerBootstrap bootstrap = new ServerBootstrap(serverChannelFactory);
        // Set up the event pipeline factory.
        bootstrap.setPipelineFactory(new PipelineFactoryProvider<REQU, RESP>(this));
        // Bind and start to accept incoming connections.
        try {
            serverChannel = bootstrap.bind(new InetSocketAddress(getProxyProviderPort()));
        } catch (ChannelException e) {
            setcException(true);
            log.error("" + e);
        } catch (Exception e) {
            log.error("" + e);
        }
    }

    public void stop() {
        if (serverChannel == null) {
            throw new IllegalStateException("Not started");
        }
        serverChannel.close().awaitUninterruptibly();
        serverChannel = null;
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append(":");
        sb.append(getProxyProviderPort());
        sb.append(" -> ");
        sb.append(getProxyConsumerHost());
        sb.append(":");
        sb.append(getProxyConsumerPort());
        return sb.toString();
    }

    public String[] getConnectionConfigAsArray() {
        return new String[] { getChannelType().getIdentifier(), Integer.toString(getProxyProviderPort()),
                getProxyConsumerHost(), Integer.toString(getProxyConsumerPort()) };
    }
}
