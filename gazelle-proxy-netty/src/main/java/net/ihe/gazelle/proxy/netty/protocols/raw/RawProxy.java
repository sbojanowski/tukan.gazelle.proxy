package net.ihe.gazelle.proxy.netty.protocols.raw;

import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import net.ihe.gazelle.proxy.netty.channel.ProxyTools;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandler;

import java.util.ArrayList;
import java.util.List;

public class RawProxy extends Proxy<byte[], byte[]> {

    public RawProxy(ProxyEventListener<byte[], byte[]> proxyEventListener, ConnectionConfig connectionConfig) {
        super(proxyEventListener, connectionConfig);
    }

    @Override
    public List<ChannelHandler> getDecodersForRequest() {
        return new ArrayList<ChannelHandler>();
    }

    @Override
    public List<ChannelHandler> getDecodersForResponse() {
        return new ArrayList<ChannelHandler>();
    }

    @Override
    public byte[] handleRequest(Object message) {
        if (message instanceof ChannelBuffer) {
            ChannelBuffer cb = (ChannelBuffer) message;
            return ProxyTools.getBytes(cb);
        }
        return null;
    }

    @Override
    public byte[] handleResponse(Object message) {
        return handleRequest(message);
    }

}
