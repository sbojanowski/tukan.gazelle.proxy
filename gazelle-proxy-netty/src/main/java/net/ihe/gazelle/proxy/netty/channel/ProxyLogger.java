package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

public abstract class ProxyLogger<REQU, RESP> extends SimpleChannelUpstreamHandler {

    static final Logger log = LoggerFactory.getLogger(ProxyLogger.class);
    protected Proxy<REQU, RESP> proxy;
    protected ProxyHandler matchingHandler;

    public ProxyLogger(Proxy<REQU, RESP> proxy, ProxyHandler matchingHandler) {
        super();
        this.proxy = proxy;
        this.matchingHandler = matchingHandler;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        ProxyHandler handler = (ProxyHandler) ctx.getPipeline().get("handler");
        final Timestamp timeStamp = handler.getLastMessage();
        final Object message = e.getMessage();
        try {
            new Thread(new Runnable() {
                public void run() {
                    processMessage(timeStamp, message);
                }
            }).start();
        } catch (Throwable t) {
            log.error("Fail to handle request", t);
        }
    }

    protected abstract void processMessage(Timestamp timeStamp, Object message);

}
