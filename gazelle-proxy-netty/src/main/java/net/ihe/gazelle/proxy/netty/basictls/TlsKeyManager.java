package net.ihe.gazelle.proxy.netty.basictls;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;
import java.net.Socket;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class TlsKeyManager extends X509ExtendedKeyManager {

    private static final String ALIAS = "default";
    private TlsCredentials credentials;

    public TlsKeyManager(TlsCredentials credentials) {
        super();
        this.credentials = credentials;
    }

    public String[] getClientAliases(String paramString, Principal[] paramArrayOfPrincipal) {
        return new String[] { ALIAS };
    }

    public String chooseClientAlias(String[] paramArrayOfString, Principal[] paramArrayOfPrincipal,
            Socket paramSocket) {
        return ALIAS;
    }

    public String[] getServerAliases(String paramString, Principal[] paramArrayOfPrincipal) {
        return new String[] { ALIAS };
    }

    public String chooseServerAlias(String paramString, Principal[] paramArrayOfPrincipal, Socket paramSocket) {
        return ALIAS;
    }

    public X509Certificate[] getCertificateChain(String paramString) {
        return credentials.getCertificateChain();
    }

    public PrivateKey getPrivateKey(String paramString) {
        return credentials.getPrivateKey();
    }

    @Override
    public String chooseEngineClientAlias(String[] paramArrayOfString, Principal[] paramArrayOfPrincipal,
            SSLEngine paramSSLEngine) {
        return ALIAS;
    }

    @Override
    public String chooseEngineServerAlias(String paramString, Principal[] paramArrayOfPrincipal,
            SSLEngine paramSSLEngine) {
        return ALIAS;
    }

}
