package net.ihe.gazelle.proxy.netty.tools;

import net.ihe.gazelle.proxy.netty.ProxyEventListenerToStream;

import java.io.PrintStream;

public class HL7EventListenerSimple extends ProxyEventListenerToStream<String, String> {

    public HL7EventListenerSimple(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected String decodeResponse(String response) {
        return response;
    }

    @Override
    protected String decodeRequest(String request) {
        return request;
    }

}
