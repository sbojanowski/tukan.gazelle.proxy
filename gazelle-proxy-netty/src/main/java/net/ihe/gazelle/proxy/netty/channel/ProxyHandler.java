package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.sql.Timestamp;

/**
 * The Class ProxyHandler
 * Two ProxyHandlers exchange their messages to each side.
 */
public abstract class ProxyHandler extends SimpleChannelUpstreamHandler {

    public static final ChannelLocal<Timestamp> data = new ChannelLocal<Timestamp>();
    static final Logger log = LoggerFactory.getLogger(ProxyHandler.class);
    protected Channel proxiedChannel;
    protected Channel providerChannel;
    protected Channel consumerChannel;
    protected String requesterIp;
    protected int requesterPort;
    private Timestamp lastMessage;

    public void setChannels(Channel providerChannel, Channel consumerChannel) {
        this.providerChannel = providerChannel;
        this.consumerChannel = consumerChannel;
        // Retrieve requester informations
        SocketAddress remoteAddress = providerChannel.getRemoteAddress();
        requesterIp = "";
        requesterPort = 0;
        if (remoteAddress instanceof InetSocketAddress) {
            InetSocketAddress inetSocketAddress = (InetSocketAddress) remoteAddress;
            requesterIp = inetSocketAddress.getHostName();
            requesterPort = inetSocketAddress.getPort();
        }
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, final MessageEvent e) throws Exception {
        ChannelBuffer msg = (ChannelBuffer) e.getMessage();
        if (providerChannel != null) {
            synchronized (providerChannel) {
                lastMessage = TimestampTool.getNanoPrecisionTimestamp();
                proxiedChannel.write(msg.copy());
                // If proxiedChannel is saturated, do not read until notified in
                // other ProxyHandler.channelInterestChanged().
                if (!proxiedChannel.isWritable()) {
                    e.getChannel().setReadable(false);
                }
            }
        }

        ctx.sendUpstream(e);
    }

    @Override
    public void channelInterestChanged(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        // If outboundChannel is not saturated anymore, continue accepting
        // the incoming traffic from the inboundChannel.
        if (providerChannel != null && proxiedChannel != null) {
            synchronized (providerChannel) {
                if (e.getChannel().isWritable()) {
                    proxiedChannel.setReadable(true);
                }
            }
        }
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        if (proxiedChannel != null) {
            ProxyTools.closeOnFlush(proxiedChannel);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        MDC.put("username", ctx.getChannel().getRemoteAddress().toString());

        String info = "Error handled by Netty: " + e.getCause().getMessage();
        Proxy proxy = null;
        try {
            if (this instanceof ProxyHandlerProvider) {
                proxy = ((ProxyHandlerProvider) this).proxy;
            } else if (this instanceof ProxyHandlerConsumer) {
                ProxyHandlerProvider proxyHandlerProvider = ((ProxyHandlerConsumer) this).proxyHandlerProvider;
                if (proxyHandlerProvider != null) {
                    proxy = proxyHandlerProvider.proxy;
                }
            }

            if (proxy != null) {
                info = info + " for proxy " + proxy.getInfo();
            } else {
                info = info + " for unknown proxy... " + this.getClass().getCanonicalName();
            }
            log.error(info);
            log.info("Details", e.getCause());
            ProxyTools.closeOnFlush(e.getChannel());
        } catch (Exception ex) {
            log.error("exceptionCaught : " + ex.getMessage());
        }
    }

    public Timestamp getLastMessage() {
        return lastMessage;
    }

}
