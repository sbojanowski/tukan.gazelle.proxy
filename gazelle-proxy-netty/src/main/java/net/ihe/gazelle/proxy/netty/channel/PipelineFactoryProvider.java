package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;

import java.util.List;

import static org.jboss.netty.channel.Channels.pipeline;

public class PipelineFactoryProvider<REQU, RESP> implements ChannelPipelineFactory {

    private Proxy<REQU, RESP> proxy;

    public PipelineFactoryProvider(Proxy<REQU, RESP> proxy) {
        super();
        this.proxy = proxy;
    }

    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline p = pipeline();

        int i = 0;
        List<ChannelHandler> connectionHandlers = proxy.getHandlersForProxyProvider();
        if (connectionHandlers != null) {
            for (ChannelHandler channelHandler : connectionHandlers) {
                p.addLast("handler" + i, channelHandler);
                i++;
            }
        }

        ProxyHandlerProvider<REQU, RESP> proxyHandlerProvider = new ProxyHandlerProvider<REQU, RESP>(proxy);
        p.addLast("handler", proxyHandlerProvider);

        List<ChannelHandler> decoderChannels = proxy.getDecodersForRequest();
        for (ChannelHandler channelHandler : decoderChannels) {
            p.addLast("handler" + i, channelHandler);
            i++;
        }

        // Logs requests when formatting is done
        p.addLast("logger", new ProxyLoggerRequest<REQU, RESP>(proxy, proxyHandlerProvider));

        return p;
    }
}
