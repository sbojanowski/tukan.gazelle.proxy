package net.ihe.gazelle.proxy.netty.channel;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.*;

import java.net.InetSocketAddress;

public class ProxyHandlerProvider<REQU, RESP> extends ProxyHandler {

    protected Proxy<REQU, RESP> proxy;

    public ProxyHandlerProvider(Proxy<REQU, RESP> proxy) {
        super();
        this.proxy = proxy;
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        final Channel providerChannelOpened = e.getChannel();

        // Suspend incoming traffic until connected to the remote host.
        providerChannelOpened.setReadable(false);

        // Start the connection attempt.
        ClientBootstrap cb = new ClientBootstrap(Proxy.clientChannelFactory);
        cb.setPipelineFactory(new PipelineFactoryConsumer<REQU, RESP>(proxy, this, providerChannelOpened));

        // Connection to real server
        ChannelFuture f = cb.connect(new InetSocketAddress(proxy.getProxyConsumerHost(), proxy.getProxyConsumerPort()));

        f.addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture future) throws Exception {
                if (!future.isSuccess()) {
                    // Close the connection if the connection attempt has
                    // failed.
                    providerChannelOpened.close();
                }
            }
        });

    }

    @Override
    public void setChannels(Channel providerChannel, Channel consumerChannel) {
        super.setChannels(providerChannel, consumerChannel);
        this.proxiedChannel = consumerChannel;
    }

}
