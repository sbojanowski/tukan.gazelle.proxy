package net.ihe.gazelle.proxy.netty.protocols.http;

import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.handler.codec.http.*;

import java.util.ArrayList;
import java.util.List;

public class HttpProxy extends Proxy<HttpMessage, HttpMessage> {

    public HttpProxy(ProxyEventListener<HttpMessage, HttpMessage> proxyEventListener,
            ConnectionConfig connectionConfig) {
        super(proxyEventListener, connectionConfig);
    }

    public List<ChannelHandler> getDecodersForRequest() {
        List<ChannelHandler> channels = new ArrayList<ChannelHandler>();

        // Decode HTTP request as we are on server side
        channels.add(new HttpRequestDecoder());
        // Inflate request content if needed
        channels.add(new HttpContentDecompressor());
        // Aggregates request chunks if needed
        channels.add(new HttpChunkAggregator(16 * 1024 * 1024));

        return channels;
    }

    public List<ChannelHandler> getDecodersForResponse() {
        List<ChannelHandler> channels = new ArrayList<ChannelHandler>();

        // Decode HTTP response as we are on client side
        channels.add(new HttpResponseDecoder());
        // Inflate response content if needed
        channels.add(new HttpContentDecompressor());
        // Aggregates request chunks if needed
        channels.add(new HttpChunkAggregator(16 * 1024 * 1024));

        return channels;
    }

    @Override
    public HttpMessage handleRequest(Object message) {
        if (message instanceof HttpMessage) {
            HttpMessage request = (HttpMessage) message;
            return request;
        }
        return null;
    }

    @Override
    public HttpMessage handleResponse(Object message) {
        return handleRequest(message);
    }
}
