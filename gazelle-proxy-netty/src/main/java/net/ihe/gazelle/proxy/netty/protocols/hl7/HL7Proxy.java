package net.ihe.gazelle.proxy.netty.protocols.hl7;

import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import org.jboss.netty.channel.ChannelHandler;

import java.util.ArrayList;
import java.util.List;

public class HL7Proxy extends Proxy<String, String> {

    public HL7Proxy(ProxyEventListener<String, String> proxyEventListener, ConnectionConfig connectionConfig) {
        super(proxyEventListener, connectionConfig);
    }

    public List<ChannelHandler> getDecodersForRequest() {
        List<ChannelHandler> channels = new ArrayList<ChannelHandler>();

        // Decode HL7 messages
        channels.add(new HL7FrameDecoder());

        return channels;
    }

    public List<ChannelHandler> getDecodersForResponse() {
        return getDecodersForRequest();
    }

    @Override
    public String handleRequest(Object message) {
        if (message instanceof String) {
            String result = (String) message;
            return result;
        }
        return null;
    }

    @Override
    public String handleResponse(Object message) {
        return handleRequest(message);
    }

}
