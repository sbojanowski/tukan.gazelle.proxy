package net.ihe.gazelle.proxy.netty.protocols.hl7;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;

public class HL7FrameDecoder extends FrameDecoder {

    static final Logger log = LoggerFactory.getLogger(HL7FrameDecoder.class);

    // character indicating the start of an HL7 message
    private static final char START_MESSAGE = '\u000b';

    // character indicating the termination of an HL7 message
    private static final char END_MESSAGE = '\u001c';

    // the final character of a message: a carriage return
    private static final char LAST_CHARACTER = 13;

    private static final int STATUS_INIT = 0;
    private static final int STATUS_READ = 1;
    private static final int STATUS_END = 2;

    private int status = STATUS_INIT;
    private ByteArrayOutputStream currentFrame = null;

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
        while (buffer.readableBytes() > 0) {
            byte readByte = buffer.readByte();
            switch (status) {
            case STATUS_INIT:
                if (readByte == START_MESSAGE) {
                    status = STATUS_READ;
                    currentFrame = new ByteArrayOutputStream();
                }
                break;
            case STATUS_READ:
                if (readByte == END_MESSAGE) {
                    status = STATUS_END;
                    return currentFrame.toString(CharsetUtil.UTF_8.name());
                } else {
                    currentFrame.write(readByte);
                }
                break;
            case STATUS_END:
                if (readByte == LAST_CHARACTER) {
                    status = STATUS_INIT;
                }
                if (readByte == START_MESSAGE) {
                    status = STATUS_READ;
                    currentFrame = new ByteArrayOutputStream();
                }
                break;
            default:
                break;
            }
        }

        return null;
    }
}
