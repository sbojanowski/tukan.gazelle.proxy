package net.ihe.gazelle.proxy.netty.protocols.dicom;

import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.Association;
import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessage;
import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessageParser;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;

public class DicomDecoder extends SimpleChannelUpstreamHandler {

    static final Logger log = LoggerFactory.getLogger(DicomDecoder.class);
    private DimseMessageParser dimseMessageParser;

    public DicomDecoder(Association association, String storageFolder) {
        super();
        try {
            dimseMessageParser = new DimseMessageParser(association, storageFolder);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public DicomDecoder(String storageFolder) {
        super();
        try {
            dimseMessageParser = new DimseMessageParser(new Association(), storageFolder);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {

        MDC.put("username", ctx.getChannel().getRemoteAddress().toString());

        Object m = e.getMessage();
        if (!(m instanceof ChannelBuffer)) {
            ctx.sendUpstream(e);
            return;
        }

        ChannelBuffer input = (ChannelBuffer) m;
        if (!input.readable()) {
            return;
        }

        try {
            byte[] buf = new byte[input.readableBytes()];
            input.readBytes(buf);

            Iterable<DimseMessage> messages = dimseMessageParser.push(buf);
            for (DimseMessage dimseMessage : messages) {
                Channels.fireMessageReceived(ctx, dimseMessage);
            }

        } catch (Exception ex) {
            log.error("messageReceived : " + ex.getMessage());
        }
    }

    @Override
    public void channelUnbound(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelUnbound(ctx, e);
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelDisconnected(ctx, e);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelClosed(ctx, e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        ctx.sendUpstream(e);
    }

}
