package net.ihe.gazelle.proxy.netty.protocols.syslog;

import org.openhealthtools.openatna.syslog.LogMessage;
import org.openhealthtools.openatna.syslog.SyslogException;
import org.openhealthtools.openatna.syslog.SyslogMessage;
import org.openhealthtools.openatna.syslog.bsd.BsdMessage;
import org.openhealthtools.openatna.syslog.protocol.ProtocolMessage;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

public class SyslogData {

    private int facility;
    private String hostName;
    private int severity;
    private String timestamp;
    private String payload;
    private String tag;
    private String appName;
    private String messageId;
    private String procId;
    private String rawMessage;

    public SyslogData(String rawMessage) {
        super();
        this.rawMessage = rawMessage;
    }

    public SyslogData(SyslogMessage<?> syslogMessage, String rawMessage) {
        super();

        this.rawMessage = rawMessage;

        facility = syslogMessage.getFacility();
        hostName = syslogMessage.getHostName();
        severity = syslogMessage.getSeverity();
        timestamp = syslogMessage.getTimestamp();

        LogMessage<?> logMessage = syslogMessage.getMessage();
        payload = null;
        if (logMessage != null) {
            Object messageObject = logMessage.getMessageObject();

            if (messageObject instanceof String) {
                payload = (String) messageObject;
            } else {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    logMessage.write(bos);
                    payload = new String(bos.toByteArray(), StandardCharsets.UTF_8);
                } catch (SyslogException e) {
                    payload = null;
                }
            }
        }
        tag = null;
        if (syslogMessage instanceof BsdMessage) {
            BsdMessage<?> bsdMessage = (BsdMessage<?>) syslogMessage;
            tag = bsdMessage.getTag();
        }

        appName = null;
        messageId = null;
        procId = null;
        if (syslogMessage instanceof ProtocolMessage) {
            ProtocolMessage<?> protocolMessage = (ProtocolMessage<?>) syslogMessage;
            appName = protocolMessage.getAppName();
            messageId = protocolMessage.getMessageId();
            procId = protocolMessage.getProcId();
        }

    }

    public int getFacility() {
        return facility;
    }

    public String getHostName() {
        return hostName;
    }

    public int getSeverity() {
        return severity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTag() {
        return tag;
    }

    public String getAppName() {
        return appName;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getProcId() {
        return procId;
    }

    public String getPayload() {
        return payload;
    }

    public String getRawMessage() {
        return rawMessage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SyslogData [facility=");
        builder.append(facility);
        builder.append(", hostName=");
        builder.append(hostName);
        builder.append(", severity=");
        builder.append(severity);
        builder.append(", timestamp=");
        builder.append(timestamp);
        builder.append(", payload=");
        builder.append(payload);
        builder.append(", tag=");
        builder.append(tag);
        builder.append(", appName=");
        builder.append(appName);
        builder.append(", messageId=");
        builder.append(messageId);
        builder.append(", procId=");
        builder.append(procId);
        builder.append(", rawMessage=");
        builder.append(rawMessage);
        builder.append("]");
        return builder.toString();
    }

}
