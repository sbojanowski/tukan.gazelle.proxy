package net.ihe.gazelle.proxy.netty.tools;

import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessage;
import net.ihe.gazelle.proxy.netty.ProxyEventListenerToStream;

import java.io.PrintStream;

public class DicomEventListenerSimple extends ProxyEventListenerToStream<DimseMessage, DimseMessage> {

    public DicomEventListenerSimple(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected String decodeResponse(DimseMessage response) {
        return response.toString();
    }

    @Override
    protected String decodeRequest(DimseMessage request) {
        return request.toString();
    }

}
