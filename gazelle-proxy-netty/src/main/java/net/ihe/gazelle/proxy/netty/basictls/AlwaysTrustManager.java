package net.ihe.gazelle.proxy.netty.basictls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class AlwaysTrustManager implements X509TrustManager {

    static final Logger log = LoggerFactory.getLogger(AlwaysTrustManager.class);

    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        // Always trust
        log.info("UNKNOWN CLIENT CERTIFICATE: " + chain[0].getSubjectDN());
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        // Always trust
        log.info("UNKNOWN SERVER CERTIFICATE: " + chain[0].getSubjectDN());
    }

}
