package net.ihe.gazelle.proxy.netty.channel;

import java.sql.Timestamp;

public class TimestampTool {

    // number of millisecond, mircoseconds and nanoseconds in one second.
    protected static final long MilliMuliplier = 1000L;
    protected static final long MicroMuliplier = MilliMuliplier * 1000L;
    protected static final long NanoMuliplier = MicroMuliplier * 1000L;

    // number of seconds passed 1970/1/1 00:00:00 GMT.
    private static long sec0;
    // fraction of seconds passed 1970/1/1 00:00:00 GMT, offset by
    // the base System.nanoTime (nano0), in nanosecond unit.
    private static long nano0;

    static {
        // initialize base time in second and fraction of second (ns).
        long curTime = System.currentTimeMillis();
        sec0 = curTime / MilliMuliplier;
        nano0 = (curTime % MilliMuliplier) * MicroMuliplier - System.nanoTime();
    }

    private TimestampTool() {
        // static
    }

    public static Timestamp getNanoPrecisionTimestamp() {
        long nano_delta = nano0 + System.nanoTime();
        long sec1 = sec0 + (nano_delta / NanoMuliplier);
        long nano1 = nano_delta % NanoMuliplier;

        Timestamp rtnTs = new Timestamp(sec1 * MilliMuliplier);
        rtnTs.setNanos((int) nano1);
        return rtnTs;
    }

}
