package net.ihe.gazelle.proxy.netty.protocols.hl7;

import ca.uhn.hl7v2.llp.MinLLPReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;

public class AppHL7Server {

    private static Logger log = LoggerFactory.getLogger(AppHL7Server.class);

    public static void main(String args[]) {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(10014);
            while (true) {
                try {
                    Socket newSocket = ss.accept();
                    MinLLPReader in = new MinLLPReader(newSocket.getInputStream());
                    String str = null;
                    do {
                        str = in.getMessage();
                        log.info(str);
                    } while (str != null);

                } catch (InterruptedIOException e) {
                    log.error("" + e);
                } catch (Exception e) {
                    log.error("" + e);
                }
            }
        } catch (Exception e) {
            log.error("" + e);
        } finally {
            try {
                if (ss != null) {
                    ss.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

}
