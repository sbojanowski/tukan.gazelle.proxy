package net.ihe.gazelle.proxy.netty;

import java.sql.Timestamp;

public interface ProxyEventListener<REQU, RESP> {

    void onRequest(Timestamp timeStamp, REQU request, String requesterIp, int requesterPort, int proxyProviderPort,
            String responderIp, int responderPort, int providerChannelId, int consumerChannelId);

    void onResponse(Timestamp timeStamp, RESP response, String requesterIp, int requesterPort, int proxyProviderPort,
            String responderIp, int responderPort, int providerChannelId, int consumerChannelId);

}
