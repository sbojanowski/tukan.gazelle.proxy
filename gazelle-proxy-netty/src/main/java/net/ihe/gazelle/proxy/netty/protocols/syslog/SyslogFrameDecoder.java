package net.ihe.gazelle.proxy.netty.protocols.syslog;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import org.openhealthtools.openatna.syslog.SyslogMessage;
import org.openhealthtools.openatna.syslog.SyslogMessageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

class SyslogFrameDecoder extends FrameDecoder {

    private static final int STATUS_INIT = 0;
    private static final int STATUS_SIZE = 1;
    private static final int STATUS_READ = 2;
    private static Logger log = LoggerFactory.getLogger(SyslogFrameDecoder.class);
    private int status = STATUS_INIT;
    private StringBuffer messageSizeBuffer = null;
    private int messageSize;
    private int rawMessageSize;
    private ByteArrayOutputStream currentFrame = null;

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
        while (buffer.readableBytes() > 0) {
            byte readByte = buffer.readByte();

            if (status == STATUS_INIT) {
                messageSizeBuffer = new StringBuffer();
                currentFrame = new ByteArrayOutputStream();
                status = STATUS_SIZE;
            }

            switch (status) {
            case STATUS_SIZE:
                if (readByte == 32) {
                    status = STATUS_READ;
                    try {
                        messageSize = Integer.valueOf(messageSizeBuffer.toString());
                    } catch (NumberFormatException e) {
                        log.error("Syslog message is malformed, it will be saved in a Raw : " + e);
                        rawMessageSize = (buffer.capacity() - 1) - messageSizeBuffer.length();

                        char c = (char) readByte;
                        messageSizeBuffer.append(c);
                    }
                } else {
                    char c = (char) readByte;
                    messageSizeBuffer.append(c);

                    if (buffer.readableBytes() == 0) {
                        byte[] bytes = String.valueOf(messageSizeBuffer).getBytes(StandardCharsets.UTF_8);
                        String rawMessage = new String(bytes, StandardCharsets.UTF_8);
                        return new SyslogData(rawMessage);
                    }
                }
                break;
            case STATUS_READ:
                currentFrame.write(readByte);
                if (messageSize == 0 && rawMessageSize != 0 && buffer.readableBytes() == 0) {
                    status = STATUS_INIT;
                    messageSizeBuffer.append(currentFrame);
                    byte[] bytes = String.valueOf(messageSizeBuffer).getBytes(StandardCharsets.UTF_8);
                    String rawMessage = new String(bytes, StandardCharsets.UTF_8);
                    return new SyslogData(rawMessage);
                }
                if (currentFrame.size() == messageSize) {
                    status = STATUS_INIT;
                    byte[] bytes = currentFrame.toByteArray();
                    String rawMessage = new String(bytes, StandardCharsets.UTF_8);
                    SyslogMessage<?> syslogMessage = SyslogMessageFactory.getFactory()
                            .read(new ByteArrayInputStream(bytes));
                    return new SyslogData(syslogMessage, rawMessage);
                }
                break;
            default:
                break;
            }
        }
        return null;
    }

}
