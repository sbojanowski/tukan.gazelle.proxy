package net.ihe.gazelle.proxy.netty.tools;

import net.ihe.gazelle.proxy.netty.ProxyEventListenerToStream;
import net.ihe.gazelle.proxy.netty.protocols.syslog.SyslogData;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class SyslogEventListenerSimple extends ProxyEventListenerToStream<SyslogData, byte[]> {

    public SyslogEventListenerSimple(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected String decodeRequest(SyslogData request) {
        return request.toString();
    }

    @Override
    protected String decodeResponse(byte[] request) {
        return new String(request, StandardCharsets.UTF_8);
    }

}
