package net.ihe.gazelle.proxy.netty.protocols.syslog;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertTrue;

public class SyslogFrameDecoderTest {

    private static Logger log = LoggerFactory.getLogger(SyslogFrameDecoderTest.class);

    @Test
    public void test_with_non_valid_syslog_raw() {
        SyslogFrameDecoder sfd = new SyslogFrameDecoder();
        String frame = "tr3sb1 <85>1 2015-06-08T13:46:29.750Z TLStest IHE_XDS 1234 ATNALOG - <atna>toto</atna>";
        byte[] bytes = frame.getBytes();
        ChannelBuffer buffer = ChannelBuffers.copiedBuffer(bytes);
        try {
            Object obj = sfd.decode(null, null, buffer);
            if (obj instanceof SyslogData) {
                SyslogData data = (SyslogData) obj;
                assertTrue(data.getRawMessage().equals(frame));
            }
        } catch (Exception e) {
            log.error("" + e);
        }
    }

    @Test
    public void test_with_non_valid_syslog_raw_string() {
        SyslogFrameDecoder sfd = new SyslogFrameDecoder();
        String frame = "blablabla";
        byte[] bytes = frame.getBytes();
        ChannelBuffer buffer = ChannelBuffers.copiedBuffer(bytes);
        try {
            Object obj = sfd.decode(null, null, buffer);
            if (obj instanceof SyslogData) {
                SyslogData data = (SyslogData) obj;
                assertTrue(data.getRawMessage().equals(frame));
            }
        } catch (Exception e) {
            log.error("" + e);
        }
    }

    @Test
    public void test_with_non_valid_syslog_raw_empty_string() {
        SyslogFrameDecoder sfd = new SyslogFrameDecoder();
        String frame = "";
        byte[] bytes = frame.getBytes();
        ChannelBuffer buffer = ChannelBuffers.copiedBuffer(bytes);
        try {
            Object obj = sfd.decode(null, null, buffer);
            assertTrue(obj == null);
        } catch (Exception e) {
            log.error("" + e);
        }
    }

    @Test
    public void test_with_valid_syslog() {
        SyslogFrameDecoder sfd = new SyslogFrameDecoder();
        String frame = "79 <85>1 2015-06-08T13:46:29.750Z TLStest IHE_XDS 1234 ATNALOG - <atna>toto</atna>";
        byte[] bytes = frame.getBytes();
        ChannelBuffer buffer = ChannelBuffers.copiedBuffer(bytes);
        try {
            Object obj = sfd.decode(null, null, buffer);
            if (obj instanceof SyslogData) {
                SyslogData data = (SyslogData) obj;
                assertTrue(data.getFacility() == 10);
                assertTrue(data.getSeverity() == 5);
                assertTrue(data.getTimestamp().equals("2015-06-08T13:46:29.750Z"));
                assertTrue(data.getHostName().equals("TLStest"));
                assertTrue(data.getAppName().equals("IHE_XDS"));
                assertTrue(data.getProcId().equals("1234"));
                assertTrue(data.getMessageId().equals("ATNALOG"));
                assertTrue(data.getPayload().equals("<atna>toto</atna>"));
            }
        } catch (Exception e) {
            log.error("" + e);
        }
    }
}
