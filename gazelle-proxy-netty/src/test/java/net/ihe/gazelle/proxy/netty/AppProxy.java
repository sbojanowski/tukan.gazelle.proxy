package net.ihe.gazelle.proxy.netty;

import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessage;
import net.ihe.gazelle.proxy.netty.basictls.ConnectionConfigSimpleTls;
import net.ihe.gazelle.proxy.netty.basictls.TlsConfig;
import net.ihe.gazelle.proxy.netty.basictls.TlsCredentials;
import net.ihe.gazelle.proxy.netty.protocols.dicom.DicomProxy;
import net.ihe.gazelle.proxy.netty.protocols.http.HttpProxy;
import net.ihe.gazelle.proxy.netty.protocols.raw.RawEventListenerSimple;
import net.ihe.gazelle.proxy.netty.protocols.raw.RawProxy;
import net.ihe.gazelle.proxy.netty.syslog.AuthSSLSocketFactory;
import net.ihe.gazelle.proxy.netty.syslog.KeystoreDetails;
import net.ihe.gazelle.proxy.netty.tools.DicomEventListenerSimple;
import net.ihe.gazelle.proxy.netty.tools.HttpEventListenerSimple;
import org.openhealthtools.openatna.syslog.Constants;
import org.openhealthtools.openatna.syslog.SyslogException;
import org.openhealthtools.openatna.syslog.SyslogMessage;
import org.openhealthtools.openatna.syslog.message.StringLogMessage;
import org.openhealthtools.openatna.syslog.mina.tls.TlsServer;
import org.openhealthtools.openatna.syslog.protocol.ProtocolMessage;
import org.openhealthtools.openatna.syslog.protocol.SdParam;
import org.openhealthtools.openatna.syslog.protocol.StructuredElement;
import org.openhealthtools.openatna.syslog.transport.SyslogListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AppProxy {
    public static void main(String[] args) throws Exception {
        // testHttpsConnection();

        // startDicomProxyTLS(10004);
        // startSyslogProxyTLS();

        startDicomProxyBasic(10000);
        /*
		 * startHTTPProxyBasic(10000); startHTTPProxyTLSClient1(10001);
		 * startHTTPProxyTLSClient2(10002); startHTTPProxyTLSServer(10003);
		 * 
		 * final ProxyEventListener<DimseMessage, DimseMessage> dicomProxyEvent
		 * = new DicomEventListenerSimple(System.out); DicomProxy dicomProxy =
		 * new DicomProxy(dicomProxyEvent, new ConnectionConfigSimple(10012,
		 * "jumbo-4.irisa.fr", 10012, ChannelType.DICOM), null);
		 * dicomProxy.setStorageFolder("/tmp/dicom"); dicomProxy.start();
		 */

		/*
		 * 
		 * final ProxyEventListener<HttpRequest, HttpResponse> httpProxyEvent =
		 * new HttpEventListenerSimple(System.out); HttpProxy httpProxy = new
		 * HttpProxy(httpProxyEvent, 8080, "jumbo-4.irisa.fr", 8080, null);
		 * httpProxy.start();
		 */

        // final ProxyEventListener<String, String> hl7ProxyEvent = new
        // HL7EventListenerSimple(System.out);
        // HL7Proxy hl7Proxy = new HL7Proxy(hl7ProxyEvent, 10013, "127.0.0.1",
        // 10014, null);
        // hl7Proxy.start();

    }

    private static void startDicomProxyBasic(int port) {
        DicomEventListenerSimple eventListener = new DicomEventListenerSimple(System.out);
        DicomProxy dicomProxy = new DicomProxy(eventListener,
                new ConnectionConfigSimple(port, "127.0.0.1", 11112, ChannelType.DICOM), "/tmp/DICOM");
        dicomProxy.start();
    }

    private static void startSyslogProxyTLS() {
        // Syslog Client 9443-TLS> Proxy1 10000-> Proxy Web app 8442-> Proxy3
        // 8443-TLS> Syslog
        // Server

        // Starts a Syslog server (8443)
        try {
            AuthSSLSocketFactory serverSocketFactory = new AuthSSLSocketFactory(getKeyStoreServer(),
                    getKeyStoreClient());
            org.openhealthtools.openatna.syslog.mina.tls.TlsConfig serverConfig = new org.openhealthtools.openatna.syslog.mina.tls.TlsConfig();
            serverConfig.setSSLContext(serverSocketFactory.getSSLContext());
            serverConfig.setHost("localhost");
            serverConfig.setPort(8443);
            TlsServer server = new TlsServer();
            server.configure(serverConfig);
            server.addSyslogListener(new Listener());
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        RawEventListenerSimple listener = new RawEventListenerSimple(System.out);

        // Starts proxy3 (provider not TLS - 8442, connects to a TLS server -
        // 8443)
        InputStream clientKeyStoreStream = listener.getClass().getResourceAsStream("/keys/185.jks");
        TlsCredentials clientCredentials = new TlsCredentials(clientKeyStoreStream, "password".toCharArray(), "tomcat",
                "password".toCharArray());
        TlsConfig tlsConfigClient = new TlsConfig(null, true, clientCredentials);
        ConnectionConfig connectionConfigClient = new ConnectionConfigSimpleTls(8442, "127.0.0.1", 8443,
                ChannelType.SYSLOG, tlsConfigClient);
        RawProxy proxy3 = new RawProxy(listener, connectionConfigClient);
        proxy3.start();

        // Starts proxy1 (provider TLS - 9443, connects to a not TLS server -
        // 10000)
        InputStream serverKeyStoreStream = listener.getClass().getResourceAsStream("/keys/186.jks");
        TlsCredentials serverCredentials = new TlsCredentials(serverKeyStoreStream, "password".toCharArray(), "tomcat",
                "password".toCharArray());
        TlsConfig tlsConfigServer = new TlsConfig(serverCredentials, false, null);
        ConnectionConfig connectionConfigServer = new ConnectionConfigSimpleTls(9443, "127.0.0.1", 10000,
                ChannelType.SYSLOG, tlsConfigServer);
        RawProxy proxy1 = new RawProxy(listener, connectionConfigServer);
        proxy1.start();

        // Ping!
        try {
            AuthSSLSocketFactory clientSocketFactory = new AuthSSLSocketFactory(getKeyStoreClient(),
                    getKeyStoreServer());
            ProtocolMessage sl = new ProtocolMessage(10, 5, "2009-08-14T14:12:23.115Z", "localhost",
                    new StringLogMessage("<atna></atna>"), "IHE_XDS", "ATNALOG", "1234");
            List<SdParam> params = new ArrayList<SdParam>();
            params.add(new SdParam("param1", "param value\\=1"));
            params.add(new SdParam("param2", "param value] 2"));
            params.add(new SdParam("param3", "param value 3"));
            params.add(new SdParam("param3", "param value 4"));
            StructuredElement se = new StructuredElement("exampleSDID@1234", params);
            sl.addStructuredElement(se);

            // Socket s = clientSocketFactory.createSecureSocket("localhost",
            // 8443);
            Socket s = clientSocketFactory.createSecureSocket("jumbo.irisa.fr", 6514);
            OutputStream out = s.getOutputStream();
            byte[] bytes = sl.toByteArray();
            for (int i = 0; i < 5; i++) {
                // add message length plus space before message
                out.write((String.valueOf(bytes.length) + " ").getBytes(Constants.ENC_UTF8));
                out.write(bytes);
                out.flush();
            }
            out.close();
            s.close();
        } catch (SyslogException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static KeystoreDetails getKeyStoreServer() {
        URL u = Thread.currentThread().getContextClassLoader().getResource("keys/186.jks");
        KeystoreDetails key = new KeystoreDetails(u.toString(), "password", "tomcat", "password");
        return key;
    }

    private static KeystoreDetails getKeyStoreClient() {
        URL uu = Thread.currentThread().getContextClassLoader().getResource("keys/185.jks");
        KeystoreDetails key = new KeystoreDetails(uu.toString(), "password", "tomcat", "password");
        return key;
    }

    private static void startDicomProxyTLS(int port) {
        final ProxyEventListener<DimseMessage, DimseMessage> dicomProxyEvent = new DicomEventListenerSimple(System.out);

        TlsConfig tlsConfig = new TlsConfig(
                new TlsCredentials(getStream("/home/glandais/435.p12"), "password".toCharArray()), false, null);

        ConnectionConfigSimpleTls connectionConfig = new ConnectionConfigSimpleTls(port, "kujira.irisa.fr", 10002,
                ChannelType.DICOM, tlsConfig);

        DicomProxy dicomProxy = new DicomProxy(dicomProxyEvent, connectionConfig, "/tmp/dicom");
        dicomProxy.start();

    }

    private static void startHTTPProxyTLSServer(int port) {
        HttpEventListenerSimple eventListener = new HttpEventListenerSimple(System.out);
        TlsConfig tlsConfig = new TlsConfig(
                new TlsCredentials(getStream("/home/glandais/435.p12"), "password".toCharArray()), false, null);
        HttpProxy httpProxy = new HttpProxy(eventListener,
                new ConnectionConfigSimpleTls(port, "www.google.fr", 80, ChannelType.HTTP, tlsConfig));
        httpProxy.start();
    }

    private static InputStream getStream(String string) {
        return AppProxy.class.getResourceAsStream(string);
    }

    private static void startHTTPProxyTLSClient1(int port) {
        HttpEventListenerSimple eventListener = new HttpEventListenerSimple(System.out);
        TlsConfig tlsConfig = new TlsConfig(null, true,
                new TlsCredentials(getStream("/home/glandais/437.p12"), "password".toCharArray()));
        HttpProxy httpProxy = new HttpProxy(eventListener,
                new ConnectionConfigSimpleTls(port, "gazelle.ihe.net", 443, ChannelType.HTTP, tlsConfig));
        httpProxy.start();
    }

    private static void startHTTPProxyTLSClient2(int port) {
        HttpEventListenerSimple eventListener = new HttpEventListenerSimple(System.out);
        TlsConfig tlsConfig = new TlsConfig(null, true, null);
        HttpProxy httpProxy = new HttpProxy(eventListener,
                new ConnectionConfigSimpleTls(port, "www.google.fr", 443, ChannelType.HTTP, tlsConfig));
        httpProxy.start();
    }

    private static void startHTTPProxyBasic(int port) {
        HttpEventListenerSimple eventListener = new HttpEventListenerSimple(System.out);
        HttpProxy httpProxy = new HttpProxy(eventListener,
                new ConnectionConfigSimple(port, "127.0.0.1", 8080, ChannelType.HTTP));
        httpProxy.start();
    }

    static class Listener implements SyslogListener {

        public void messageArrived(SyslogMessage message) {
            System.out.println("serialized message:");
            System.out.println(message.toString());
            System.out.println("application message:");
            System.out.println(message.getMessage().getMessageObject());
        }

        public void exceptionThrown(SyslogException exception) {
            exception.printStackTrace();
        }
    }
}
